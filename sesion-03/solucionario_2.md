# ejercicio 01
## Trabaje con la tabla "agenda" que almacena información de sus amigos.

## 01 Elimine la tabla "agenda"
codigo sql
```sql
drop table agenda;
```
salida sql
```sh
Table AGENDA dropped.
```
## 02 Cree una tabla llamada "agenda". Debe tener los siguientes campos: apellido (cadena de 30), nombre (cadena de 20), domicilio (cadena de 30) y telefono (cadena de 11)
codigo sql
```sql
create table agenda(
    nombre varchar2(20),
    domicilio varchar2(30),
    apellido varchar2(30),
    telefono varchar2(11)
);
```
salida sql
```sh
Table AGENDA created.
```
## 03 Visualice las tablas existentes para verificar la creación de "agenda" (all_tables)
codigo sql
```sql
select * from all_tables;
```
salida sql
```sh
si existe la tabla agenda
```
## 04 Visualice la estructura de la tabla "agenda" (describe)
codigo sql
```sql
DESCRIBE agenda;
```
salida sql
```sh
Name      Null? Type         
--------- ----- ------------ 
NOMBRE          VARCHAR2(20) 
DOMICILIO       VARCHAR2(30) 
TELEFONO        VARCHAR2(11) 
```
## 05 Ingrese los siguientes registros:
codigo sql
```sql
insert into agenda (apellido, nombre, domicilio, telefono) values ('Moreno','Alberto','Colon 123','4234567');
insert into agenda (apellido,nombre, domicilio, telefono) values ('Torres','Juan','Avellaneda 135','4458787');

```
salida sql
```sh
1 row inserted.
```
## 06 Seleccione todos los registros de la tabla.
codigo sql
```sql
select * from agenda;
```
salida sql
```sh
NOMBRE   APELLIDO  DOMICILIO       TELEFONO
Alberto  Moreno    Colon 123       4234567
Juan     Torrea    Avellanada123   4458787
```
## 07 Elimine la tabla "agenda"
codigo sql
```sql
drop table agenda;
```
salida sql
```sh
Table AGENDA dropped.
```
## 08 Intente eliminar la tabla nuevamente (aparece un mensaje de error)
codigo sql
```sql
drop table agenda;
```
salida sql
```sh
Error starting at line : 1 in command -
drop table agenda
Error report -
ORA-00942: table or view does not exist
00942. 00000 -  "table or view does not exist"
*Cause:    
*Action:
```
# ejercicio 02
## Trabaje con la tabla "libros" que almacena los datos de los libros de su propia biblioteca.

## 01 Elimine la tabla "libros"
codigo sql
```sql
drop table libros;
```
salida sql
```sh
Table LIBROS dropped.
```
## 02 Cree una tabla llamada "libros". Debe definirse con los siguientes campos: titulo (cadena de 20), autor (cadena de 30) y editorial (cadena de 15)
codigo sql
```sql
create table libros (
     titulo varchar2(20), 
     autor varchar2(30),  
     editorial varchar2(15)
);
```
salida sql
```sh
Table LIBROS created.
```
## 03 Visualice las tablas existentes.
codigo sql
```sql
select * from libros;
```
salida sql
```sh
TITULO AUTOR EDITORIAL
```
## 04 Visualice la estructura de la tabla "libros" Muestra los campos y los tipos de datos de la tabla "libros".
codigo sql
```sql
DESCRIBE LIBROS;
```
salida sql
```sh
Name      Null? Type         
--------- ----- ------------ 
TITULO          VARCHAR2(20) 
AUTOR           VARCHAR2(30) 
EDITORIAL       VARCHAR2(15) 
```
## 05 Ingrese los siguientes registros:
codigo sql
```sql
insert into libros (titulo,autor,editorial) values ('El aleph','Borges','Planeta');
insert into libros (titulo,autor,editorial) values ('Martin Fierro','Jose Hernandez','Emece');
insert into libros (titulo,autor,editorial) values ('Aprenda PHP','Mario Molina','Emece');

```
salida sql
```sh
1 row inserted.


1 row inserted.


1 row inserted.
```
## 06 Muestre todos los registros (select) de "libros"
codigo sql
```sql
select * from libros;
```
salida sql
```sh
TUTOR           AUTOR           EDITORIAL
El aleph        Borges          Planeta
Martin Fierro   Jose Hernandez  Emece
Aprenda PHP     Mario Molina    Emece
```