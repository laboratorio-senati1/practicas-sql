## Ejercicios propuestos
# Una empresa almacena los datos de sus empleados en una tabla denominada "empleados".
# 01 Elimine la tabla y créela con la siguiente estructura:
```sql
 drop table empleados;
 create table empleados(
  nombre varchar2(40),
  sueldo number(6,2)
 );

```
# 02 Ingrese algunos registros:
```sql
 insert into empleados values('Acosta Ana',550); 
 insert into empleados values('Bustos Bernardo',850); 
 insert into empleados values('Caseros Carolina',900); 
 insert into empleados values('Dominguez Daniel',490); 
 insert into empleados values('Fuentes Fabiola',820); 
 insert into empleados values('Gomez Gaston',740); 
 insert into empleados values('Huerta Hernan',1050); 

```
# 03 Muestre la suma total de todos los sueldos realizando un "select" (5400)
```sql
select sum(sueldo) as suma_total from empleados;    
```
# 04 Se necesita incrementar los sueldos en forma proporcional, en un 10% cada vez y controlar que la suma total de sueldos no sea menor a $7000, si lo es, el bucle debe continuar y volver a incrementar los sueldos, en caso de superarlo, se saldrá del ciclo repetitivo; es decir, este bucle continuará el incremento de sueldos hasta que la suma de los mismos llegue o supere los 7000.
```sql
declare
    total_sueldos number(6,2);
begin
    while total_sueldos < 7000 loop
        update empleados set sueldo = sueldo * 1.1;
        select sum(sueldo) into total_sueldos from empleados;
    end loop;
    commit;
end;
/
```
# 05 Verifique que los sueldos han sido incrementados y la suma de todos los sueldos es superior a 7000
```sql
select * from empleados;
select sum(sueldo) as suma_total from empleados;

```
# 06 Muestre el sueldo máximo realizando un "select"
```sql
select max(sueldo) as sueldo_maximo from empleados;

```
# 07 Se necesita incrementar los sueldos en forma proporcional, en un 5% cada vez y controlar que el sueldo máximo alcance o supere los $1600, al llegar o superarlo, el bucle debe finalizar. Incluya una variable contador que cuente cuántas veces se repite el bucle
```sql
declare
    sueldo_maximo number(6,2);
    contador number := 0;
begin
    loop
        update empleados set sueldo = sueldo * 1.05;
        select max(sueldo) into sueldo_maximo from empleados;
        contador := contador + 1;
        exit when sueldo_maximo >= 1600;
    end loop;
    commit;
    dbms_output.put_line('Repeticiones del bucle: ' || contador);
end;
/

```
# 08 Verifique que los sueldos han sido incrementados y el sueldo máximo es igual o superior a 1600
```sql
select * from empleados;
select max(sueldo) as sueldo_maximo from empleados;

```
# 09 Muestre el sueldo mínimo realizando un "select"
```sql
select min(sueldo) as sueldo_minimo from empleados;
```
# 10 Se necesita incrementar los sueldos en forma proporcional, en un 10% cada vez y controlar que el sueldo mínimo no supere los $900. Emplee la sintaxis "if CONDICION then exit"
```sql
declare
    sueldo_minimo number(6,2);
begin
    loop
        update empleados set sueldo = sueldo * 1.1;
        select min(sueldo) into sueldo_minimo from empleados;
        exit when sueldo_minimo > 900;
    end loop;
    commit;
end;
/
```
# 11 Muestre el sueldo mínimo realizando un "select"
```sql
select min(sueldo) as sueldo_minimo from empleados;
```