## Ejercicios propuestos
# Una empresa almacena la información de sus clientes en una tabla llamada "clientes".
# 01 Elimine la tabla:
```sql
drop table clientes cascade constraints;
```
# 02 Cree la tabla:
```sql
create table clientes(
    nombre varchar2(40),
    documento char(8),
    domicilio varchar2(30),
    ciudad varchar2(30)
);

```
# 03 Ingrese algunos registros:
```sql
insert into clientes values('Juan Perez','22222222','Colon 1123','Cordoba');
insert into clientes values('Karina Lopez','23333333','San Martin 254','Cordoba');
insert into clientes values('Luis Garcia','24444444','Caseros 345','Cordoba');
insert into clientes values('Marcos Gonzalez','25555555','Sucre 458','Santa Fe');
insert into clientes values('Nora Torres','26666666','Bulnes 567','Santa Fe');
insert into clientes values('Oscar Luque','27777777','San Martin 786','Santa Fe');
insert into clientes values('Pedro Perez','28888888','Colon 234','Buenos Aires');
insert into clientes values('Rosa Rodriguez','29999999','Avellaneda 23','Buenos Aires');

```
# 04 Cree o reemplace la vista "vista_clientes" para que recupere el nombre y ciudad de todos los clientes
```sql
CREATE OR REPLACE VIEW vista_clientes AS
SELECT nombre, ciudad
FROM clientes;

```
# 05 Cree o reemplace la vista "vista_clientes2" para que recupere el nombre y ciudad de todos los clientes no permita modificaciones.
```sql
CREATE OR REPLACE VIEW vista_clientes2 AS
SELECT nombre, ciudad
FROM clientes
WITH READ ONLY;

```
# 06 Consulte ambas vistas
```sql
SELECT *
FROM vista_clientes;

SELECT *
FROM vista_clientes2;

```
# 07 Intente ingresar el siguiente registro mediante la vista que permite sólo lectura Oracle no lo permite.
```sql
INSERT INTO vista_clientes2 (nombre, ciudad)
VALUES ('Lucia Lopez', 'Cordoba');

```
# 08 Ingrese el registro anterior en la vista "vista_clientes"
```sql
INSERT INTO vista_clientes (nombre, ciudad)
VALUES ('Lucia Lopez', 'Cordoba');

```
# 09 Intente modificar un registro mediante la vista que permite sólo lectura
```sql
UPDATE vista_clientes2
SET ciudad = 'Rosario'
WHERE nombre = 'Juan Perez';

```
# 10 Actualice el registro anterior en la vista "vista_clientes"
```sql
UPDATE vista_clientes
SET ciudad = 'Rosario'
WHERE nombre = 'Juan Perez';

```
# 11 Intente eliminar un registro mediante la vista "vista_clientes2"
```sql
DELETE FROM vista_clientes2
WHERE nombre = 'Juan Perez';

```
# 12 Elimine todos los clientes de "Buenos Aires" a través de la vista "vista_clientes"
```sql
DELETE FROM vista_clientes
WHERE ciudad = 'Buenos Aires';
|
```