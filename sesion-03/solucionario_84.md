## Ejercicios propuestos
# Un club dicta cursos de distintos deportes. Almacena la información en varias tablas.
# 01 Elimine las tabla "inscriptos", "socios" y "cursos":
```sql
drop table inscriptos;
drop table socios;
drop table cursos;
```
# 02 Cree las tablas:
```sql
create table socios(
    documento char(8) not null,
    nombre varchar2(40),
    domicilio varchar2(30),
    constraint PK_socios_documento
    primary key (documento)
);

create table cursos(
    numero number(2),
    deporte varchar2(20),
    dia varchar2(15),
    constraint CK_inscriptos_dia check (dia in('lunes','martes','miercoles','jueves','viernes','sabado')),
    profesor varchar2(20),
    constraint PK_cursos_numero
    primary key (numero)
);

create table inscriptos(
    documentosocio char(8) not null,
    numero number(2) not null,
    matricula char(1),
    constraint PK_inscriptos_documento_numero
    primary key (documentosocio,numero),
    constraint FK_inscriptos_documento
    foreign key (documentosocio)
    references socios(documento),
    constraint FK_inscriptos_numero
    foreign key (numero)
    references cursos(numero)
);

```
# 03 Ingrese algunos registros para todas las tablas:
```sql
insert into socios values('30000000','Fabian Fuentes','Caseros 987');
insert into socios values('31111111','Gaston Garcia','Guemes 65');
insert into socios values('32222222','Hector Huerta','Sucre 534');
insert into socios values('33333333','Ines Irala','Bulnes 345');
insert into cursos values(1,'tenis','lunes','Ana Acosta');
insert into cursos values(2,'tenis','martes','Ana Acosta');
insert into cursos values(3,'natacion','miercoles','Ana Acosta');
insert into cursos values(4,'natacion','jueves','Carlos Caseres');
insert into cursos values(5,'futbol','sabado','Pedro Perez');
insert into cursos values(6,'futbol','lunes','Pedro Perez');
insert into cursos values(7,'basquet','viernes','Pedro Perez');
insert into inscriptos values('30000000',1,'s');
insert into inscriptos values('30000000',3,'n');
insert into inscriptos values('30000000',6,null);
insert into inscriptos values('31111111',1,'s');
insert into inscriptos values('31111111',4,'s');
insert into inscriptos values('32222222',1,'s');
insert into inscriptos values('32222222',7,'s');

```
# 04 Realice un join para mostrar todos los datos de todas las tablas, sin repetirlos (7 registros)
```sql
SELECT *
FROM socios s
FULL OUTER JOIN inscriptos i ON s.documento = i.documentosocio
FULL OUTER JOIN cursos c ON i.numero = c.numero;
```
# 05 Elimine la vista "vista_cursos"
```sql
DROP VIEW vista_cursos;
```
# 06 Cree la vista "vista_cursos" que muestre el número, deporte y día de todos los cursos.
```sql
CREATE VIEW vista_cursos AS
SELECT numero, deporte, dia
FROM cursos;

```
# 07 Consulte la vista ordenada por deporte (7 registros)
```sql
SELECT *
FROM vista_cursos
ORDER BY deporte;

```
# 08 Ingrese un registro mediante la vista "vista_cursos" y vea si afectó a "cursos"
```sql
INSERT INTO vista_cursos (numero, deporte, dia)
VALUES (8, 'voley', 'miercoles');

SELECT *
FROM cursos;

```
# 09 Actualice un registro sobre la vista y vea si afectó a la tabla "cursos"
```sql
UPDATE vista_cursos
SET deporte = 'basketball'
WHERE numero = 7;

SELECT *
FROM cursos;

```
# 10 Elimine un registro de la vista para el cual no haya inscriptos y vea si afectó a "cursos"
```sql
DELETE FROM vista_cursos
WHERE numero = 8;

SELECT *
FROM cursos;

```
# 11 Intente eliminar un registro de la vista para el cual haya inscriptos
```sql
DELETE FROM vista_cursos
WHERE numero = 1;

```
# 12 Elimine la vista "vista_inscriptos" y créela para que muestre el documento y nombre del socio, el numero de curso, el deporte y día de los cursos en los cuales está inscripto
```sql
DROP VIEW vista_inscriptos;

CREATE VIEW vista_inscriptos AS
SELECT s.documento, s.nombre, i.numero, c.deporte, c.dia
FROM socios s
JOIN inscriptos i ON s.documento = i.documentosocio
JOIN cursos c ON i.numero = c.numero;

```
# 13 Intente ingresar un registro en la vista:
```sql
insert into vista_inscriptos values('32222222','Hector Huerta',6,'futbol','lunes');
```
# 14 Intente actualizar el documento de un socio (no lo permite)
```sql
UPDATE vista_inscriptos
SET documento = '32222223'
WHERE documento = '32222222';

```
# 15 Elimine un registro mediante la vista
```sql
DELETE FROM vista_inscriptos
WHERE numero = 6;

```
# 16 Verifique que el registro se ha eliminado de "inscriptos"
```sql
SELECT *
FROM inscriptos;
```