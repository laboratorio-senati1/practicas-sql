# ejercicio 01 
## Primer problema: Un banco tiene registrados las cuentas corrientes de sus clientes en una tabla llamada "cuentas".

## 01 Elimine la tabla "cuentas":
codigo sql
```sql
drop table cuentas;
```
## 02 Cree la tabla :
codigo sql
```sql
create table cuentas(
    numero number(10) not null,
    documento char(8) not null,
    nombre varchar2(30),
    saldo number(9,2)
);
```
## 03 Ingrese un registro con valores para todos sus campos, omitiendo la lista de campos.
codigo sql
```sql
insert into cuentas values ('7418926','789416','Chostito',200);
```
## 04 Ingrese un registro omitiendo algún campo que admita valores nulos.
codigo sql
```sql
insert into cuentas values (' ',' ','Chostito',200);
```
## 05 Verifique que en tal campo se almacenó "null"
codigo sql
```sql
describe cuentas;
```
## 06 Intente ingresar un registro listando 3 campos y colocando 4 valores. Un mensaje indica que hay demasiados valores.
codigo sql
```sql
insert into cuentas values (' ',' ',' ','Chostito',200);
```
## 07 Intente ingresar un registro listando 3 campos y colocando 2 valores. Un mensaje indica que no hay suficientes valores.
codigo sql
```sql
insert into cuentas values (' ','Chostito',200);
```
## 08 Intente ingresar un registro sin valor para un campo definido "not null".
codigo sql
```sql
insert into cuentas values ('0','0','Chostito',200);
```
## 09 Vea los registros ingresados.
codigo sql
```sql
select * from cuentas;
```