## Ejercicios propuestos
# Una playa de estacionamiento almacena cada día los datos de los vehículos que ingresan en la tabla llamada "vehiculos".
## 01 Setee el formato de "date" para que nos muestre hora y minutos:
codigo sql
```sql
alter SESSION SET NLS_DATE_FORMAT = 'HH24:MI';
```
## 02 Elimine la tabla y créela con la siguiente estructura:
codigo sql
```sql
drop table vehiculos;

create table vehiculos(
    patente char(6) not null,
    tipo char(1),--'a'=auto, 'm'=moto
    horallegada date not null,
    horasalida date
);

```
## 03 Establezca una restricción "check" que admita solamente los valores "a" y "m" para el campo "tipo":
codigo sql
```sql
alter table vehiculos
add constraint CK_vehiculos_tipo
check (tipo in ('a','m'));

```
## 04 Agregue una restricción "primary key" que incluya los campos "patente" y "horallegada"
codigo sql
```sql
ALTER TABLE vehiculos ADD CONSTRAINT PK_vehiculos PRIMARY KEY (patente, horallegada);

```
## 05 Ingrese un vehículo.
codigo sql
```sql
INSERT INTO vehiculos VALUES ('ABC123', 'a', SYSDATE, NULL);

```
## 06 Intente ingresar un registro repitiendo la clave primaria.
codigo sql
```sql
INSERT INTO vehiculos VALUES ('ABC123', 'a', SYSDATE, NULL); -- Generará un error debido a la violación de la clave primaria

```
## 07 Ingrese un registro repitiendo la patente pero no la hora de llegada.
codigo sql
```sql
INSERT INTO vehiculos VALUES ('ABC123', 'm', SYSDATE + 1/24/60, NULL);

```
## 08 Ingrese un registro repitiendo la hora de llegada pero no la patente.
codigo sql
```sql
INSERT INTO vehiculos VALUES ('XYZ789', 'a', SYSDATE + 1/24/60, NULL);

```
## 09 Vea todas las restricciones para la tabla "vehiculos" aparecen 4 filas, 3 correspondientes a restricciones "check" y 1 a "primary key". Dos de las restricciones de control tienen nombres dados por Oracle.
codigo sql
```sql
SELECT constraint_name, constraint_type, status, validated
FROM user_constraints
WHERE table_name = 'VEHICULOS';
```
## 10 Elimine la restricción "primary key"
codigo sql
```sql
ALTER TABLE vehiculos DROP CONSTRAINT PK_vehiculos;

```
## 11 Vea si se ha eliminado. Ahora aparecen 3 restricciones.
codigo sql
```sql
SELECT constraint_name, constraint_type, status, validated
FROM user_constraints
WHERE table_name = 'VEHICULOS';
```
## 12 Elimine la restricción de control que establece que el campo "patente" no sea nulo (busque el nombre consultando "user_constraints")
codigo sql
```sql
ALTER TABLE vehiculos DROP CONSTRAINT SYS_C0011546;

```
## 13 Vea si se han eliminado.
codigo sql
```sql
SELECT constraint_name, constraint_type, status, validated
FROM user_constraints
WHERE table_name = 'VEHICULOS';
```
## 14 Vuelva a establecer la restricción "primary key" eliminada
codigo sql
```sql
ALTER TABLE vehiculos ADD CONSTRAINT PK_vehiculos PRIMARY KEY (patente, horallegada);

```
## 15 La playa quiere incluir, para el campo "tipo", además de los valores permitidos "a" (auto) y "m" (moto), el caracter "c" (camión). No puede modificar la restricción, debe eliminarla y luego redefinirla con los 3 valores.
codigo sql
```sql
ALTER TABLE vehiculos DROP CONSTRAINT CK_vehiculos_tipo;
ALTER TABLE vehiculos ADD CONSTRAINT CK_vehiculos_tipo CHECK (tipo IN ('a', 'm', 'c'));
```
## 16 Consulte "user_constraints" para ver si la condición de chequeo de la restricción "CK_vehiculos_tipo" se ha modificado.
codigo sql
```sql
SELECT constraint_name, search_condition
FROM user_constraints
WHERE table_name = 'VEHICULOS' AND constraint_name = 'CK_VEHICULOS_TIPO';
```
