## Ejercicios propuestos
# Un profesor almacena el documento, nombre y la nota final de cada alumno de su clase en una tabla llamada "alumnos".
# 01 Elimine la tabla: 
```sql
drop table alumnos;
```
# 02 Créela con los campos necesarios. Agregue una restricción "primary key" para el campo "documento" y una "check" para validar que el campo "nota" se encuentre entre los valores 0 y 10:
```sql
create table alumnos(
  .documento char(8),
  .nombre varchar2(30),
  .nota number(4,2),
  .primary key(documento),
  .constraint CK_alumnos_nota_valores check (nota>=0 and nota <=10)
);
```
# 03 Ingrese algunos registros: 
```sql
insert into alumnos values('30111111','Ana Algarbe',5.1);
insert into alumnos values('30222222','Bernardo Bustamante',3.2);
insert into alumnos values('30333333','Carolina Conte',4.5);
insert into alumnos values('30444444','Diana Dominguez',9.7);
insert into alumnos values('30555555','Fabian Fuentes',8.5);
insert into alumnos values('30666666','Gaston Gonzalez',9.70);

```
# 04 Obtenga todos los datos de los alumnos con la nota más alta, empleando subconsulta 
```sql
SELECT *
FROM alumnos
WHERE nota = (
    SELECT MAX(nota)
    FROM alumnos
);

```
# 05 Realice la misma consulta anterior pero intente que la consulta interna retorne, además del máximo valor de nota, el nombre del alumno. Mensaje de error, porque la lista de selección de una subconsulta que va luego de un operador de comparación puede incluir sólo un campo o expresión (excepto si se emplea "exists" o "in").
```sql
SELECT *
FROM alumnos
WHERE (nota, nombre) = (
    SELECT MAX(nota), nombre
    FROM alumnos
);

```
# 06 Muestre los alumnos que tienen una nota menor al promedio, su nota, y la diferencia con el promedio.
```sql
SELECT nombre, nota, (nota - AVG(nota)) AS diferencia_promedio
FROM alumnos
WHERE nota < (SELECT AVG(nota) FROM alumnos)

```
# 07 Cambie la nota del alumno que tiene la menor nota por 4.
```sql
UPDATE alumnos
SET nota = 4
WHERE nota = (
    SELECT MIN(nota)
    FROM alumnos
);
```
# 08 Elimine los alumnos cuya nota es menor al promedio.
```sql
DELETE FROM alumnos
WHERE nota < (SELECT AVG(nota) FROM alumnos);
```