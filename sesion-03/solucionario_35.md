# Ejercicios propuestos
## Una empresa registra los datos de sus empleados en una tabla llamada "empleados".
## 01 Elimine la tabla "empleados":
codigo sql
```sql
drop table empleados;
```
## 02 Cree la tabla:
codigo sql
```sql
create table empleados(
    legajo number(3),
    documento char(8) not null,
    nombre varchar2(30) not null,
    primary key(legajo)
);
```
## 03 Elimine la secuencia "sec_legajoempleados" y luego créela estableciendo el valor mínimo (1), máximo (210), valor inicial (206), valor de incremento (2) y no circular. Finalmente inicialice la secuencia.
codigo sql
```sql
DROP SEQUENCE sec_legajoempleados;
CREATE SEQUENCE sec_legajoempleados
    MINVALUE 1
    MAXVALUE 210
    START WITH 206
    INCREMENT BY 2
    NOCYCLE;
```
## 04 Ingrese algunos registros, empleando la secuencia creada para los valores de la clave primaria.
codigo sql
```sql
insert into empleados
values (sec_legajoempleados.currval,'22333444','Ana Acosta');

insert into empleados
values (sec_legajoempleados.nextval,'23444555','Betina Bustamante');

insert into empleados
values (sec_legajoempleados.nextval,'24555666','Carlos Caseros');

```
## 05 Recupere los registros de "libros" para ver los valores de clave primaria.
codigo sql
```sql
SELECT * FROM empleados;

```
## 06 Vea el valor actual de la secuencia empleando la tabla "dual"
codigo sql
```sql
SELECT sec_legajoempleados.CURRVAL FROM dual;

```
## 07 Intente ingresar un registro empleando "nextval":
codigo sql
```sql
insert into empleados
values (sec_legajoempleados.nextval,'25666777','Diana Dominguez');
```
## 08 Altere la secuencia modificando el atributo "maxvalue" a 999.
codigo sql
```sql
ALTER SEQUENCE sec_legajoempleados MAXVALUE 999;
```
## 09 Obtenga información de la secuencia.
codigo sql
```sql
SELECT * FROM user_sequences WHERE sequence_name = 'SEC_LEGAJOEMPLEADOS';

```
## 10 Ingrese el registro del punto 7.
codigo sql
```sql
INSERT INTO empleados
VALUES (sec_legajoempleados.NEXTVAL, '25666777', 'Diana Dominguez');
```
## 11 Recupere los registros.
codigo sql
```sql
SELECT * FROM empleados;
```
## 12 Modifique la secuencia para que sus valores se incrementen en 1.
codigo sql
```sql
ALTER SEQUENCE sec_legajoempleados INCREMENT BY 1;
```
## 13 Ingrese un nuevo registro:
codigo sql
```sql
insert into empleados
values (sec_legajoempleados.nextval,'26777888','Federico Fuentes');

```
## 14 Recupere los registros.
codigo sql
```sql
SELECT * FROM empleados;
```
## 15 Elimine la secuencia creada.
codigo sql
```sql
DROP SEQUENCE sec_legajoempleados;
```
## 16 Consulte todos los objetos de la base de datos que sean secuencias y verifique que "sec_legajoempleados" ya no existe.
codigo sql
```sql
SELECT object_name, object_type FROM all_objects WHERE object_type = 'SEQUENCE';
```