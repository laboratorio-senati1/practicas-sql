## Ejercicio 01
# Una empresa registra los datos de sus empleados en una tabla llamada "empleados".
## 01 Elimine la tabla "empleados":
codigo sql
```sql
drop table empleados;
```
## 02 Cree la tabla:
codigo sql
```sql
create table empleados(
    legajo number(3),
    documento char(8) not null,
    nombre varchar2(30) not null,
    primary key(legajo)
);
```
## 03 Elimine la secuencia "sec_legajoempleados" y luego créela estableciendo el valor mínimo (1), máximo (999), valor inicial (100), valor de incremento (2) y no circular. Finalmente inicialice la secuencia.

codigo sql
```sql

```
## 04 Ingrese algunos registros, empleando la secuencia creada para los valores de la clave primaria:
codigo sql
```sql
insert into empleados
values (sec_legajoempleados.currval,'22333444','Ana Acosta');

insert into empleados
values (sec_legajoempleados.nextval,'23444555','Betina Bustamante');

insert into empleados
values (sec_legajoempleados.nextval,'24555666','Carlos Caseros');

insert into empleados
values (sec_legajoempleados.nextval,'25666777','Diana Dominguez');

insert into empleados
values (sec_legajoempleados.nextval,'26777888','Estela Esper');

```
## 05 Recupere los registros de "libros" para ver los valores de clave primaria.
codigo sql
```sql
SELECT * FROM libros;
```
## 06 Vea el valor actual de la secuencia empleando la tabla "dual". Retorna 108.
codigo sql
```sql
SELECT sec_legajoempleados.currval FROM dual;

```
## 07 Recupere el valor siguiente de la secuencia empleando la tabla "dual" Retorna 110.
codigo sql
```sql
SELECT sec_legajoempleados.currval FROM dual;
```
## 08 Ingrese un nuevo empleado (recuerde que la secuencia ya tiene el próximo valor, emplee "currval" para almacenar el valor de legajo)
codigo sql
```sql
INSERT INTO empleados
VALUES (sec_legajoempleados.currval, '29000111', 'Hector Huerta');

``` 
## 09 Recupere los registros de "libros" para ver el valor de clave primaria ingresado anteriormente.
codigo sql
```sql
SELECT * FROM libros;
```
## 10 Incremente el valor de la secuencia empleando la tabla "dual" (retorna 112)
codigo sql
```sql
SELECT sec_legajoempleados.nextval FROM dual;
```
## 11 Ingrese un empleado con valor de legajo "112".
codigo sql
```sql
INSERT INTO empleados
VALUES (112, '30000222', 'Isabel Iglesias');
```
## 12 Intente ingresar un registro empleando "currval":
codigo sql
```sql
insert into empleados
values (sec_legajoempleados.currval,'29000111','Hector Huerta');
```
## 13 Incremente el valor de la secuencia. Retorna 114.
codigo sql
```sql
SELECT sec_legajoempleados.nextval FROM dual;

```
## 14 Ingrese el registro del punto 11.
codigo sql
```sql
INSERT INTO empleados
VALUES (112, '30000222', 'Isabel Iglesias');
```
## 15 Recupere los registros.
codigo sql
```sql
SELECT * FROM empleados;
```
## 16 Vea las secuencias existentes y analice la información retornada.
codigo sql
```sql
SELECT * FROM user_sequences;
```
## 17 Vea todos los objetos de la base de datos actual que contengan en su nombre la cadena "EMPLEADOS". Debe aparacer la tabla "empleados" y la secuencia "sec_legajoempleados".
codigo sql
```sql
SELECT object_name, object_type FROM all_objects WHERE object_name LIKE '%EMPLEADOS%';
```
## 18 Elimine la secuencia creada.
codigo sql
```sql
DROP SEQUENCE sec_legajoempleados;
```
## 19 Consulte todos los objetos de la base de datos que sean secuencias y verifique que "sec_legajoempleados" ya no existe.
codigo sql
```sql
SELECT object_name, object_type FROM all_objects WHERE object_type = 'SEQUENCE';
```
