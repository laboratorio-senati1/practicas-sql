## Práctica de laboratorio
# Una escuela necesita crear 3 usuarios diferentes en su base de datos. Uno denominado "director", otro "profesor" y un tercero "alumno". Conéctese como administrador (por ejemplo "system").
# 01 Elimine el usuario "director", porque si existe, aparecerá un mensaje de error:
```sql
drop user director cascade;
```
# 02 drop user director cascade;
```sql
```
# 03 Elimine el usuario "profesor":
```sql
drop user profesor cascade;
```
# 04 Cree un usuario "profesor", con contraseña "profe" y espacio en "system"
```sql
CREATE USER profesor IDENTIFIED BY profe QUOTA UNLIMITED ON system;

```
# 05 Elimine el usuario "alumno" y luego créelo con contraseña "alu" y espacio en "system"
```sql
DROP USER alumno CASCADE;
CREATE USER alumno IDENTIFIED BY alu QUOTA UNLIMITED ON system;
```
# 06 Consulte el diccionario "dba_users" y analice la información que nos muestra Deben aparecer los tres usuarios creados anteriormente.
```sql
SELECT * FROM dba_users;

```
# 07 Consulte el diccionario "dba_sys_privs" para encontrar los privilegios concedidos a nuestros tres usuarios Nos muestra que estos usuarios no tienen ningún privilegio concedido.
```sql
SELECT * FROM dba_sys_privs WHERE username IN ('director', 'profesor', 'alumno');

```
# 08 Conceda a "director" permiso para conectarse
```sql
GRANT CREATE SESSION TO director;

```
# 09 Conceda a "profesor" permiso para conectarse
```sql
GRANT CREATE SESSION TO profesor;

```
# 10 Consulte el diccionario "dba_sys_privs" para encontrar los privilegios concedidos a nuestros 3 usuarios
```sql
SELECT * FROM dba_sys_privs WHERE username IN ('director', 'profesor', 'alumno');
```
# 11 Abra una nueva conexión para "director". Se debe abrir una nueva solapa (nueva conexión) con el nombre del usuario (director)
```sql
CONNECT director;
```
# 12 En la conexión de "director" consulte sus privilegios
```sql
SELECT * FROM session_privs;
```
# 13 Obtenga el nombre del usuario conectado
```sql
SELECT USER FROM dual;
```
# 14 Vuelva a la conexión "system" (la otra solapa) y compruebe el usuario actual|
```sql
SELECT USER FROM dual;
```
# 15 Intente abrir una nueva conexión para el usuario inexistente. Debe aparecer un mensaje de error y denegarse la conexión. Cancele.
```sql
CONNECT usuario_inexistente;
```
# 16 Intente abrir una nueva conexión para el usuario "profesor" colocando una contraseña incorrecta. Debe aparecer un mensaje de error y denegarse la conexión. Cancele.
```sql
CONNECT profesor/contraseña_incorrecta;
```
# 17 Abra una nueva conexión para "profesor" colocando los datos correctos. Se debe abrir una nueva solapa (nueva conexión) con el nombre del usuario (profesor).
```sql
CONNECT profesor;
```
# 18 Intentemos abrir una nueva conexión para el usuario "alumno", el cual no tiene permiso. Un mensaje de error indica que el usuario "alumno" no tiene permiso "create session" por lo cual se deniega la conexión. Cancele.
```sql
CONNECT alumno;
```
# 19 Conceda a "alumno" permiso de conexión
```sql
GRANT CREATE SESSION TO alumno;
```
# 20 Consulte el diccionario "dba_sys_privs" para encontrar los privilegios concedidos a "alumno"
```sql
SELECT * FROM dba_sys_privs WHERE username = 'alumno';
```
# 21 Abra una nueva conexión para "ALUMNO". Se debe abrir una nueva solapa (nueva conexión) con el nombre del usuario (profesor)
```sql
CONNECT alumno;
```
# 22 Consulte el diccionario "user_sys_privs"
```sql
SELECT * FROM user_sys_privs;
```
# 23 Compruebe que está en la sesión de "alumno"
```sql
SELECT USER FROM dual;

```