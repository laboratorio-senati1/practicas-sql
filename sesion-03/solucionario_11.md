# Ejercicio 01
# Una farmacia guarda información referente a sus medicamentos en una tabla llamada "medicamentos".
## 01 Elimine la tabla y créela con la siguiente estructura:
codigo sql
```sql
drop table medicamentos;
create table medicamentos(
    codigo number(5) not null,
    nombre varchar2(20) not null,
    laboratorio varchar2(20),
    precio number(5,2),
    cantidad number(3,0) not null
);

```
salida sql
```sh
Table MEDICAMENTOS dropped.


Table MEDICAMENTOS created.
```
## 02 Visualice la estructura de la tabla "medicamentos" note que los campos "codigo", "nombre" y "cantidad", en la columna "Null" muestra "NOT NULL".
codigo sql
```sql
describe medicamentos;
```
salida sql
```sh
Name        Null?    Type         
----------- -------- ------------ 
CODIGO      NOT NULL NUMBER(5)    
NOMBRE      NOT NULL VARCHAR2(20) 
LABORATORIO          VARCHAR2(20) 
PRECIO               NUMBER(5,2)  
CANTIDAD    NOT NULL NUMBER(3) 
```
## 03 Ingrese algunos registros con valores "null" para los campos que lo admitan:
codigo sql
```sql
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(1,'Sertal gotas',null,null,100);
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(2,'Sertal compuesto',null,8.90,150);
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(3,'Buscapina','Roche',null,200);

```
salida sql
```sh
1 row inserted.


1 row inserted.


1 row inserted.
```
## 04 Vea todos los registros.
codigo sql
```sql
select * from medicamentos; 
```
salida sql
```sh
    CODIGO NOMBRE               LABORATORIO              PRECIO   CANTIDAD
---------- -------------------- -------------------- ---------- ----------
         1 Sertal gotas                                                100
         2 Sertal compuesto                                 8.9        150
         3 Buscapina            Roche                                  200
```
## 05 Ingrese un registro con valor "0" para el precio y cadena vacía para el laboratorio.
codigo sql
```sql
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(2,'paracetamol',' ',0,200);
```
salida sql
```sh
1 row inserted.
```
## 06 Intente ingresar un registro con cadena vacía para el nombre (mensaje de error)
codigo sql
```sql
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(3,' ','Roche',null,200);

```
salida sql
```sh
1 row inserted.
```
## 07 Intente ingresar un registro con valor nulo para un campo que no lo admite (aparece un mensaje de error)
codigo sql
```sql
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(null,' ','Roche',null,200);
```
salida sql
```sh
Error starting at line : 1 in command -
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(null,' ','Roche',null,200)
Error at Command Line : 1 Column : 77
Error report -
SQL Error: ORA-01400: cannot insert NULL into ("BRAYAN"."MEDICAMENTOS"."CODIGO")
01400. 00000 -  "cannot insert NULL into (%s)"
*Cause:    An attempt was made to insert NULL into previously listed objects.
*Action:   These objects cannot accept NULL values.
```
## 08 Recupere los registros que contengan valor "null" en el campo "laboratorio" (3 registros)
codigo sql
```sql
SELECT * FROM medicamentos WHERE laboratorio IS NULL;
```
salida sql
```sh
    CODIGO NOMBRE               LABORATORIO              PRECIO   CANTIDAD
---------- -------------------- -------------------- ---------- ----------
         1 Sertal gotas                                                100
         2 Sertal compuesto                                 8.9        150
```
## 09 Recupere los registros que contengan valor "null" en el campo "precio", luego los que tengan el valor 0 en el mismo campo. Note que el resultado es distinto (2 y 1 registros respectivamente)
codigo sql
```sql
SELECT * FROM medicamentos WHERE precio IS NULL;

SELECT * FROM medicamentos WHERE precio = 0;
```
salida sql
```sh
    CODIGO NOMBRE               LABORATORIO              PRECIO   CANTIDAD
---------- -------------------- -------------------- ---------- ----------
         1 Sertal gotas                                                100
         3 Buscapina            Roche                                  200
         3                      Roche                                  200

             CODIGO NOMBRE               LABORATORIO              PRECIO   CANTIDAD
---------- -------------------- -------------------- ---------- ----------

    CODIGO NOMBRE               LABORATORIO              PRECIO   CANTIDAD
---------- -------------------- -------------------- ---------- ----------
         2 paracetamol                                        0        200

```
## 10 Recupere los registros cuyo laboratorio no contenga valor nulo (1 registro)
codigo sql
```sql
SELECT * FROM medicamentos WHERE laboratorio IS NOT NULL;
```
salida sql
```sh
    CODIGO NOMBRE               LABORATORIO              PRECIO   CANTIDAD
---------- -------------------- -------------------- ---------- ----------
         3 Buscapina            Roche                                  200
         2 paracetamol                                        0        200
         3                      Roche                                  200
```
## 11 Recupere los registros cuyo precio sea distinto de 0, luego los que sean distintos de "null" (1 y 2 resgistros respectivamente) Note que la salida de la primera sentencia no muestra los registros con valor 0 y tampoco los que tienen valor nulo; el resultado de la segunda sentencia muestra los registros con valor para el campo precio (incluso el valor 0).
codigo sql
```sql
SELECT * FROM medicamentos WHERE precio <> 0;
```
salida sql
```sh
    CODIGO NOMBRE               LABORATORIO              PRECIO   CANTIDAD
---------- -------------------- -------------------- ---------- ----------
         2 Sertal compuesto                                 8.9        150
```
## 12 Ingrese un registro con una cadena de 1 espacio para el laboratorio.
codigo sql
```sql
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(1,'Several gotas',' ',null,100);
```
salida sql
```sh
1 row inserted.
```
## 13 Recupere los registros cuyo laboratorio sea "null" y luego los que contengan 1 espacio (3 y 1 registros respectivamente) Note que la salida de la primera sentencia no muestra los registros con valores para el campo "laboratorio" (un caracter espacio es un valor); el resultado de la segunda sentencia muestra los registros con el valor " " para el campo precio.
codigo sql
```sql
SELECT * FROM medicamentos WHERE laboratorio IS NULL OR laboratorio = ' ';
```
salida sql
```sh
    CODIGO NOMBRE               LABORATORIO              PRECIO   CANTIDAD
---------- -------------------- -------------------- ---------- ----------
         1 Sertal gotas                                                100
         2 Sertal compuesto                                 8.9        150
         2 paracetamol                                        0        200
         1 Several gotas                                               100
```
## 14 Recupere los registros cuyo laboratorio sea distinto de ' '(cadena de 1 espacio), luego los que sean distintos de "null" (1 y 2 registros respectivamente) Note que la salida de la primera sentencia no muestra los registros con valor ' ' y tampoco los que tienen valor nulo; el resultado de la segunda sentencia muestra los registros con valor para el campo laboratorio (incluso el valor ' ')
codigo sql
```sql
SELECT * FROM medicamentos WHERE laboratorio <> ' ';
```
salida sql
```sh
    CODIGO NOMBRE               LABORATORIO              PRECIO   CANTIDAD
---------- -------------------- -------------------- ---------- ----------
         3 Buscapina            Roche                                  200
         3                      Roche                                  200

```
# Ejercicio 02 
## Trabaje con la tabla que almacena los datos sobre películas, llamada "peliculas".

## 01 Elimine la tabla:
codigo sql
```sql
drop table peliculas;
```
salida sql
```sh
Table PELICULAS dropped.
```
## 02 Créela con la siguiente estructura:
codigo sql
```sql
create table peliculas(
    codigo number(4) not null,
    titulo varchar2(40) not null,
    actor varchar2(20),
    duracion number(3)
);

```
salida sql
```sh
Table PELICULAS created.
```
## 03 Visualice la estructura de la tabla. note que el campo "codigo" y "titulo", en la columna "Null" muestran "NOT NULL".
codigo sql
```sql
describe peliculas;
```
salida sql
```sh
Name     Null?    Type         
-------- -------- ------------ 
CODIGO   NOT NULL NUMBER(4)    
TITULO   NOT NULL VARCHAR2(40) 
ACTOR             VARCHAR2(20) 
DURACION          NUMBER(3) 
```
## 04 Ingrese los siguientes registros:
codigo sql
```sql
insert into peliculas (codigo,titulo,actor,duracion) values(1,'Mision imposible','Tom Cruise',120);
insert into peliculas (codigo,titulo,actor,duracion) values(2,'Harry Potter y la piedra filosofal',null,180);
insert into peliculas (codigo,titulo,actor,duracion) values(3,'Harry Potter y la camara secreta','Daniel R.',null);
insert into peliculas (codigo,titulo,actor,duracion) values(0,'Mision imposible 2','',150);
insert into peliculas (codigo,titulo,actor,duracion) values(4,'Titanic','L. Di Caprio',220);
insert into peliculas (codigo,titulo,actor,duracion) values(5,'Mujer bonita','R. Gere.J. Roberts',0);

```
salida sql
```sh
1 row inserted.


1 row inserted.


1 row inserted.


1 row inserted.


1 row inserted.


1 row inserted.
```
## 05 Recupere todos los registros para ver cómo Oracle los almacenó.
codigo sql
```sql
select * from peliculas;
```
salida sql
```sh
    CODIGO TITULO                                   ACTOR                  DURACION
---------- ---------------------------------------- -------------------- ----------
         1 Mision imposible                         Tom Cruise                  120
         2 Harry Potter y la piedra filosofal                                   180
         3 Harry Potter y la camara secreta         Daniel R.                      
         0 Mision imposible 2                                                   150
         4 Titanic                                  L. Di Caprio                220
         5 Mujer bonita                             R. Gere.J. Roberts            0
```
## 06 Intente ingresar un registro con valor nulo para campos que no lo admiten (aparece un mensaje de error)
codigo sql
```sql
insert into peliculas (codigo,titulo,actor,duracion) values(null,'Mision imposible','Tom Cruise',120);
```
salida sql
```sh
Error starting at line : 1 in command -
insert into peliculas (codigo,titulo,actor,duracion) values(null,'Mision imposible','Tom Cruise',120)
Error at Command Line : 1 Column : 61
Error report -
SQL Error: ORA-01400: cannot insert NULL into ("BRAYAN"."PELICULAS"."CODIGO")
01400. 00000 -  "cannot insert NULL into (%s)"
*Cause:    An attempt was made to insert NULL into previously listed objects.
*Action:   These objects cannot accept NULL values.
```
## 07 Muestre los registros con valor nulo en el campo "actor" (2 registros)
codigo sql
```sql
SELECT * FROM peliculas WHERE actor IS NULL;
```
salida sql
```sh
    CODIGO TITULO                                   ACTOR                  DURACION
---------- ---------------------------------------- -------------------- ----------
         2 Harry Potter y la piedra filosofal                                   180
         0 Mision imposible 2                                                   150
```
## 08 Actualice los registros que tengan valor de duración desconocido (nulo) por "120" (1 registro actualizado)
codigo sql
```sql
UPDATE peliculas SET duracion = 120 WHERE duracion IS NULL;
```
salida sql
```sh
1 row updated.
```
## 09 Coloque 'Desconocido' en el campo "actor" en los registros que tengan valor nulo en dicho campo (2 registros)
codigo sql
```sql
UPDATE peliculas SET actor = 'Desconocido' WHERE actor IS NULL;
```
salida sql
```sh
2 rows updated.
```
## 10 Muestre todos los registros
codigo sql
```sql
select * from peliculas;
```
salida sql
```sh
    CODIGO TITULO                                   ACTOR                  DURACION
---------- ---------------------------------------- -------------------- ----------
         1 Mision imposible                         Tom Cruise                  120
         2 Harry Potter y la piedra filosofal       Desconocido                 180
         3 Harry Potter y la camara secreta         Daniel R.                   120
         0 Mision imposible 2                       Desconocido                 150
         4 Titanic                                  L. Di Caprio                220
         5 Mujer bonita                             R. Gere.J. Roberts            0
```
## 11 Muestre todos los registros con valor nulo en el campo "actor" (ninguno)
codigo sql
```sql
SELECT * FROM peliculas WHERE actor IS NULL;
```
salida sql
```sh
no rows selected
```
## 12 Actualice la película en cuyo campo "duracion" hay 0 por "null" (1 registro)
codigo sql
```sql
UPDATE peliculas SET duracion = NULL WHERE duracion = 0;
```
salida sql
```sh
1 row updated.
```
## 13 Recupere todos los registros.
codigo sql
```sql
select * from peliculas;
```
salida sql
```sh
    CODIGO TITULO                                   ACTOR                  DURACION
---------- ---------------------------------------- -------------------- ----------
         1 Mision imposible                         Tom Cruise                  120
         2 Harry Potter y la piedra filosofal       Desconocido                 180
         3 Harry Potter y la camara secreta         Daniel R.                   120
         0 Mision imposible 2                       Desconocido                 150
         4 Titanic                                  L. Di Caprio                220
         5 Mujer bonita                             R. Gere.J. Roberts             
```
## 14 Borre todos los registros en los cuales haya un valor nulo en "duracion" (1 registro)
codigo sql
```sql
DELETE FROM peliculas WHERE duracion IS NULL;
```
salida sql
```sh
1 row deleted.
```
## 15 Verifique que se eliminó recuperando todos los registros.
codigo sql
```sql
select * from peliculas;
```
salida sql
```sh
    CODIGO TITULO                                   ACTOR                  DURACION
---------- ---------------------------------------- -------------------- ----------
         1 Mision imposible                         Tom Cruise                  120
         2 Harry Potter y la piedra filosofal       Desconocido                 180
         3 Harry Potter y la camara secreta         Daniel R.                   120
         0 Mision imposible 2                       Desconocido                 150
         4 Titanic                                  L. Di Caprio                220

```
