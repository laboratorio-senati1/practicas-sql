## Ejercicios propuestos
# Una empresa tiene registrados sus clientes en una tabla llamada "clientes", también tiene una tabla "provincias" donde registra los nombres de las provincias.
## 01 Elimine las tablas "clientes" y "provincias":
codigo sql
```sql
drop table clientes;
drop table provincias;

```
## 02 Créelas con las siguientes estructuras:
codigo sql
```sql
create table clientes (
    codigo number(5),
    nombre varchar2(30),
    domicilio varchar2(30),
    ciudad varchar2(20),
    codigoprovincia number(2),
    primary key(codigo)
);

create table provincias(
    codigo number(2),
    nombre varchar2(20),
    primary key (codigo)
);

```
## 03 Ingrese algunos registros para ambas tablas:
codigo sql
```sql
insert into provincias values(1,'Cordoba');
insert into provincias values(2,'Santa Fe');
insert into provincias values(3,'Misiones');
insert into provincias values(4,'Rio Negro');
insert into clientes values(100,'Perez Juan','San Martin 123','Carlos Paz',1);
insert into clientes values(101,'Moreno Marcos','Colon 234','Rosario',2);
insert into clientes values(102,'Garcia Juan','Sucre 345','Cordoba',1);
insert into clientes values(103,'Lopez Susana','Caseros 998','Posadas',3);
insert into clientes values(104,'Marcelo Moreno','Peru 876','Viedma',4);
insert into clientes values(105,'Lopez Sergio','Avellaneda 333','La Plata',5);

```
## 04 Intente agregar una restricción "foreign key" para que los códigos de provincia de "clientes" existan en "provincias" sin especificar la opción de comprobación de datos No se puede porque al no especificar opción para la comprobación de datos, por defecto es "validate" y hay un registro que no cumple con la restricción.
codigo sql
```sql
ALTER TABLE clientes
ADD CONSTRAINT fk_codigoprovincia
FOREIGN KEY (codigoprovincia)
REFERENCES provincias(codigo);
```
## 05 Agregue la restricción anterior pero deshabilitando la comprobación de datos existentes
codigo sql
```sql
ALTER TABLE clientes
ADD CONSTRAINT fk_codigoprovincia
FOREIGN KEY (codigoprovincia)
REFERENCES provincias(codigo)
DISABLE;
```
## 06 Vea las restricciones de "clientes"
codigo sql
```sql
SELECT constraint_name, constraint_type, table_name
FROM user_constraints
WHERE table_name = 'CLIENTES';
```
## 07 Deshabilite la restricción "foreign key" de "clientes"
codigo sql
```sql
ALTER TABLE clientes
DISABLE CONSTRAINT fk_codigoprovincia;
```
## 08 Vea las restricciones de "clientes"
codigo sql
```sql
SELECT constraint_name, constraint_type, table_name
FROM user_constraints
WHERE table_name = 'CLIENTES';
```
## 09 Agregue un registro que no cumpla la restricción "foreign key" Se permite porque la restricción está deshabilitada.
codigo sql
```sql
INSERT INTO clientes VALUES (106, 'Ana Torres', 'Sucre 999', 'Cordoba', 10);

``` 
## 10 Modifique el código de provincia del cliente código 104 por 9 Oracle lo permite porque la restricción está deshabilitada.
codigo sql
```sql
UPDATE clientes SET codigoprovincia = 9 WHERE codigo = 104;

```
## 11 Habilite la restricción "foreign key"
codigo sql
```sql
ALTER TABLE clientes
ENABLE CONSTRAINT fk_codigoprovincia;
```
## 12 Intente modificar un código de provincia existente por uno inexistente
codigo sql
```sql
UPDATE clientes SET codigoprovincia = 10 WHERE codigo = 104;

```
## 13 Intentealterar la restricción "foreign key" para que valide los datos existentes
codigo sql
```sql
ALTER TABLE clientes
MODIFY CONSTRAINT fk_codigoprovincia
ENABLE VALIDATE;
```
## 14 Elimine los registros que no cumplen la restricción y modifique la restricción a "enable" y "validate"
codigo sql
```sql
DELETE FROM clientes WHERE codigoprovincia = 10;

ALTER TABLE clientes
ENABLE CONSTRAINT fk_codigoprovincia VALIDATE;
```
## 15 Obtenga información sobre la restricción "foreign key" de "clientes"
codigo sql
```sql
SELECT constraint_name, constraint_type, table_name
FROM user_constraints
WHERE table_name = 'CLIENTES' AND constraint_name = 'FK_CODIGOPROVINCIA';
```