# Ejercicio 01 
## Un comercio que tiene un stand en una feria registra en una tabla llamada "visitantes" algunos datos de las personas que visitan o compran en su stand para luego enviarle publicidad de sus productos.
## 01 Elimine la tabla "visitantes" y créela con la siguiente estructura:
codigo sql
```sql
drop table visitantes;
create table visitantes(
    nombre varchar2(30),
    edad number(2),
    sexo char(1) default 'f',
    domicilio varchar2(30),
    ciudad varchar2(20) default 'Cordoba',
    telefono varchar2(11),
    mail varchar2(30) default 'no tiene',
    montocompra number(6,2)
);
```
## 02 Ingrese algunos registros:
codigo sql
```sql
insert into visitantes
values ('Susana Molina',35,default,'Colon 123',default,null,null,59.80);

insert into visitantes
values ('Marcos Torres',29,'m',default,'Carlos Paz',default,'<marcostorres@hotmail.com>',150.50);

insert into visitantes
values ('Mariana Juarez',45,default,default,'Carlos Paz',null,default,23.90);

insert into visitantes (nombre, edad,sexo,telefono, mail)
values ('Fabian Perez',36,'m','4556677','<fabianperez@xaxamail.com>');

insert into visitantes (nombre, ciudad, montocompra)
values ('Alejandra Gonzalez','La Falda',280.50);

insert into visitantes (nombre, edad,sexo, ciudad, mail,montocompra)
values ('Gaston Perez',29,'m','Carlos Paz','<gastonperez1@gmail.com>',95.40);

insert into visitantes
values ('Liliana Torres',40,default,'Sarmiento 876',default,default,default,85);

insert into visitantes
values ('Gabriela Duarte',21,null,null,'Rio Tercero',default,'<gabrielaltorres@hotmail.com>',321.50);

```
## 03 Queremos saber la cantidad de visitantes de cada ciudad utilizando la cláusula "group by" (4 filas devueltas)
codigo sql
```sql
select ciudad, count(*) from visitantes group by ciudad;
```
## 04 Queremos la cantidad visitantes con teléfono no nulo, de cada ciudad (4 filas devueltas)
codigo sql
```sql
select ciudad, count(*) from visitantes group by ciudad;
```
## 05 Necesitamos el total del monto de las compras agrupadas por sexo (3 filas) Note que los registros con valor nulo en el campo "sexo" se procesan como un grupo diferente.
codigo sql
```sql
select count(*) from visitantes group by sexo;
```
## 06 Se necesita saber el máximo y mínimo valor de compra agrupados por sexo y ciudad (6 filas)
codigo sql
```sql
select max(montocompra) as mayor, min(montocompra) as menor
from visitantes
group by sexo, ciudad;
```
## 07 Calcule el promedio del valor de compra agrupados por ciudad (4 filas)
codigo sql
```sql
select ciudad, avg(montocompra)
from visitantes
group by ciudad;
```
## 08 Cuente y agrupe por ciudad sin tener en cuenta los visitantes que no tienen mail (3 filas)
codigo sql
```sql
select count(*) from visitantes where mail is not NULL group by ciudad;
```
# Ejercicio 02 
## Una empresa almacena los datos de sus empleados en una tabla "empleados".

## 01 Elimine la tabla y luego créela con la estructura definida a continuación:
codigo sql
```sql
drop table empleados;
create table empleados(
    nombre varchar2(30),
    documento char(8),
    domicilio varchar2(30),
    seccion varchar2(20),
    sueldo number(6,2),
    cantidadhijos number(2),
    fechaingreso date,
    primary key(documento)
);
```
## 02 Ingrese algunos registros:
codigo sql
```sql
insert into empleados
values('Juan Perez','22333444','Colon 123','Gerencia',5000,2,'10/05/1980');

insert into empleados
values('Ana Acosta','23444555','Caseros 987','Secretaria',2000,0,'12/10/1980');

insert into empleados
values('Lucas Duarte','25666777','Sucre 235','Sistemas',4000,1,'25/05/1985');

insert into empleados
values('Pamela Gonzalez','26777888','Sarmiento 873','Secretaria',2200,3,'25/06/1990');

insert into empleados
values('Marcos Juarez','30000111','Rivadavia 801','Contaduria',3000,0,'01/05/1996');

insert into empleados
values('Yolanda Perez','35111222','Colon 180','Administracion',3200,1,'01/05/1996');

insert into empleados
values('Rodolfo Perez','35555888','Coronel Olmedo 588','Sistemas',4000,3,'01/05/1996');

insert into empleados
values('Martina Rodriguez','30141414','Sarmiento 1234','Administracion',3800,4,'01/09/2000');

insert into empleados
values('Andres Costa','28444555',default,'Secretaria',null,null,null);

```
## 03 Cuente la cantidad de empleados agrupados por sección (5 filas)
codigo sql
```sql
select seccion, count(*) from empleados group by seccion;
```
## 04 Calcule el promedio de hijos por sección (5 filas)
codigo sql
```sql
select seccion, count(cantidadhijos) from empleados group by seccion;
```
## 05 Cuente la cantidad de empleados agrupados por año de ingreso (6 filas)
codigo sql
```sql
select fechaingreso, count(*) from empleados group by fechaingreso;
```
## 06 Calcule el promedio de sueldo por sección de los empleados con hijos (4 filas)
codigo sql
```sql
SELECT seccion, AVG(sueldo)
FROM empleados
WHERE cantidadhijos > 0
GROUP BY seccion;
```