## Ejercicios propuestos
# Una empresa de remises tiene registrada la información de sus vehículos en una tabla llamada "remis".
# 01 Elimine la tabla:
codigo sql
```sql
drop table remis;
```
## 02 Cree la tabla con la siguiente estructura:
codigo sql
```sql
create table remis(
    numero number(5),
    patente char(6),
    marca varchar2(15),
    modelo char(4)
);
```
## 03 Ingrese algunos registros, 2 de ellos con patente repetida y alguno con patente nula.
codigo sql
```sql
INSERT INTO remis VALUES (1, 'ABC123', 'Toyota', '2020');
INSERT INTO remis VALUES (2, 'DEF456', 'Ford', '2018');
INSERT INTO remis VALUES (3, 'GHI789', 'Renault', '2019');
INSERT INTO remis VALUES (4, 'ABC123', 'Chevrolet', '2017');
INSERT INTO remis VALUES (5, NULL, 'Nissan', '2021');
```
## 04 Agregue una restricción "primary key" para el campo "numero".
codigo sql
```sql
ALTER TABLE remis ADD CONSTRAINT pk_numero_remis PRIMARY KEY (numero);
```
## 05 Intente agregar una restricción "unique" para asegurarse que la patente del remis no tomará valores repetidos. No se puede porque hay valores duplicados, un mensaje indica que se encontraron claves duplicadas.
codigo sql
```sql
ALTER TABLE remis ADD CONSTRAINT uk_patente_remis UNIQUE (patente);

```
## 06 Elimine el registro con patente duplicada y establezca la restricción. Note que hay 1 registro con valor nulo en "patente".
codigo sql
```sql
DELETE FROM remis WHERE ROWID NOT IN (
    SELECT MIN(ROWID) FROM remis GROUP BY patente HAVING COUNT(*) > 1
);
ALTER TABLE remis ADD CONSTRAINT uk_patente_remis UNIQUE (patente);
```
## 07 Intente ingresar un registro con patente repetida (no lo permite)
codigo sql
```sql
INSERT INTO remis VALUES (6, 'ABC123', 'Volkswagen', '2022');
```
## 08 Ingrese un registro con valor nulo para el campo "patente".
codigo sql
```sql
INSERT INTO remis VALUES (6, NULL, 'Hyundai', '2023');
```
## 09 Muestre la información de las restricciones consultando "user_constraints" y "user_cons_columns" y analice la información retornada (2 filas en cada consulta)    
codigo sql
```sql
ELECT constraint_name, constraint_type, table_name FROM user_constraints WHERE table_name = 'REMIS';
SELECT constraint_name, column_name FROM user_cons_columns WHERE table_name = 'REMIS';
```
