# Ejercicio 01
## Trabaje con la tabla "agenda" que almacena los datos de sus amigos.

## 01 Elimine la tabla y créela con la siguiente estructura:
codigo sql
```sql
drop table agenda;

create table agenda(
    apellido varchar2(30),
    nombre varchar2(20),
    domicilio varchar2(30),
    telefono varchar2(11)
);

```
salida sql
```sh
Table AGENDA dropped.


Table AGENDA created.
```
## 02 Ingrese los siguientes registros:
codigo sql
```sql
insert into agenda (apellido,nombre,domicilio,telefono) values ('Acosta','Alberto','Colon 123','4234567');
insert into agenda (apellido,nombre,domicilio,telefono) values ('Juarez','Juan','Avellaneda 135','4458787');
insert into agenda (apellido,nombre,domicilio,telefono) values ('Lopez','Maria','Urquiza 333','4545454');
insert into agenda (apellido,nombre,domicilio,telefono) values ('Lopez','Jose','Urquiza 333','4545454');
insert into agenda (apellido,nombre,domicilio,telefono) values ('Suarez','Susana','Gral. Paz 1234','4123456');

```
salida sql
```sh
1 row inserted.


1 row inserted.


1 row inserted.


1 row inserted.


1 row inserted.
```
## 03 Modifique el registro cuyo nombre sea "Juan" por "Juan Jose" (1 registro actualizado)
codigo sql
```sql
UPDATE agenda SET nombre = 'Juan Jose' WHERE nombre = 'Juan';
```
salida sql
```sh
1 row updated.
```
## 04 Actualice los registros cuyo número telefónico sea igual a "4545454" por "4445566" (2 registros)
codigo sql
```sql
UPDATE agenda SET telefono = '4445566' WHERE telefono = '4545454';
```
salida sql
```sh
2 rows updated.
```
## 05 Actualice los registros que tengan en el campo "nombre" el valor "Juan" por "Juan Jose" (ningún registro afectado porque ninguno cumple con la condición del "where")
codigo sql
```sql
UPDATE agenda SET nombre = 'Juan Jose' WHERE nombre = 'Juan';

```
salida sql
```sh
0 rows updated.
```
# Ejercicio 02
## Trabaje con la tabla "libros" de una librería.
## 01 Elimine la tabla y créela con los siguientes campos: titulo (cadena de 30 caracteres de longitud), autor (cadena de 20), editorial (cadena de 15) y precio (entero no mayor a 999.99):
codigo sql
```sql
drop table libros; 
create table libros (
    titulo varchar2(30),
    autor varchar2(20),
    editorial varchar2(15),
    precio number(5,2)
);
```
salida sql
```sh
Table LIBROS dropped.

Table LIBROS created.
```
## 02 Ingrese los siguientes registros:
codigo sql
```sql
insert into libros (titulo, autor, editorial, precio) values ('El aleph','Borges','Emece',25.00);
insert into libros (titulo, autor, editorial, precio) values ('Martin Fierro','Jose Hernandez','Planeta',35 50);
insert into libros (titulo, autor, editorial, precio) values ('Aprenda PHP','Mario Molina','Emece',45.50);
insert into libros (titulo, autor, editorial, precio) values ('Cervantes y el quijote','Borges','Emece',25);
insert into libros (titulo, autor, editorial, precio) values ('Matematica estas ahi','Paenza','Siglo XXI',15);
```
salida sql
```sh
1 row inserted.


1 row inserted.


1 row inserted.


1 row inserted.


1 row inserted.
```
## 03 Muestre todos los registros (5 registros)
codigo sql
```sql
select * from libros;
```
salida sql
```sh
TITULO AUTOR EDITORIAL PRECIO
```
## 04 Modifique los registros cuyo autor sea igual a "Paenza", por "Adrian Paenza" (1 registro)
codigo sql
```sql
update libros set autor='Adrian Paenza' where autor='Paenza';
```
salida sql
```sh
1 rows updated.
```
## 05 Nuevamente, modifique los registros cuyo autor sea igual a "Paenza", por "Adrian Paenza" (ningún registro afectado porque ninguno cumple la condición)
codigo sql
```sql
update libros set autor='Adrian Paenza' where autor='Paenza';
```
salida sql
```sh
0 rows update.
```
## 06 Actualice el precio del libro de "Mario Molina" a 27 pesos (1 registro)
codigo sql
```sql
update libros set precio = '27' where autor= 'Mario Molina';
```
salida sql
```sh
1 rows update
```
## 07 Actualice el valor del campo "editorial" por "Emece S.A.", para todos los registros cuya editorial sea igual a "Emece" (3 registros)
codigo sql
```sql
update editorial set Emece_S.A.;
```
salida sql
```sh

```
