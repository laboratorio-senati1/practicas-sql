## Ejercicios propuestos
# Un club dicta clases de distintos deportes. En una tabla llamada "socios" guarda los datos de los socios, en una tabla llamada "deportes" la información referente a los diferentes deportes que se dictan y en una tabla denominada "inscriptos", las inscripciones de los socios a los distintos deportes.
# Un socio puede inscribirse en varios deportes el mismo año. Un socio no puede inscribirse en el mismo deporte el mismo año. Distintos socios se inscriben en un mismo deporte en el mismo año.
## 01 Elimine las tablas:
codigo sql
```sql
drop table socios;
drop table deportes;
drop table inscriptos;

```
## 02 Cree las tablas con las siguientes estructuras:
codigo sql
```sql
create table socios(
    documento char(8) not null, 
    nombre varchar2(30),
    domicilio varchar2(30),
    primary key(documento)
);

create table deportes(
    codigo number(2),
    nombre varchar2(20),
    profesor varchar2(15),
    primary key(codigo)
);

create table inscriptos(
    documento char(8) not null, 
    codigodeporte number(2) not null,
    año char(4),
    matricula char(1),--'s'=paga, 'n'=impaga
    primary key(documento,codigodeporte,año)
);

```
## 03 Ingrese algunos registros en "socios":
codigo sql
```sql
insert into socios values('22222222','Ana Acosta','Avellaneda 111');
insert into socios values('23333333','Betina Bustos','Bulnes 222');
insert into socios values('24444444','Carlos Castro','Caseros 333');
insert into socios values('25555555','Daniel Duarte','Dinamarca 44');

```
## 04 Ingrese algunos registros en "deportes":
codigo sql
```sql
insert into deportes values(1,'basquet','Juan Juarez');
insert into deportes values(2,'futbol','Pedro Perez');
insert into deportes values(3,'natacion','Marina Morales');
insert into deportes values(4,'tenis','Marina Morales');

```
## 05 Inscriba a varios socios en el mismo deporte en el mismo año:
codigo sql
```sql
insert into inscriptos values ('22222222',3,'2016','s');
insert into inscriptos values ('23333333',3,'2016','s');
insert into inscriptos values ('24444444',3,'2016','n');

```
## 06 Inscriba a un mismo socio en el mismo deporte en distintos años:
codigo sql
```sql
insert into inscriptos values ('22222222',3,'2015','s');
insert into inscriptos values ('22222222',3,'2017','n');

```
## 07 Inscriba a un mismo socio en distintos deportes el mismo año:
codigo sql
```sql
insert into inscriptos values ('24444444',1,'2016','s');
insert into inscriptos values ('24444444',2,'2016','s');

```
## 08 Ingrese una inscripción con un código de deporte inexistente y un documento de socio que no exista en "socios":
codigo sql
```sql
insert into inscriptos values ('26666666',0,'2016','s');
```
## 09 Muestre el nombre del socio, el nombre del deporte en que se inscribió y el año empleando diferentes tipos de join (8 filas):
codigo sql
```sql
SELECT s.nombre AS nombre_socio, d.nombre AS nombre_deporte, i.año
FROM socios s
INNER JOIN inscriptos i ON s.documento = i.documento
INNER JOIN deportes d ON i.codigodeporte = d.codigo;
``` 
## 10 Muestre todos los datos de las inscripciones (excepto los códigos) incluyendo aquellas inscripciones cuyo código de deporte no existe en "deportes" y cuyo documento de socio no se encuentra en "socios" (10 filas)
codigo sql
```sql
SELECT i.documento, s.nombre, d.nombre, i.año, i.matricula
FROM inscriptos i
LEFT JOIN socios s ON i.documento = s.documento
LEFT JOIN deportes d ON i.codigodeporte = d.codigo;
```
## 11 Muestre todas las inscripciones del socio con documento "22222222" (3 filas)
codigo sql
```sql
SELECT i.documento, s.nombre, d.nombre, i.año, i.matricula
FROM inscriptos i
INNER JOIN socios s ON i.documento = s.documento
INNER JOIN deportes d ON i.codigodeporte = d.codigo
WHERE i.documento = '22222222';
```