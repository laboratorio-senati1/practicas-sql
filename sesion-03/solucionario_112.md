## Ejercicios propuestos
# Una empresa almacena los datos de sus empleados en una tabla denominada "empleados".
# 01 Elimine la tabla:
```sql
drop table empleados;
```
# 02 Cree la tabla con la siguiente estructura:
```sql
create table empleados(
    documento char(8),
    apellido varchar2(20),
    nombre varchar2(20),
    seccion varchar2(30),
    sueldo number(8,2)
);
```
# 03 Ingrese algunos registros: 
```sql
insert into empleados values('22333444','ACOSTA','Ana','Secretaria',500);
insert into empleados values('22555666','CASEROS','Carlos','Contaduria',900);
insert into empleados values('22777888','DOMINGUEZ','Daniel','Secretaria',560);
insert into empleados values('22999000','FUENTES','Federico','Sistemas',680);
insert into empleados values('23444555','GOMEZ','Gabriela','Sistemas',1200);
insert into empleados values('23666777','JUAREZ','Juan','Contaduria',1000);
```
# 04 La empresa necesita controlar cuando se le aumenta el sueldo a los empleados, guardando en una tabla denominada "control", el nombre del usuario, la fecha, el documento de quien se ha modificado el sueldo, el antiguo sueldo y el nuevo sueldo. Para ello cree la tabla control (antes elimínela por si existe):
```sql
drop table control;
create table control(
    usuario varchar2(30),
    fecha date,
    documento char(8),
    antiguosueldo number(8,2),
    nuevosueldo number(8,2)
);
```
# 05 Cree un disparador que almacene el nombre del usuario, la fecha, documento, el antiguo y el nuevo sueldo en "control" cada vez que se actualice un sueldo de la tabla "empleados" a un valor mayor. Si el sueldo se disminuye, el trigger no debe activarse. Si se modifica otro campo diferente de "sueldo", no debe activarse.
```sql
create or replace trigger tr_aumento_sueldo
before update on empleados
for each row
begin
  if :new.sueldo > :old.sueldo then
    insert into control(usuario, fecha, documento, antiguosueldo, nuevosueldo)
    values(USER, sysdate, :new.documento, :old.sueldo, :new.sueldo);
  end if;
end;
/
```
# 06 Actualice el sueldo de todos los empleados de la sección "Sistemas" a "1000"
```sql
update empleados
set sueldo = 1000
where seccion = 'Sistemas';
```
# 07 Consulte la tabla "control" para ver cuántas veces se ha disparado el trigger Se ha disparado una sola vez; se actualizaron 2 registros, pero en solo uno de ellos se aumentó el sueldo.
```sql
select * from control;
```
# 08 Al empleado con documento "22333444" se lo ha cambiado a la sección "contaduria". Realice el cambio en la tabla "empleados"
```sql
update empleados
set seccion = 'Contaduria'
where documento = '22333444';
```
# 09 Verifique que el trigger no se ha activado porque no se ha modificado el campo "sueldo". Consulte "control"
```sql
select * from control;
```
# 10 Cree un disparador a nivel de fila que se dispare cada vez que se ingrese un nuevo empleado y coloque en mayúsculas el apellido ingresado. Además, si no se ingresa sueldo, debe ingresar '0'
```sql
create or replace trigger tr_ingresar_empleados
before insert on empleados
for each row
begin
  :new.apellido := upper(:new.apellido);
  if :new.sueldo is null then
    :new.sueldo := 0;
  end if;
end;
/
```
# 11 Ingrese un nuevo empleado empleando minúsculas en el apellido
```sql
insert into empleados values('24555666','lopez','Luis','Contaduria',800);
```
# 12 Verifique que el trigger "tr_ingresar_empleados" se disparó
```sql
select * from empleados;
```
# 13 Ingrese dos nuevos empleados, uno sin sueldo y otro con sueldo "null"
```sql
insert into empleados values('24777888','gomez','Gonzalo','Sistemas',null);
insert into empleados(documento, apellido, nombre, seccion)
values('24999000', 'perez', 'Pedro', 'Secretaria');
```
# 14 Verifique que el trigger "tr_ingresar_empleados" se ha disparado Los dos registros deben tener el apellido en mayúsculas y deben tener el valor '0' en sueldo.
```sql
select * from empleados;

```
# 15 Cree un disparador a nivel de fila que se dispare cada vez que se ingresa un nuevo empleado y coloque "null" en "sueldo" si el sueldo ingresado supera los $1000 o es inferior a $500
```sql
create or replace trigger tr_ingresar_empleados2
before insert on empleados
for each row
begin
  if :new.sueldo > 1000 or :new.sueldo < 500 then
    :new.sueldo := null;
  end if;
end;
/
```
# 16 Ingrese un nuevo empleado con un sueldo que dispare el trigger creado anteriormente
```sql
insert into empleados values('25111222','gonzalez','Laura','Sistemas',1500);
```
# 17 Verifique que el trigger "tr_ingresar_empleados" se disparó
```sql
select * from empleados;
```