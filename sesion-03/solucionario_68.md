## Ejercicios propuestos
# Trabaje con una tabla llamada "empleados" y "secciones".
# 01 Elimine las tablas y créelas:
codigo sql
```sql
drop table empleados;
drop table secciones;

create table secciones(
    codigo number(2),
    nombre varchar(20),
    primary key (codigo)
);

create table empleados(
    apellido varchar2(20) not null,
    nombre varchar2(20),
    domicilio varchar2(30),
    seccion number(2),
    fechaingreso date,
    telefono number(7),
    constraint FK_empleados_seccion
    foreign key (seccion)
    references secciones(codigo)
    on delete set null
);
```
## 02 Modifique el tipo de dato del campo "telefono" a varchar(11) Oracle lo permite porque la tabla está vacía; si no lo estuviese, no lo permitiría.
codigo sql
```sql

```
## 03 Ingrese algunos registros en ambas tablas
codigo sql
```sql
insert into secciones values(8,'Secretaria');
insert into secciones values(9,'Contaduria');
insert into secciones values(10,'Sistemas');
insert into empleados values('Lopez','Luciano','Colon 123',8,'10/10/1980','4819977');
insert into empleados values('Gonzalez',null,'Avellaneda 222',9,'01/05/1990','4515566');
```
## 04 Modifique el campo "nombre" de "empleados" para que permita cadenas variables de 10 caracteres (menor longitud) y luego verifique el cambio. Oracle lo permite, porque los valores en tal campo son menores a 10.
codigo sql
```sql

```
## 05 Intente modificar el campo "nombre" de "empleados" para que permita cadenas variables de 6 caracteres (menor longitud) Oracle no lo permite, porque un valor en tal campo consta de 7 caracteres.
codigo sql
```sql

```
## 06 Elimine el registro correspondiente a "Lopez Luciano"
codigo sql
```sql

```
## 07 Modifique el campo "nombre" de "empleados" para que permita cadenas variables de 6 caracteres Oracle lo permite, la tabla no está vacía pero los registros contienen valor nulo en el campo "nombre".
codigo sql
```sql

```
## 08 Intente cambiar el tipo de dato del campo "codigo" de "secciones" a char(2) Oracle no lo permite porque tal campo es referenciado por una clave externa.
codigo sql
```sql

```
## 09 Cambie la longitud del campo "codigo" de "secciones" a 3. Oracle lo permite porque el cambio no afecta la restricción "foreign key" que referencia el campo "codigo".
codigo sql
```sql

```
## 10 Intente modificar el campo "nombre" de "empleados" para que no admita valores nulos. Mensaje de error, la tabla contiene valores nulos en tal campo.
codigo sql
```sql

```
## 11 Modifique el valor nulo por uno válido del campo "nombre" de "empleados" y luego realice la modificación del punto anterior.
codigo sql
```sql

```
## 12 Verifique que "nombre" ya no admite valores nulos
codigo sql
```sql

```
