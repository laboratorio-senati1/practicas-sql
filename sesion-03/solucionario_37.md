## Ejercicio 01
# Una empresa tiene registrados datos de sus empleados en una tabla llamada "empleados".
## 01 Elimine la tabla:
codigo sql
```sql
drop table empleados;

```
## 02 Créela con la siguiente estructura:
codigo sql
```sql
create table empleados (
    documento char(8),
    nombre varchar2(30),
    seccion varchar2(20)
);

```
## 03 Ingrese algunos registros, dos de ellos con el mismo número de documento:
codigo sql
```sql
INSERT INTO empleados VALUES ('22333444', 'Juan Perez', 'Administración');
INSERT INTO empleados VALUES ('23444555', 'Ana Acosta', 'Ventas');
INSERT INTO empleados VALUES ('22333444', 'Pedro Gomez', 'Recursos Humanos');
```
## 04 Intente establecer una restricción "primary key" para la tabla para que el documento no se repita ni admita valores nulos. No lo permite porque la tabla contiene datos que no cumplen con la restricción, debemos eliminar (o modificar) el registro que tiene documento duplicado.
codigo sql
```sql
ALTER TABLE empleados ADD CONSTRAINT pk_documento_empleados PRIMARY KEY (documento);
```
## 05 Establecezca la restricción "primary key" del punto 4
codigo sql
```sql
ALTER TABLE empleados ADD CONSTRAINT pk_empleados PRIMARY KEY (documento);
```
## 06 Intente actualizar un documento para que se repita. No lo permite porque va contra la restricción.
codigo sql
```sql
UPDATE empleados SET documento = '22333444' WHERE nombre = 'Ana Acosta';
```
## 07 Intente establecer otra restricción "primary key" con el campo "nombre".
codigo sql
```sql
ALTER TABLE empleados ADD CONSTRAINT pk_nombre_empleados PRIMARY KEY (nombre);

```
## 08 Vea las restricciones de la tabla "empleados" consultando el catálogo "user_constraints" (1 restricción "P")
codigo sql
```sql
SELECT constraint_name, constraint_type
FROM user_constraints
WHERE table_name = 'EMPLEADOS';
```
## 09 Consulte el catálogo "user_cons_columns"
codigo sql
```sql
SELECT constraint_name, column_name
FROM user_cons_columns
WHERE table_name = 'EMPLEADOS';
```
## Ejercicio 02
# Una empresa de remises tiene registrada la información de sus vehículos en una tabla llamada "remis".
## 01 Elimine la tabla:
codigo sql
```sql
drop table remis;
```
## 02 Cree la tabla con la siguiente estructura:
codigo sql
```sql
create table remis(
    numero number(5),
    patente char(6),
    marca varchar2(15),
    modelo char(4)
);

```
## 03 Ingrese algunos registros sin repetir patente y repitiendo número.
codigo sql
```sql
INSERT INTO remis VALUES (10001, 'ABC123', 'Ford', '2021');
INSERT INTO remis VALUES (10001, 'DEF456', 'Chevrolet', '2022');
INSERT INTO remis VALUES (10002, 'GHI789', 'Renault', '2020');
```
## 04 Ingrese un registro con patente nula.
codigo sql
```sql
INSERT INTO remis VALUES (10003, NULL, 'Volkswagen', '2019');

```
## 05 Intente definir una restricción "primary key" para el campo "numero".
codigo sql
```sql
ALTER TABLE remis ADD CONSTRAINT pk_numero_remis PRIMARY KEY (numero);

```
## 06 Intente establecer una restricción "primary key" para el campo "patente".
codigo sql
```sql
ALTER TABLE remis ADD CONSTRAINT pk_patente_remis PRIMARY KEY (patente);
```
## 07 Modifique el valor "null" de la patente.
codigo sql
```sql
UPDATE remis SET patente = 'JKL012' WHERE numero = 10003;

```
## 08 Establezca una restricción "primary key" del punto 6.
codigo sql
```sql
ALTER TABLE remis ADD CONSTRAINT pk_remis PRIMARY KEY (patente);

```
## 09 Vea la información de las restricciones consultando "user_constraints" (1 restricción "P")
codigo sql
```sql
SELECT constraint_name, constraint_type
FROM user_constraints
WHERE table_name = 'REMIS';
```
## 10 Consulte el catálogo "user_cons_columns" y analice la información retornada (1 restricción)
codigo sql
```sql
SELECT constraint_name, column_name
FROM user_cons_columns
WHERE table_name = 'REMIS';
```