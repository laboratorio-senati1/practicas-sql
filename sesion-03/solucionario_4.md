# ejercicio 01 
## Un videoclub que alquila películas en video almacena la información de sus películas en alquiler en una tabla llamada "peliculas".

## 01 Elimine la tabla, si existe:
codigo sql
```sql
drop table peliculas;
```
salida sql
```sh
Table PELICULAS dropped.
```
## 02 Crea la tabla 
codigo sql
```sql
create table peliculas(
  titulo varchar(20),
  actor varchar(20),
  duracion integer,
  cantidad integer
);
```
salida sql
```sh
Table PELICULAS created.
```
## 03 Vea la estructura de la tabla:
codigo sql
```sql
describe peliculas;
```
salida sql
```sh
Name     Null? Type         
-------- ----- ------------ 
TITULO         VARCHAR2(20) 
ACTOR          VARCHAR2(20) 
DURACION       NUMBER(38)   
CANTIDAD       NUMBER(38) 
```
## 04 Ingrese los siguientes registros:
codigo sql
```sql
 insert into peliculas (titulo, actor, duracion, cantidad) values ('Mision imposible','Tom Cruise',120,3);
 insert into peliculas (titulo, actor, duracion, cantidad) values ('Mision imposible 2','Tom Cruise',180,2);
 insert into peliculas (titulo, actor, duracion, cantidad) values ('Mujer bonita','Julia R.',90,3);
 insert into peliculas (titulo, actor, duracion, cantidad) values ('Elsa y Fred','China Zorrilla',90,2);
```
salida sql
```sh
1 row inserted.


1 row inserted.


1 row inserted.


1 row inserted.
```
## 05 Realice un "select" mostrando solamente el título y actor de todas las películas:
codigo sql
```sql
select titulo,actor from peliculas;
```
salida sql
```sh
TITULO               ACTOR               
-------------------- --------------------
Mision imposible     Tom Cruise          
Mision imposible 2   Tom Cruise          
Mujer bonita         Julia R.            
Elsa y Fred          China Zorrilla 
```
## 06 Muestre el título y duración de todas las peliculas.
codigo sql
```sql
select titulo,duracion from peliculas;
```
salida sql
```sh
TITULO                 DURACION
-------------------- ----------
Mision imposible            120
Mision imposible 2          180
Mujer bonita                 90
Elsa y Fred                  90
```c 
## 07 Muestre el título y la cantidad de copias.
codigo sql
```sql
select titulo, cantidad from peliculas;
```
salida sql
```sh
TITULO                 CANTIDAD
-------------------- ----------
Mision imposible              3
Mision imposible 2            2
Mujer bonita                  3
Elsa y Fred                   2

``` 

# ejercicio 02 
## Una empresa almacena los datos de sus empleados en una tabla llamada "empleados".

## 01 Elimine la tabla, si existe:
codigo sql
```sql
drop table empleados;
```
salida sql
```sh
Error starting at line : 1 in command -
drop table empleados
Error report -
ORA-00942: table or view does not exist
00942. 00000 -  "table or view does not exist"
*Cause:    
*Action:
```c 
## 02 Cree la tabla:
codigo sql
```sql
 create table empleados(
  nombre varchar(20),
  documento varchar(8), 
  sexo varchar(1),
  domicilio varchar(30),
  sueldobasico float
 );

```
salida sql
```sh
Table EMPLEADOS created.
```c 
## 03 Vea la estructura de la tabla:
codigo sql
```sql
Vea la estructura de la tabla:
```
salida sql
```sh
Name         Null? Type         
------------ ----- ------------ 
NOMBRE             VARCHAR2(20) 
DOCUMENTO          VARCHAR2(8)  
SEXO               VARCHAR2(1)  
DOMICILIO          VARCHAR2(30) 
SUELDOBASICO       FLOAT(126) 
```c 
## 04 Ingrese algunos registros:
codigo sql
```sql
insert into empleados (nombre, documento, sexo, domicilio, sueldobasico) values ('Juan Perez','22345678','m','Sarmiento 123',300);
insert into empleados (nombre, documento, sexo, domicilio, sueldobasico) values ('Ana Acosta','24345678','f','Colon 134',500);
insert into empleados (nombre, documento, sexo, domicilio, sueldobasico) values ('Marcos Torres','27345678','m','Urquiza 479',800);

```
salida sql
```sh
1 row inserted.


1 row inserted.


1 row inserted.
```c 
## 05 Muestre todos los datos de los empleados.
codigo sql
```sql
select * from empleados;
```
salida sql
```sh
NOMBRE               DOCUMENT S DOMICILIO                      SUELDOBASICO
-------------------- -------- - ------------------------------ ------------
Juan Perez           22345678 m Sarmiento 123                           300
Ana Acosta           24345678 f Colon 134                               500
Marcos Torres        27345678 m Urquiza 479                             800
```c 
## 06 Muestre el nombre y documento de los empleados.
codigo sql
```sql
select nombre,documento from empleados;
```
salida sql
```sh
NOMBRE               DOCUMENT
-------------------- --------
Juan Perez           22345678
Ana Acosta           24345678
Marcos Torres        27345678
```c 
## 07 Realice un "select" mostrando el nombre, documento y sueldo básico de todos los empleados.
codigo sql
```sql
select nombre,documento, sueldobasico from empleados;
```
salida sql
```sh
NOMBRE               DOCUMENT SUELDOBASICO
-------------------- -------- ------------
Juan Perez           22345678          300
Ana Acosta           24345678          500
Marcos Torres        27345678          800
```c 

# ejercicio 03 
#  Un comercio que vende artículos de computación registra la información de sus productos en la tabla llamada "articulos".
## 01 Elimine la tabla si existe:
codigo sql
```sql
drop table if exists articulos;

```
salida sql
```sh
Error starting at line : 1 in command -
drop table if exists articulos
Error report -
ORA-00933: SQL command not properly ended
00933. 00000 -  "SQL command not properly ended"
*Cause:    
*Action:
```c 
## 02 Cree la tabla "articulos" con los campos necesarios para almacenar los siguientes datos:
codigo sql
```sql
create table articulos (
     codigo int, 
     nombre varchar2(20),  
     descripcion varchar2(30),
     precio float
);

```
salida sql
```sh
Table ARTICULOS created.
```
## 03 Vea la estructura de la tabla (describe).
codigo sql
```sql
describe articulos;
```
salida sql
```sh
Name        Null? Type         
----------- ----- ------------ 
CODIGO            NUMBER(38)   
NOMBRE            VARCHAR2(20) 
DESCRIPCION       VARCHAR2(30) 
PRECIO            FLOAT(126)  
``` 
## 04 Ingrese algunos registros:
codigo sql
```sql
 insert into articulos (codigo, nombre, descripcion, precio)
  values (1,'impresora','Epson Stylus C45',400.80);
 insert into articulos (codigo, nombre, descripcion, precio)
  values (2,'impresora','Epson Stylus C85',500);
 insert into articulos (codigo, nombre, descripcion, precio)
  values (3,'monitor','Samsung 14',800);

```
salida sql
```sh
1 row inserted.


1 row inserted.


1 row inserted.

``` 
## 05 Muestre todos los campos de todos los registros.
codigo sql
```sql
select * from articulos;
```
salida sql
```sh
    CODIGO NOMBRE               DESCRIPCION                        PRECIO
---------- -------------------- ------------------------------ ----------
         1 impresora            Epson Stylus C45                    400.8
         2 impresora            Epson Stylus C85                      500
         3 monitor              Samsung 14                            800

``` 
## 06 Muestre sólo el nombre, descripción y precio.
codigo sql
```sql
select nombre,descripcion,precio from articulos;
```
salida sql
```sh
NOMBRE               DESCRIPCION                        PRECIO
-------------------- ------------------------------ ----------
impresora            Epson Stylus C45                    400.8
impresora            Epson Stylus C85                      500
monitor              Samsung 14                            800
```