## Ejercicios propuestos
# Un negocio almacena los datos de sus productos en una tabla denominada "productos". Dicha tabla contiene el código de producto, el precio, el stock mínimo que se necesita (cantidad mínima requerida antes de comprar más) y el stock actual (cantidad disponible en depósito). Si el stock actual es cero, es urgente reponer tal producto; menor al stock mínimo requerido, es necesario reponer tal producto; si el stock actual es igual o supera el stock minimo, está en estado normal.
# 01 Elimine la tabla "productos"
```sql
drop table productos;
```
# 02 Cree la tabla con la siguiente estructura:
```sql
create table productos(
    codigo_producto char(10),
    precio number(6,2),
    stock_minimo number(6,0),
    stock_actual number(6,0)
);

```
# 03 Ingrese algunos registros:
```sql
insert into productos values('P001', 10.50, 50, 100);
insert into productos values('P002', 5.75, 30, 25);
insert into productos values('P003', 8.99, 20, 0);
insert into productos values('P004', 15.25, 50, 45);
insert into productos values('P005', 12.50, 60, 70);

```
# 04 Cree una función que reciba dos valores numéricos correspondientes a ambos stosks. Debe comparar ambos stocks y retornar una cadena de caracteres indicando el estado de cada producto, si stock actual es:
 # cero: "faltante",
 # menor al stock mínimo: "reponer",
 # igual o superior al stock mínimo: "normal".
```sql
create or replace function fn_estado_producto(pstock_actual in number, pstock_minimo in number) return varchar2 is
begin
  if pstock_actual = 0 then
    return 'faltante';
  elsif pstock_actual < pstock_minimo then
    return 'reponer';
  else
    return 'normal';
  end if;
end;
/

```
# 05 Realice un "select" mostrando el código del producto, ambos stocks y, empleando la función creada anteriormente, una columna que muestre el estado del producto
```sql
select codigo_producto, stock_actual, stock_minimo, fn_estado_producto(stock_actual, stock_minimo) as estado
from productos;

```
# 06 Realice la misma función que en el punto 4, pero esta vez empleando en la estructura condicional la sintaxis "if... elsif...end if"
```sql
create or replace function fn_estado_producto(pstock_actual in number, pstock_minimo in number) return varchar2 is
begin
  if pstock_actual = 0 then
    return 'faltante';
  elsif pstock_actual < pstock_minimo then
    return 'reponer';
  else
    return 'normal';
  end if;
end;
/

```
# 07 Realice un "select" mostrando el código del producto, ambos stocks y, empleando la función creada anteriormente, una columna que muestre el estado del producto
```sql
select codigo_producto, stock_actual, stock_minimo, fn_estado_producto(stock_actual, stock_minimo) as estado
from productos;

```
# 08 Realice una función similar a las anteriores, pero esta vez, si el estado es "reponer" o "faltante", debe especificar la cantidad necesaria (valor necesario para llegar al stock mínimo)
```sql
create or replace function fn_estado_producto(pstock_actual in number, pstock_minimo in number) return varchar2 is
  vestado varchar2(20);
  vcantidad_necesaria number(6,0);
begin
  if pstock_actual = 0 then
    vestado := 'faltante';
  elsif pstock_actual < pstock_minimo then
    vestado := 'reponer';
    vcantidad_necesaria := pstock_minimo - pstock_actual;
    vestado := vestado || ' (' || vcantidad_necesaria || ')';
  else
    vestado := 'normal';
  end if;
  
  return vestado;
end;
/

```
# 09 Realice un "select" mostrando el código del producto, ambos stocks y, empleando la función creada anteriormente, una columna que muestre el estado del producto
```sql
select codigo_producto, stock_actual, stock_minimo, fn_estado_producto(stock_actual, stock_minimo) as estado
from productos;

```