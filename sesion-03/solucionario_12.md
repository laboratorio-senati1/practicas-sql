# Ejeercicio 01 
## Trabaje con la tabla "libros" de una librería.
 
## 01 Elimine la tabla:
codigo sql
```sql
drop table libros;
```
salida sql
```sh
Table LIBROS dropped.
```
## 02 Créela con los siguientes campos, estableciendo como clave primaria el campo "codigo":
codigo sql
```sql
create table libros(
    codigo number(4) not null,
    titulo varchar2(40) not null,
    autor varchar2(20),
    editorial varchar2(15),
    primary key (codigo)
);
```
salida sql
```sh
Table LIBROS created.
```
## 03 Ingrese los siguientes registros:
codigo sql
```sql
insert into libros (codigo,titulo,autor,editorial) values (1,'El aleph','Borges','Emece');
insert into libros (codigo,titulo,autor,editorial) values (2,'Martin Fierro','Jose Hernandez','Planeta');
insert into libros (codigo,titulo,autor,editorial) values (3,'Aprenda PHP','Mario Molina','Nuevo Siglo');
```
salida sql
```sh
1 row inserted.


1 row inserted.


1 row inserted.
```
## 04 Ingrese un registro con código repetido (aparece un mensaje de error)
codigo sql
```sql
insert into libros (codigo,titulo,autor,editorial) values (1,'El aleph','Borges','Emece');
```
salida sql
```sh
Error starting at line : 1 in command -
insert into libros (codigo,titulo,autor,editorial) values (1,'El aleph','Borges','Emece')
Error report -
ORA-00001: unique constraint (BRAYAN.SYS_C008397) violated
```
## 05 Intente ingresar el valor "null" en el campo "codigo"
codigo sql
```sql
insert into libros (codigo,titulo,autor,editorial) values (null,'El aleph','Borges','Emece');
```
salida sql
```sh
Error starting at line : 1 in command -
insert into libros (codigo,titulo,autor,editorial) values (null,'El ph','Bores','Emece')
Error at Command Line : 1 Column : 60
Error report -
SQL Error: ORA-01400: cannot insert NULL into ("BRAYAN"."LIBROS"."CODIGO")
01400. 00000 -  "cannot insert NULL into (%s)"
*Cause:    An attempt was made to insert NULL into previously listed objects.
*Action:   These objects cannot accept NULL values.
```
## 06 Intente actualizar el código del libro "Martin Fierro" a "1" (mensaje de error)
codigo sql
```sql
UPDATE libros SET codigo = '1' WHERE titulo = 'Martin Fierro';
```
salida sql
```sh
Error starting at line : 1 in command -
UPDATE libros SET codigo = '1' WHERE titulo = 'Martin Fierro'
Error report -
ORA-00001: unique constraint (BRAYAN.SYS_C008397) violated
```
## 07 Actualice el código del libro "Martin Fierro" a "10"
codigo sql
```sql
UPDATE libros SET codigo = '10' WHERE titulo = 'Martin Fierro';
```
salida sql
```sh
1 row updated.
```
## 08 Vea qué campo de la tabla "LIBROS" fue establecido como clave primaria
codigo sql
```sql
SHOW KEYS FROM LIBROS WHERE Key_name = 'PRIMARY';
```
salida sql
```sh
SP2-0158: unknown SHOW option "KEYS"
SP2-0158: unknown SHOW option "FROM"
SP2-0158: unknown SHOW option "LIBROS"
SP2-0158: unknown SHOW option "WHERE"
SP2-0158: unknown SHOW option "Key_name"
SP2-0158: unknown SHOW option "="
SP2-0158: unknown SHOW option "'PRIMARY'"
```
## 09 Vea qué campo de la tabla "libros" (en minúsculas) fue establecido como clave primaria
codigo sql
```sql
SELECT cols.column_name
FROM all_constraints cons
JOIN all_cons_columns cols ON cons.constraint_name = cols.constraint_name
WHERE cons.table_name = 'libros' AND cons.constraint_type = 'P';

```
salida sql
```sh
no rows selected
```
# ejercicio 02 
## Un instituto de enseñanza almacena los datos de sus estudiantes en una tabla llamada "alumnos".

## 01 Elimine la tabla "alumnos":
codigo sql
```sql
drop table alumnos;
```
salida sql
```sh
Error starting at line : 1 in command -
drop table alumnos
Error report -
ORA-00942: table or view does not exist
00942. 00000 -  "table or view does not exist"
*Cause:    
*Action:
```
## 02 Cree la tabla con la siguiente estructura intentando establecer 2 campos como clave primaria, el campo "documento" y "legajo":
codigo sql
```sql
create table alumnos(
    legajo varchar2(4) not null,
    documento varchar2(8),
    nombre varchar2(30),
    domicilio varchar2(30),
    primary key(codigo),
    primary key(documento)
);

```
salida sql
```sh
Error starting at line : 1 in command -
create table alumnos(
    legajo varchar2(4) not null,
    documento varchar2(8),
    nombre varchar2(30),
    domicilio varchar2(30),
    primary key(codigo),
    primary key(documento)
)
Error report -
ORA-02260: table can have only one primary key
02260. 00000 -  "table can have only one primary key"
*Cause:    Self-evident.
*Action:   Remove the extra primary key.
```
## 03 Cree la tabla estableciendo como clave primaria el campo "documento":
codigo sql
```sql
create table alumnos(
    legajo varchar2(4) not null,
    documento varchar2(8),
    nombre varchar2(30),
    domicilio varchar2(30),
    primary key(documento)
);

```
salida sql
```sh
Error starting at line : 1 in command -
create table alumnos(
    legajo varchar2(4) not null,
    documento varchar2(8),
    nombre varchar2(30),
    domicilio varchar2(30),
    primary key(documento)
)
Error report -
ORA-00955: name is already used by an existing object
00955. 00000 -  "name is already used by an existing object"
*Cause:    
*Action:
```
## 04 Verifique que el campo "documento" no admite valores nulos
codigo sql
```sql
SELECT nullable
FROM user_tab_columns
WHERE table_name = 'Alumnos' AND column_name = 'documento';
```
salida sql
```sh
no rows selected
``` 
## 05 Ingrese los siguientes registros:
codigo sql
```sql
insert into alumnos (legajo,documento,nombre,domicilio) values('A233','22345345','Perez Mariana','Colon 234');
insert into alumnos (legajo,documento,nombre,domicilio) values('A567','23545345','Morales Marcos','Avellaneda 348');

```
salida sql
```sh
1 row inserted.
```
## 06 Intente ingresar un alumno con número de documento existente (no lo permite)
codigo sql
```sql
insert into alumnos (legajo,documento,nombre,domicilio) values('A133','22345345','Romel','Colon 234');

```
salida sql
```sh
1 row inserted.
```
## 07 Intente ingresar un alumno con documento nulo (no lo permite)
codigo sql
```sql
insert into alumnos (legajo,documento,nombre,domicilio) values('A133',null,'Romel','Colon 234');

```
salida sql
```sh
Error starting at line : 1 in command -
insert into alumnos (legajo,documento,nombre,domicilio) values('A133',null,'Romel','Colon 234')
Error at Command Line : 1 Column : 71
Error report -
SQL Error: ORA-01400: cannot insert NULL into ("BRAYAN"."ALUMNOS"."DOCUMENTO")
01400. 00000 -  "cannot insert NULL into (%s)"
*Cause:    An attempt was made to insert NULL into previously listed objects.
*Action:   These objects cannot accept NULL values.

```
## 08 Vea el campo clave primaria de "ALUMNOS".
codigo sql
```sql
select uc.table_name, column_name from user_cons_columns ucc
    join user_constraints uc
    on ucc.constraint_name=uc.constraint_name
    where uc.constraint_type='P' and
    uc.table_name='ALUMNOS';

```
salida sql
```sh
TABLE_NAME
------------------------------------------------------------------------------------------------------------------------------
COLUMN_NAME
-----------------------------------------------------------------------------------------------------------------------------                                                                                                                  
ALUMNOS                                                                                                                          
DOCUMENTO                                                                                                                                                                                                               
```