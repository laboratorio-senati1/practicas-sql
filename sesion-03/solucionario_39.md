## Ejercicio 01
# Una empresa tiene registrados datos de sus empleados en una tabla llamada "empleados".

# 01 Elimine la tabla:
codigo sql
```sql
drop table empleados;
```
# 02 Créela con la siguiente estructura:
codigosql
```sql
create table empleados (
    documento char(8),
    nombre varchar2(30),
    cantidadhijos number(2),
    seccion varchar2(20),
    sueldo number(6,2) default -1
);
```
# 03 Agregue una restricción "check" para asegurarse que no se ingresen valores negativos para el sueldo.
# Note que el campo "sueldo" tiene establecido un valor por defecto (el valor -1) que va contra la restricción; Oracle no controla esto, permite establecer la restricción, pero al intentar ingresar un registro con el valor por defecto en tal campo, muestra un mensaje de error.
codigo sql
```sql
ALTER TABLE empleados ADD CONSTRAINT CK_empleados_sueldo_positivo CHECK (sueldo >= 0);

```

# 04 Intente ingresar un registro con la palabra clave "default" en el campo "sueldo" (mensaje de error)
codigo sql
```sql
INSERT INTO empleados VALUES ('12345678', 'Juan Perez', 2, 'Sistemas', DEFAULT);

```

# 05 Ingrese algunos registros válidos:
codigo sql
```sql
insert into empleados values ('22222222','Alberto Lopez',1,'Sistemas',1000);
insert into empleados values ('33333333','Beatriz Garcia',2,'Administracion',3000);
insert into empleados values ('34444444','Carlos Caseres',0,'Contaduría',6000);
```

# 06 Intente agregar otra restricción "check" al campo sueldo para asegurar que ninguno supere el valor 5000. La sentencia no se ejecuta porque hay un sueldo que no cumple la restricción.
codigo sql
```sql
ALTER TABLE empleados ADD CONSTRAINT CK_empleados_sueldo_max CHECK (sueldo <= 5000);

```

# 07 Elimine el registro infractor y vuelva a crear la restricción

codigo sql
```sql
DELETE FROM empleados WHERE documento = '12345678';
ALTER TABLE empleados DROP CONSTRAINT CK_empleados_sueldo_max;
ALTER TABLE empleados ADD CONSTRAINT CK_empleados_sueldo_max CHECK (sueldo <= 5000);
```
# 08 Establezca una restricción "check" para "seccion" que permita solamente los valores "Sistemas", "Administracion" y "Contaduría".

codigo sql
```sql
ALTER TABLE empleados ADD CONSTRAINT CK_empleados_seccion CHECK (seccion IN ('Sistemas', 'Administracion', 'Contaduría'));

```
# 09 Ingrese un registro con valor "null" en el campo "seccion".
codigo sql
```sql
INSERT INTO empleados VALUES ('87654321', 'Maria Rodriguez', 0, NULL, 2000);

```

# 10 Establezca una restricción "check" para "cantidadhijos" que permita solamente valores entre 0 y 15.
codigo sql
```sql
ALTER TABLE empleados ADD CONSTRAINT CK_empleados_cantidadhijos CHECK (cantidadhijos >= 0 AND cantidadhijos <= 15);

```

# 11 Vea todas las restricciones de la tabla (4 filas)
codigo sql
```sql
SELECT constraint_name, constraint_type, search_condition
FROM user_constraints
WHERE table_name = 'EMPLEADOS';
```

# 12 Intente agregar un registro que vaya contra alguna de las restricciones al campo "sueldo". Mensaje de error porque se infringe la restricción "CK_empleados_sueldo_positivo".
codigo sql
```sql
INSERT INTO empleados VALUES ('11111111', 'Pedro Gomez', 1, 'Recursos Humanos', -500);

```

# 13  Intente modificar un registro colocando en "cantidadhijos" el valor "21".
codigo sql
```sql
UPDATE empleados SET cantidadhijos = 21 WHERE documento = '87654321';

```

# 14  Intente modificar el valor de algún registro en el campo "seccion" cambiándolo por uno que no esté incluido en la lista de permitidos.
codigo sql
```sql
UPDATE empleados SET seccion = 'Ventas' WHERE documento = '87654321';

```

# 15  Intente agregar una restricción al campo sección para aceptar solamente valores que comiencen con la letra "B".
# Note que NO se puede establecer esta restricción porque va en contra de la establecida anteriormente para el mismo campo, si lo permitiera, no podríamos ingresar ningún valor para "seccion".
codigo sql
```sql
ALTER TABLE empleados ADD CONSTRAINT CK_empleados_seccion_letraB CHECK (seccion LIKE 'B%');

```

# 16 Agregue un registro con documento nulo.
codigo sql
```sql
INSERT INTO empleados VALUES (NULL, 'Luisa Martinez', 0, 'Ventas', 1500);

```

# 17  Intente agregar una restricción "primary key" para el campo "documento". No lo permite porque existe un registro con valor nulo en tal campo.
codigo sql
```sql
ALTER TABLE empleados ADD CONSTRAINT PK_empleados_documento PRIMARY KEY (documento);

```

# 18  Elimine el registro que infringe la restricción y establezca la restricción del punto 17.
codigo sql
```sql
DELETE FROM empleados WHERE documento IS NULL;
ALTER TABLE empleados DROP CONSTRAINT PK_empleados_documento;
ALTER TABLE empleados ADD CONSTRAINT PK_empleados_documento PRIMARY KEY (documento);
```

# 19 Consulte "user_constraints", mostrando los campos "constraint_name", "constraint_type" y "search_condition" de la tabla "empleados" (5 filas)
codigo sql
```sql
SELECT constraint_name, constraint_type, search_condition
FROM user_constraints
WHERE table_name = 'EMPLEADOS';
```

# 20  Consulte el catálogo "user_cons_colums" recuperando el nombre de las restricciones establecidas en el campo sueldo de la tabla "empleados" (2 filas)

codigo sql
```sql
SELECT constraint_name
FROM user_cons_columns
WHERE table_name = 'EMPLEADOS' AND column_name = 'SUELDO';
```

## Ejercicio 02
# Una playa de estacionamiento almacena los datos de los vehículos que ingresan en la tabla llamada "vehiculos".

# 01 Setee el formato de "date" para que nos muestre día, mes, año, hora y minutos:
codigo sql
```sql
alter session set NLS_DATE_FORMAT = 'DD/MM/YYYY HH24:MI';
```
# 02 Elimine la tabla "vehiculos" y créela con la siguiente estructura:
codigo sql
```sql
drop table vehiculos;

create table vehiculos(
    numero number(5),
    patente char(6),
    tipo char(4),
    fechahoraentrada date,
    fechahorasalida date
);
```
# 03 Ingrese algunos registros:
codigo sql
```sql
insert into vehiculos values(1,'AIC124','auto','17/01/2017 8:05','17/01/2017 12:30');
insert into vehiculos values(2,'CAA258','auto','17/01/2017 8:10',null);
insert into vehiculos values(3,'DSE367','moto','17/01/2017 8:30','17/01/2017 18:00');
```
# 04 Agregue una restricción de control que especifique que el campo "tipo" acepte solamente los valores "auto" y "moto":
codigo sql
```sql
alter table vehiculos
add constraint CK_vehiculos_tipo_valores
check (tipo in ('auto','moto'));
```
# 05  Intente modificar el valor del campo "tipo" ingresando un valor inexistente en la lista de valores permitidos por la restricción establecida a dicho campo.
codigo sql
```sql
UPDATE vehiculos SET tipo = 'camioneta' WHERE numero = 1;

```

# 06 Ingrese un registro con valor nulo para "tipo".
codigo sql
```sql
INSERT INTO vehiculos VALUES(4, 'EER789', NULL, TO_DATE('17/01/2017 9:00', 'DD/MM/YYYY HH24:MI'), NULL);

```

# 07 Agregue una restricción de control al campo "fechahoraentrada" que establezca que sus valores no sean posteriores a "fechahorasalida".
codigo sql
```sql
ALTER TABLE vehiculos ADD CONSTRAINT CK_vehiculos_fechas CHECK (fechahoraentrada <= fechahorasalida);

```

# 08 Intente modificar un registro para que la salida sea anterior a la entrada.
codigo sql
```sql
UPDATE vehiculos SET fechahorasalida = TO_DATE('17/01/2017 7:30', 'DD/MM/YYYY HH24:MI') WHERE numero = 1;

```

# 09 Vea todas las restricciones para la tabla "vehiculos".

codigo sql
```sql
SELECT constraint_name, constraint_type, search_condition
FROM user_constraints
WHERE table_name = 'VEHICULOS';
```
# 10 Ingrese un registro con valor nulo para "fechahoraentrada".
codigo sql
```sql
INSERT INTO vehiculos VALUES(5, 'FGR456', 'auto', NULL, TO_DATE('17/01/2017 12:00', 'DD/MM/YYYY HH24:MI'));

```

# 11 Vea todos los registros.
codigo sql
```sql
SELECT * FROM vehiculos;

```

# 12 Consulte "user_cons_columns" y analice la información retornada.
codigo sql
```sql
SELECT *
FROM user_cons_columns
WHERE table_name = 'VEHICULOS';
```
