## Ejercicios propuestos
# Trabaje con una tabla llamada "empleados"
# 01 Elimine la tabla y créela: 
```sql
drop table empleados;

create table empleados(
    documento char(8) not null,
    nombre varchar2(10),
    domicilio varchar2(30),
    ciudad varchar2(20) default 'Buenos Aires'
);
```
# 02 Agregue el campo "legajo" de tipo number(3) y una restricción "primary key"
```sql
ALTER TABLE empleados
ADD legajo NUMBER(3),
ADD CONSTRAINT pk_empleados PRIMARY KEY (legajo);
```
# 03 Vea si la estructura cambió y si se agregó la restricción.
```sql
DESCRIBE empleados;
```
# 04 Agregue el campo "hijos" de tipo number(2) y en la misma sentencia una restricción "check" que no permita valores superiores a 30
```sql
ALTER TABLE empleados
ADD hijos NUMBER(2) CONSTRAINT check_hijos CHECK (hijos <= 30);
```
# 05 Ingrese algunos registros:
```sql
insert into empleados values('22222222','Juan Lopez','Colon 123','Cordoba',100,2);
insert into empleados values('23333333','Ana Garcia','Sucre 435','Cordoba',200,3);

```
# 06 Intente agregar el campo "sueldo" de tipo number(6,2) no nulo y una restricción "check" que no permita valores negativos para dicho campo. No lo permite porque no damos un valor por defecto para dicho campo no nulo y los registros existentes necesitan cargar un valor.
```sql
ALTER TABLE empleados
ADD sueldo NUMBER(6,2);

ALTER TABLE empleados
MODIFY sueldo NUMBER(6,2) NOT NULL,
ADD CONSTRAINT check_sueldo CHECK (sueldo >= 0);
```
# 07 Agregue el campo "sueldo" de tipo number(6,2) no nulo, con el valor por defecto 0 y una restricción "check" que no permita valores negativos para dicho campo
```sql
ALTER TABLE empleados
ADD sueldo NUMBER(6,2) DEFAULT 0 NOT NULL,
    ADD CONSTRAINT check_sueldo CHECK (sueldo >= 0);

```
# 08 Recupere los registros
```sql
select *from empleados;
```
# 09 Vea la nueva estructura de la tabla
```sql
DESCRIBE empleados;
```
# 10 Vea las restricciones
```sql
SELECT CONSTRAINT_NAME, CONSTRAINT_TYPE
FROM USER_CONSTRAINTS
WHERE TABLE_NAME = 'EMPLEADOS';
```