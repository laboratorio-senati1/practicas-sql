## Ejercicios propuestos
# Una empresa tiene registrados datos de sus empleados en una tabla llamada "empleados".
## 01 Elimine la tabla:
codigo sql
```sql
drop table empleados;
```
## 02 Créela con la siguiente estructura e ingrese los registros siguientes:
codigo sql
```sql
create table empleados (
    codigo number(6),
    documento char(8),
    nombre varchar2(30),
    seccion varchar2(20),
    sueldo number(6,2)
);

insert into empleados
values (1,'22222222','Alberto Acosta','Sistemas',-10);

insert into empleados
values (2,'33333333','Beatriz Benitez','Recursos',3000);

insert into empleados
values (3,'34444444','Carlos Caseres','Contaduria',4000);

```
## 03 Intente agregar una restricción "check" para asegurarse que no se ingresen valores negativos para el sueldo sin especificar validación ni estado:
codigo sql
```sql
alter table empleados
add constraint CK_empleados_sueldo_positivo
check (sueldo>=0);

```
## 04 Vuelva a intentarlo agregando la opción "novalidate".
codigo sql
```sql
ALTER TABLE empleados ADD CONSTRAINT CK_empleados_sueldo_positivo CHECK (sueldo >= 0) NOVALIDATE;

```
## 05 Intente ingresar un valor negativo para sueldo.
codigo sql
```sql
INSERT INTO empleados VALUES (4, '55555555', 'David Delgado', 'Ventas', -1000);

```
## 06 Deshabilite la restricción e ingrese el registro anterior.
codigo sql
```sql
ALTER TABLE empleados DISABLE CONSTRAINT CK_empleados_sueldo_positivo;

INSERT INTO empleados VALUES (4, '45555555', 'David Duran', 'Ventas', -2000);

```
## 07 Intente establecer una restricción "check" para "seccion" que permita solamente los valores "Sistemas", "Administracion" y "Contaduría" sin especificar validación:
codigo sql
```sql
alter table empleados
add constraint CK_empleados_seccion_lista
check (seccion in ('Sistemas','Administracion','Contaduria'));
```
## 08 Establezca la restricción anterior evitando que se controlen los datos existentes.
codigo sql
```sql
ALTER TABLE empleados ADD CONSTRAINT CK_empleados_seccion_lista CHECK (seccion IN ('Sistemas', 'Administracion', 'Contaduria')) NOVALIDATE;

```
## 09 Vea si las restricciones de la tabla están o no habilitadas y validadas. Muestra 2 filas, una por cada restricción; ambas son de control, ninguna valida los datos existentes, "CK_empleados_sueldo_positivo" está deshabilitada, la otra habilitada.
codigo sql
```sql
SELECT constraint_name, constraint_type, status, validated
FROM user_constraints
WHERE table_name = 'EMPLEADOS';

```
## 10 Habilite la restricción deshabilitada.Note que existe un sueldo que infringe la condición.
codigo sql
```sql
ALTER TABLE empleados ENABLE CONSTRAINT CK_empleados_sueldo_positivo;

```
## 11 Intente modificar la sección del empleado "Carlos Caseres" a "Recursos" No lo permite.
codigo sql
```sql
UPDATE empleados SET seccion = 'Recursos' WHERE nombre = 'Carlos Caseres';

```
## 12 Deshabilite la restricción para poder realizar la actualización del punto precedente.
codigo sql
```sql
ALTER TABLE empleados DISABLE CONSTRAINT CK_empleados_seccion_lista;

UPDATE empleados SET seccion = 'Recursos' WHERE nombre = 'Carlos Caseres';

```
## 13 Agregue una restricción "primary key" para el campo "codigo" deshabilitada.
codigo sql
```sql
ALTER TABLE empleados ADD CONSTRAINT PK_empleados_codigo PRIMARY KEY (codigo) DISABLE;

```
## 14 Ingrese un registro con código existente.
codigo sql
```sql
INSERT INTO empleados VALUES (1, '77777777', 'Eduardo Escobar', 'Ventas', 5000);

```
## 15 Intente habilitar la restricción. No se permite porque aun cuando se especifica que no lo haga, Oracle verifica los datos existentes, y existe un código repetido.
codigo sql
```sql
ALTER TABLE empleados ENABLE CONSTRAINT PK_empleados_codigo NOVALIDATE;

```
## 16 Modifique el registro con clave primaria repetida.
codigo sql
```sql
UPDATE empleados SET codigo = 4 WHERE codigo = 1;

```
## 17 Habilite la restricción "primary key"
codigo sql
```sql
ALTER TABLE empleados ENABLE CONSTRAINT PK_empleados_codigo;

```
## 18 Agregue una restricción "unique" para el campo "documento"
codigo sql
```sql
ALTER TABLE empleados ADD CONSTRAINT UQ_empleados_documento UNIQUE (documento);

```
## 19 Vea todas las restricciones de la tabla "empleados" Muestra las 4 restricciones: 2 de control (1 habilitada y la otra no, no validan datos existentes), 1 "primary key" (habilitada y no valida datos existentes) y 1 única (habilitada y valida los datos anteriores).
codigo sql
```sql
SELECT constraint_name, constraint_type, status, validated
FROM user_constraints
WHERE table_name = 'EMPLEADOS';

```
## 20 Deshabilite todas las restricciones de "empleados"
codigo sql
```sql
ALTER TABLE empleados DISABLE CONSTRAINT ALL;

```
## 21 Ingrese un registro que viole todas las restricciones.
codigo sql
```sql
INSERT INTO empleados VALUES (5, '77777777', 'Eduardo Escobar', 'Recursos', -5000);

```
## 22 Habilite la restricción "CK_empleados_sueldo_positivo" sin validar los datos existentes.
codigo sql
```sql
ALTER TABLE empleados ENABLE CONSTRAINT CK_empleados_sueldo_positivo NOVALIDATE;

```
## 23 Habilite la restricción "CK_empleados_seccion_lista" sin validar los datos existentes.
codigo sql
```sql
ALTER TABLE empleados ENABLE CONSTRAINT CK_empleados_seccion_lista NOVALIDATE;

```
## 24 Intente habilitar la restricción "PK_empleados_codigo" sin validar los datos existentes.
codigo sql
```sql
ALTER TABLE empleados ENABLE CONSTRAINT PK_empleados_codigo NOVALIDATE;

```
## 25 Intente habilitar la restricción "UQ_empleados_documento" sin validar los datos existentes.
codigo sql
```sql
ALTER TABLE empleados ENABLE CONSTRAINT UQ_empleados_documento NOVALIDATE;

```
## 26 Elimine el registro que infringe con las restricciones "primary key" y "unique".
codigo sql
```sql
DELETE FROM empleados WHERE codigo = 4;

```
## 27 Habilite las restricciones "PK_empleados_codigo" y "UQ_empleados_documento" sin validar los datos existentes.
codigo sql
```sql
ALTER TABLE empleados ENABLE CONSTRAINT PK_empleados_codigo NOVALIDATE;
ALTER TABLE empleados ENABLE CONSTRAINT UQ_empleados_documento NOVALIDATE;

```
## 28 Consulte el catálogo "user_constraints" y analice la información.
codigo sql
```sql
SELECT constraint_name, constraint_type, status, validated
FROM user_constraints
WHERE table_name = 'EMPLEADOS';

```
