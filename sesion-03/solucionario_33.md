## Ejercicio 01
# Un consultorio médico en el cual trabajan 3 médicos registra las consultas de los pacientes en una tabla llamada "consultas".

## 01 Elimine la tabla:
codigo sql
```sql
drop table consultas
```
## 02 La tabla contiene los siguientes datos:

# fechayhora: date not null, fecha y hora de la consulta,
# medico: varchar2(30), not null, nombre del médico (Perez,Lopez,Duarte),
# documento: char(8) not null, documento del paciente,
# paciente: varchar2(30), nombre del paciente,
# obrasocial: varchar2(30), nombre de la obra social (IPAM,PAMI, etc.).
codigo sql
```sql
CREATE TABLE consultas (
  fechayhora DATE NOT NULL,
  medico VARCHAR2(30) NOT NULL,
  documento CHAR(8) NOT NULL,
  paciente VARCHAR2(30),
  obrasocial VARCHAR2(30)
);
```
## 03 Setee el formato de "date" para que nos muestre día, mes, año, hora y minutos:
codigo sql
```sql
AL.TER SESSION SET NLS_DATE_FORMAT = 'DD/MM/YYYY HH24:MI';
```
## 04 Un médico sólo puede atender a un paciente en una fecha y hora determinada. En una fecha y hora determinada, varios médicos atienden a distintos pacientes. Cree la tabla definiendo una clave primaria compuesta:
codigo sql
```sql
create table consultas(
    fechayhora date not null,
    medico varchar2(30) not null,
    documento char(8) not null,
    paciente varchar2(30),
    obrasocial varchar2(30),
    primary key(fechayhora,medico)
);

```
## 05 Ingrese varias consultas para un mismo médico en distintas horas el mismo día:
codigo sql
```sql
insert into consultas
values ('05/11/2006 8:00','Lopez','12222222','Acosta Betina','PAMI');

insert into consultas
values ('05/11/2006 8:30','Lopez','23333333','Fuentes Carlos','PAMI');

```
## 06 Ingrese varias consultas para diferentes médicos en la misma fecha y hora:
codigo sql
```sql
insert into consultas
values ('05/11/2006 8:00','Perez','34444444','Garcia Marisa','IPAM');

insert into consultas
values ('05/11/2006 8:00','Duarte','45555555','Pereyra Luis','PAMI');

```
## 07 Intente ingresar una consulta para un mismo médico en la misma hora el mismo día (mensaje de error)
codigo sql
```sql
insert into consultas
values ('05/11/2006 8:00','Duarte','45555555','Pereyra Luis','PAMI');
```
## 08 Intente cambiar la hora de la consulta de "Acosta Betina" por una no disponible ("8:30") (error)
codigo sql
```sql
insert into consultas
values ('05/11/2006 8:30','Lopez','12222222','Acosta Betina','PAMI');
```
## 09 Cambie la hora de la consulta de "Acosta Betina" por una disponible ("9:30")
codigo sql
```sql
insert into consultas
values ('05/11/2006 9:30','Lopez','12222222','Acosta Betina','PAMI');
```
## 10 Ingrese una consulta para el día "06/11/2006" a las 10 hs. para el doctor "Perez"
codigo sql
```sql
insert into consultas
values ('06/11/2006 10:00','Perez','12222222','Acosta Betina','PAMI');
```
## 11 Recupere todos los datos de las consultas de "Lopez" (3 registros)
codigo sql
```sql
SELECT *
FROM consultas
WHERE medico = 'Lopez';
```
## 12 Recupere todos los datos de las consultas programadas para el "05/11/2006 8:00" (2 registros)
codigo sql
```sql
SELECT * FROM consultas
WHERE fechayhora = TO_DATE('05/11/2006 8:00', 'DD/MM/YYYY HH24:MI');

```
## 13 Muestre día y mes de todas las consultas de "Lopez"
codigo sql
```sql
SELECT EXTRACT(DAY FROM fechayhora) AS dia, EXTRACT(MONTH FROM fechayhora) AS mes
FROM consultas
WHERE medico = 'Lopez';
```
## Ejercicio 02
# Un club dicta clases de distintos deportes. En una tabla llamada "inscriptos" almacena la información necesaria.
## 01 Elimine la tabla "inscriptos":
codigo sql
```sql
drop table inscriptos;
```
## 02 La tabla contiene los siguientes campos:

# documento del socio alumno: char(8) not null
# nombre del socio: varchar2(30),
# nombre del deporte (tenis, futbol, natación, basquet): varchar2(15) not null,
# año de inscripcion: date,
# matrícula: si la matrícula ha sido o no pagada ('s' o 'n').
codigo sql
```sql
CREATE TABLE inscriptos (
    documento char(8) not null,
    nombre varchar2(30),
    deporte varchar2(15) not null,
    año date,
    matricula char(1),
);
```
## 03 Necesitamos una clave primaria que identifique cada registro. Un socio puede inscribirse en varios deportes en distintos años. Un socio no puede inscribirse en el mismo deporte el mismo año. Varios socios se inscriben en un mismo deporte en distintos años. Cree la tabla con una clave compuesta:
codigo sql
```sql
create table inscriptos(
    documento char(8) not null,
    nombre varchar2(30),
    deporte varchar2(15) not null,
    año date,
    matricula char(1),
    primary key(documento,deporte,año)
);
```
## 04 Setee el formato de "date" para que nos muestre solamente el año (no necesitamos las otras partes de la fecha ni la hora)
codigo sql
```sql
SELECT TO_CHAR(fecha, 'YYYY') AS año
FROM tu_tabla;
```
## 05 Inscriba a varios alumnos en el mismo deporte en el mismo año:
codigo sql
```sql
insert into inscriptos
values ('12222222','Juan Perez','tenis','2005','s');

insert into inscriptos
values ('23333333','Marta Garcia','tenis','2005','s');

insert into inscriptos
values ('34444444','Luis Perez','tenis','2005','n');

```
## 06 Inscriba a un mismo alumno en varios deportes en el mismo año:
codigo sql
```sql
insert into inscriptos
values ('12222222','Juan Perez','futbol','2005','s');

insert into inscriptos
values ('12222222','Juan Perez','natacion','2005','s');

insert into inscriptos
values ('12222222','Juan Perez','basquet','2005','n');

```
## 07 Ingrese un registro con el mismo documento de socio en el mismo deporte en distintos años:
codigo sql
```sql
insert into inscriptos
values ('12222222','Juan Perez','tenis','2006','s');

insert into inscriptos
values ('12222222','Juan Perez','tenis','2007','s');

```
## 08 Intente inscribir a un socio alumno en un deporte en el cual ya esté inscripto en un año en el cual ya se haya inscripto (mensaje de error)
codigo sql
```sql
insert into inscriptos
values ('12222222','Juan Perez','basquet','2005','n');
```
## 09 Intente actualizar un registro para que la clave primaria se repita (error)
codigo sql
```sql
SELECT nombre, deporte
FROM inscriptos
WHERE TO_CHAR(anio_inscripcion, 'YYYY') = '2005';
```
## 10 Muestre los nombres y años de los inscriptos en "tenis" (5 registros)
codigo sql
```sql
SELECT nombre, TO_CHAR(anio_inscripcion, 'YYYY') AS año
FROM inscriptos
WHERE deporte = 'tenis';
```
## 11 Muestre los nombres y deportes de los inscriptos en el año 2005 (6 registros)
codigo sql
```sql
SELECT nombre, deporte
FROM inscriptos
WHERE TO_CHAR(anio_inscripcion, 'YYYY') = '2005';
```
## 12 Muestre el deporte y año de todas las inscripciones del socio documento "12222222" (6 registros)
codigo sql
```sql
SELECT deporte, TO_CHAR(anio_inscripcion, 'YYYY') AS año
FROM inscriptos
WHERE documento = '12222222';

```
