# ejercicio01
## 01 Elimine la tabla "agenda" Si no existe, un mensaje indicará tal situación.

## 02 Intente crear una tabla llamada "*agenda" 
codigo sql 
```sql
create table*agenda(
    apellido varchar2(30),
    nombre varchar2(20),
    domicilio varchar2(30),
    telefono varchar2(11)
);
```
salida sql
```sh
Error starting at line : 1 in command -
create table*agenda(
    apellido varchar2(30),
    nombre varchar2(20),
    domicilio varchar2(30),
    telefono varchar2(11)
)
Error report -
ORA-00903: invalid table name
00903. 00000 -  "invalid table name"
*Cause:    
*Action:
```
## 03 Cree una tabla llamada "agenda", debe tener los siguientes campos: apellido, varchar2(30); nombre, varchar2(20); domicilio, varchar2 (30) y telefono, varchar2(11)
codigo sql
```sql
create table agenda(
    apellido varchar2(30),
    nombre varchar2(20),
    domicilio varchar2(30),
    telefono varchar2(11)
);
```
salida sql
```sh
Table AGENDA created.
```
## 04 Intente crearla nuevamente. Aparece mensaje de error indicando que el nombre ya lo tiene otro objeto.
codigo sql 
```sql
 create table agenda(
    apellido varchar2(30),
    nombre varchar2(20),
    domicilio varchar2(30),
    telefono varchar2(11)
);

```
salida sql 
```sh
Error report -
ORA-00955: name is already used by an existing object
00955. 00000 -  "name is already used by an existing object"
*Cause:    
*Action:
```
## 05 Visualice las tablas existentes (all_tables) La tabla "agenda" aparece en la lista.
codigo sql
```sql
select * from all_tables;
```
salida sql
```sql
si existe
```

# ejercicio2
## 01 Elimine la tabla "libros" Si no existe, un mensaje indica tal situación.
codigo sql 
```sql
drop table libros;
```
salida sql
```sh
Error starting at line : 1 in command -
drop table libros
Error report -
ORA-00942: table or view does not exist
00942. 00000 -  "table or view does not exist"
*Cause:    
*Action:
```
## 02 Verifique que la tabla "libros" no existe (all_tables) No aparece en la lista.
codigo sql
```sql
select * from all_tables;
```
salida sql
```sh
no existe la tabla libros 
```

## 03 Cree una tabla llamada "libros". Debe definirse con los siguientes campos: titulo, varchar2(20); autor, varchar2(30) y editorial, varchar2(15)
codigo sql  
```sql
create table libros (
     titulo varchar2(20), 
     autor varchar2(30),  
     editorial varchar2(15)
);
```
salida sql 
```sh
Table LIBROS created.
```
## 04 Intente crearla nuevamente: Aparece mensaje de error indicando que existe un objeto con el nombre "libros".
codigo sql 
```qsl
create table libros (
     titulo varchar2(20), 
     autor varchar2(30),  
     editorial varchar2(15)
); 
```
salida sql 
```sh
Error report -
ORA-00955: name is already used by an existing object
00955. 00000 -  "name is already used by an existing object"
*Cause:    
*Action:
```
## 05 Visualice las tablas existentes
codigo sql 
```sql 
select * from libros;
```
salida sql
```sh
TITULO AUTOR EDITORIAL
```
## 06 Visualice la estructura de la tabla "libros": Aparece "libros" en la lista.

codigo sql 
```sql 
describe libros;
```
salida sql
```sh
Name      Null? Type         
--------- ----- ------------ 
TITULO          VARCHAR2(20) 
AUTOR           VARCHAR2(30) 
EDITORIAL       VARCHAR2(15) 
```
## 07 Elimine la tabla
codigo sql 
```sql
drop table libros;
```
salida sql
```sh
Table LIBROS dropped.
```
## 08 Intente eliminar la tabla Un mensaje indica que no existe.
codigo sql 
```sql 
drop table libros;
```
salida sql
```sh 
Error starting at line : 1 in command -
drop table libros
Error report -
ORA-00942: table or view does not exist
00942. 00000 -  "table or view does not exist"
*Cause:    
*Action:
```