# ejercicio 01
## 01 Elimine "agenda"
codigo sql
```sql
drop table agenda;
```
salida sql
```sh
Error starting at line : 1 in command -
drop table agenda
Error report -
ORA-00942: table or view does not exist
00942. 00000 -  "table or view does not exist"
*Cause:    
*Action:
```
## 02 Cree la tabla, con los siguientes campos: apellido (cadena de 30), nombre (cadena de 20), domicilio (cadena de 30) y telefono (cadena de 11):
codigo sql
```sql
create table agenda(
    apellido varchar2(30),
    nombre varchar2(30),
    domicilio varchar2(30),
    telefono varchar2(11)
);

```
salida sql
```sh
Table AGENDA created.
```
## 03 Visualice la estructura de la tabla "agenda" (4 campos)
codigo sql
```sql
describe agenda;
```
salida sql
```sh
Name      Null? Type         
--------- ----- ------------ 
APELLIDO        VARCHAR2(30) 
NOMBRE          VARCHAR2(30) 
DOMICILIO       VARCHAR2(30) 
TELEFONO        VARCHAR2(11) 
```
## 04 Ingrese los siguientes registros ("insert into"):
codigo sql
```sql
insert into agenda(apellido,nombre,domicilio,telefono) values ('Acosta', 'Ana', 'Colon 123', '4234567');
insert into agenda(apellido,nombre,domicilio,telefono) values ('Bustamante', 'Betina', 'Avellaneda 135', '4458787');
insert into agenda(apellido,nombre,domicilio,telefono) values ('Lopez', 'Hector', 'Salta 545', '4887788'); 
insert into agenda(apellido,nombre,domicilio,telefono) values ('Lopez', 'Luis', 'Urquiza 333', '4545454');
insert into agenda(apellido,nombre,domicilio,telefono) values ('Lopez', 'Marisa', 'Urquiza 333', '4545454');

```
salida sql
```sh
1 row inserted.


1 row inserted.


1 row inserted.


1 row inserted.


1 row inserted.

```
## 05 Seleccione todos los registros de la tabla (5 registros)

codigo sql
```sql
select * from agenda;
```
salida sql
```sh

APELLIDO                       NOMBRE                         DOMICILIO                      TELEFONO   
------------------------------ ------------------------------ ------------------------------ -----------
Acosta                         Ana                            Colon 123                      4234567    
Bustamante                     Betina                         Avellaneda 135                 4458787    
Lopez                          Hector                         Salta 545                      4887788    
Lopez                          Luis                           Urquiza 333                    4545454    
Lopez                          Marisa                         Urquiza 333                    4545454    

```
## 06 Seleccione el registro cuyo nombre sea "Marisa" (1 registro)
codig6o sql
```sql
select *from agenda where nombre='Marisa';
```
salida sql
```sh
APELLIDO                       NOMBRE                         DOMICILIO                      TELEFONO   
------------------------------ ------------------------------ ------------------------------ -----------
Lopez                          Marisa                         Urquiza 333                    4545454    
```
## 07 Seleccione los nombres y domicilios de quienes tengan apellido igual a "Lopez" (3 registros)
codigo sql
```sql
select nombre, domicilio from agenda where apellido='Lopez';
```
salida sql
```sh
NOMBRE                         DOMICILIO                     
------------------------------ ------------------------------
Hector                         Salta 545                     
Luis                           Urquiza 333                   
Marisa                         Urquiza 333  
```
## 08 Seleccione los nombres y domicilios de quienes tengan apellido igual a "lopez" (en minúsculas)
codigo sql
```sql
select nombre, domicilio from agenda where apellido='lopez';

```
salida sql
```sh
no rows selected
```
## 09 Muestre el nombre de quienes tengan el teléfono "4545454" (2 registros)
codigo sql
```sql
select * from agenda where telefono = 4545454;
```
salida sql
```sh
APELLIDO                       NOMBRE                         DOMICILIO                      TELEFONO   
------------------------------ ------------------------------ ------------------------------ -----------
Lopez                          Luis                           Urquiza 333                    4545454    
Lopez                          Marisa                         Urquiza 333                    4545454 
```
# ejercicio 02

## Un comercio que vende artículos de computación registra los datos de sus artículos en una tabla llamada "articulos".

## 01 Elimine la tabla si existe.
codigo sql
```sql
drop table articulos;
```
salida sql
```sh
Table ARTICULOS dropped.
```
## 02 Cree la tabla "articulos" con la siguiente estructura:
codigo sql
```sql
create table articulos(
    codigo number(5),
    nombre varchar2(20),
    descripcion varchar2(30),
    precio number(7,2)
);

```
salida sql
```sh
Table ARTICULOS created.
```
## 03 Vea la estructura de la tabla:
codigo sql
```sql
describe articulos;
```
salida sql
```sh
Name        Null? Type         
----------- ----- ------------ 
CODIGO            NUMBER(5)    
NOMBRE            VARCHAR2(20) 
DESCRIPCION       VARCHAR2(30) 
PRECIO            NUMBER(7,2)  
```
## 04 Ingrese algunos registros:
codigo sql
```sql
insert into articulos (codigo, nombre, descripcion, precio) values (1,'impresora','Epson Stylus C45',400.80);
insert into articulos (codigo, nombre, descripcion, precio) values (2,'impresora','Epson Stylus C85',500);
insert into articulos (codigo, nombre, descripcion, precio) values (3,'monitor','Samsung 14',800);
insert into articulos (codigo, nombre, descripcion, precio) values (4,'teclado','ingles Biswal',100);
insert into articulos (codigo, nombre, descripcion, precio) values (5,'teclado','español Biswal',90);

```
salida sql
```sh
1 row inserted.


1 row inserted.


1 row inserted.


1 row inserted.


1 row inserted.
```
## 05 Seleccione todos los datos de los registros cuyo nombre sea "impresora" (2 registros)
codigo sql
```sql
select * from articulos where nombre='impresora';
```
salida sql
```sh
    CODIGO NOMBRE               DESCRIPCION                        PRECIO
---------- -------------------- ------------------------------ ----------
         1 impresora            Epson Stylus C45                    400.8
         2 impresora            Epson Stylus C85                      500
```
## 06 Muestre sólo el código, descripción y precio de los teclados (2 registros)
codigo sql
```sql
SELECT codigo,descripcion,precio from articulos where nombre='teclado';
```
salida sql
```sh
    CODIGO DESCRIPCION                        PRECIO
---------- ------------------------------ ----------
         4 ingles Biswal                         100
         5 español Biswal                         90
``` 