## Ejercicios propuestos
# Una librería almacena los datos de sus libros en una tabla denominada "libros" y en una tabla "ofertas", algunos datos de los libros cuyo precio no supera los $30. Además, controla las inserciones que los empleados realizan sobre "ofertas", almacenando en la tabla "control" el nombre del usuario, la fecha y hora, cada vez que se ingresa un nuevo registro en la tabla "ofertas".

# 01 Elimine las tres tablas:
```sql
drop table libros;
drop table ofertas;
drop table control;

```
# 02 Cree las tablas con las siguientes estructuras:
```sql
create table libros(
    codigo number(6),
    titulo varchar2(40),
    autor varchar2(30),
    editorial varchar2(20),
    precio number(6,2)
);

create table ofertas(
    titulo varchar2(40),
    autor varchar2(30),
    precio number(6,2)
);

create table control(
    usuario varchar2(30),
    fecha date
);

```
# 03 Establezca el formato de fecha para que muestre "DD/MM/YYYY HH24:MI":
```sql
ALTER SESSION SET NLS_DATE_FORMAT = 'DD/MM/YYYY HH24:MI';
```
# 04 Cree un disparador que se dispare cuando se ingrese un nuevo registro en "ofertas"; el trigger debe ingresar en la tabla "control", el nombre del usuario, la fecha y la hora en la cual se realizó un "insert" sobre "ofertas"
```sql
CREATE OR REPLACE TRIGGER tr_insert_ofertas
AFTER INSERT ON ofertas
FOR EACH ROW
BEGIN
    INSERT INTO control (usuario, fecha)
    VALUES ('nombre_usuario', SYSDATE);
END;
/

```
# 05 Vea qué informa el diccionario "user_triggers" respecto del trigger anteriormente creado
```sql
SELECT * FROM user_triggers WHERE trigger_name = 'TR_INSERT_OFERTAS';
```
# 06 Ingrese algunos registros en "libros"
```sql
insert into libros values(100,'Uno','Richard Bach','Planeta',25);
insert into libros values(102,'Matematica estas ahi','Paenza','Nuevo siglo',12);
insert into libros values(105,'El aleph','Borges','Emece',32);
insert into libros values(120,'Aprenda PHP','Molina Mario','Nuevo siglo',55);

```
# 07 Ingrese en "ofertas" los libros de "libros" cuyo precio no superen los $30, utilizando la siguiente sentencia:
```sql
insert into ofertas select titulo,autor,precio from libros where precio<30;
```
# 08 Verifique que el trigger se disparó consultando la tabla "control".
```sql
SELECT * FROM control;
```
