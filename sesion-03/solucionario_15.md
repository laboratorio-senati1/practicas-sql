## Ejeercicio 01 
# Un banco tiene registrados las cuentas corrientes de sus clientes en una tabla llamada "cuentas". La tabla contiene estos datos:
 Número de Cuenta        Documento       Nombre          Saldo
 ______________________________________________________________
 1234                         25666777        Pedro Perez     500000.60
 2234                         27888999        Juan Lopez      -250000
 3344                         27888999        Juan Lopez      4000.50
 3346                         32111222        Susana Molina   1000


## 01 Elimine la tabla "cuentas":
codigo sql
```sql
drop table cuentas;
```
salida sql
```sh
Error starting at line : 1 in command -
drop table cuentas
Error report -
ORA-00942: table or view does not exist
00942. 00000 -  "table or view does not exist"
*Cause:    
*Action:

```
## 02 Cree la tabla eligiendo el tipo de dato adecuado para almacenar los datos descriptos arriba:

# Número de cuenta: entero hasta 9999, no nulo, no puede haber valores repetidos, clave primaria;


# Documento del propietario de la cuenta: cadena de caracteres de 8 de longitud (siempre 8), no nulo;


# Nombre del propietario de la cuenta: cadena de caracteres de 30 de longitud,


# Saldo de la cuenta: valores que no superan 999999.99
codigo sql
```sql
create table cuentas(
    numero number(4) not null,
    documento char(8),
    nombre varchar2(30),
    saldo number(8,2),
    primary key (numero)
);

```
salida sql
```sh
Table CUENTAS created.
```
## 03 Ingrese los siguientes registros:
codigo sql
```sql
insert into cuentas(numero,documento,nombre,saldo) values('1234','25666777','Pedro Perez',500000.60);
insert into cuentas(numero,documento,nombre,saldo) values('2234','27888999','Juan Lopez',-250000);
insert into cuentas(numero,documento,nombre,saldo) values('3344','27888999','Juan Lopez',4000.50);
insert into cuentas(numero,documento,nombre,saldo) values('3346','32111222','Susana Molina',1000);
```
salida sql
```sh
1 row inserted.


1 row inserted.


1 row inserted.


1 row inserted.

```
## 04 Seleccione todos los registros cuyo saldo sea mayor a "4000" (2 registros)
codigo sql
```sql
SELECT * FROM cuentas WHERE saldo > 4000;
```
salida sql
```sh
    NUMERO DOCUMENT NOMBRE                              SALDO
---------- -------- ------------------------------ ----------
      1234 25666777 Pedro Perez                      500000.6
      3344 27888999 Juan Lopez                         4000.5
```
## 05 Muestre el número de cuenta y saldo de todas las cuentas cuyo propietario sea "Juan Lopez" (2 registros)
codigo sql
```sql
SELECT numero, saldo FROM cuentas WHERE nombre = 'Juan Lopez';
```
salida sql
```sh
    NUMERO      SALDO
---------- ----------
      2234    -250000
      3344     4000.5
```
## 06 Muestre las cuentas con saldo negativo (1 registro)
codigo sql
```sql
SELECT * FROM cuentas WHERE saldo < 0;
```
salida sql
```sh
    NUMERO DOCUMENT NOMBRE                              SALDO
---------- -------- ------------------------------ ----------
      2234 27888999 Juan Lopez                        -250000
```
## 07 Muestre todas las cuentas cuyo número es igual o mayor a "3000" (2 registros)
codigo sql
```sql
SELECT * FROM cuentas WHERE saldo >= 3000;
```
salida sql
```sh
    NUMERO DOCUMENT NOMBRE                              SALDO
---------- -------- ------------------------------ ----------
      1234 25666777 Pedro Perez                      500000.6
      3344 27888999 Juan Lopez                         4000.5
```
# Ejercicio 02 
## Una empresa almacena los datos de sus empleados en una tabla "empleados" que guarda los siguientes datos: nombre, documento, sexo, domicilio, sueldobasico.
## 01 Elimine la tabla:
codigo sql
```sql
drop table empleados;
```
salida sql
```sh
Table EMPLEADOS dropped.
```
## 02 Cree la tabla eligiendo el tipo de dato adecuado para cada campo:
codigo sql
```sql
create table empleados(
    nombre varchar2(30),
    documento char(8),
    sexo char(1),
    domicilio varchar2(30),
    sueldobasico number(7,2),--máximo estimado 99999.99
    cantidadhijos number(2)--no superará los 99
);
```
salida sql
```sh
Table EMPLEADOS created.
```
## 03 Ingrese algunos registros:
codigo sql
```sql
insert into empleados (nombre,documento,sexo,domicilio,sueldobasico,cantidadhijos) values ('Juan Perez','22333444','m','Sarmiento 123',500,2);
insert into empleados (nombre,documento,sexo,domicilio,sueldobasico,cantidadhijos) values ('Ana Acosta','24555666','f','Colon 134',850,0);
insert into empleados (nombre,documento,sexo,domicilio,sueldobasico,cantidadhijos) values ('Bartolome Barrios','27888999','m','Urquiza 479',10000.80,4);
```
salida sql
```sh
1 row inserted.


1 row inserted.


1 row inserted.
```
## 04 Ingrese un valor de "sueldobasico" con más decimales que los definidos (redondea los decimales al valor más cercano 800.89)
codigo sql
```sql
INSERT INTO empleados (sueldobasico) VALUES (ROUND(800.89, 2));
```
salida sql
```sh
1 row inserted.
```
## 05 Intente ingresar un sueldo que supere los 7 dígitos (no lo permite)
codigo sql
```sql
INSERT INTO empleados (sueldo) VALUES (10000000);
```
salida sql
```sh
Error starting at line : 1 in command -
INSERT INTO empleados (sueldo) VALUES (10000000)
Error at Command Line : 1 Column : 24
Error report -
SQL Error: ORA-00904: "SUELDO": invalid identifier
00904. 00000 -  "%s: invalid identifier"
*Cause:    
*Action:

```
## 06 Muestre todos los empleados cuyo sueldo no supere los 900 pesos
codigo sql
```sql
select * from empleados where sueldobasico <900;
```
salida sql
```sh
NOMBRE                         DOCUMENT S DOMICILIO                      SUELDOBASICO CANTIDADHIJOS
------------------------------ -------- - ------------------------------ ------------ -------------
Juan Perez                     22333444 m Sarmiento 123                           500             2
Ana Acosta                     24555666 f Colon 134                               850             0
                                                                               800.89              
                                                                               800.89              
```
## 07 Seleccione los nombres de los empleados que tengan hijos (3 registros)
codigo sql
```sql
select * from empleados where cantidadhijos >0;
```
salida sql
```sh
NOMBRE                         DOCUMENT S DOMICILIO                      SUELDOBASICO CANTIDADHIJOS
------------------------------ -------- - ------------------------------ ------------ -------------
Bartolome Barrios              27888999 m Urquiza 479                         10000.8             4
Bartolome Barrios              27888999 m Urquiza 479                         10000.8             4
Juan Perez                     22333444 m Sarmiento 123                           500             2
Bartolome Barrios              27888999 m Urquiza 479                         10000.8             4
```