## Ejercicios propuestos
# Una empresa tiene registrados sus clientes en una tabla llamada "clientes", también tiene una tabla "provincias" donde registra los nombres de las provincias.
## 01 Elimine las tablas "clientes" y "provincias":
codigo sql
```sql
drop table clientes;
drop table provincias;
```
## 02 Créelas con las siguientes estructuras:
codigo sql
```sql
create table clientes (
    codigo number(5),
    nombre varchar2(30),
    domicilio varchar2(30),
    ciudad varchar2(20),
    codigoprovincia number(2),
    primary key(codigo)
);

create table provincias(
    codigo number(2),
    nombre varchar2(20),
    primary key (codigo)    );

```
## 03 Ingrese algunos registros para ambas tablas:
codigo sql
```sql
insert into provincias values(1,'Cordoba');
insert into provincias values(2,'Santa Fe');
insert into provincias values(3,'Misiones');
insert into provincias values(4,'Rio Negro');
insert into clientes values(100,'Perez Juan','San Martin 123','Carlos Paz',1);
insert into clientes values(101,'Moreno Marcos','Colon 234','Rosario',2);
insert into clientes values(102,'Acosta Ana','Avellaneda 333','Posadas',3);

```
## 04 Establezca una restricción "foreign key" especificando la acción "set null" para eliminaciones.
codigo sql
```sql
ALTER TABLE clientes
ADD CONSTRAINT fk_codigoprovincia
FOREIGN KEY (codigoprovincia)
REFERENCES provincias(codigo)
ON DELETE SET NULL;
```
## 05 Elimine el registro con código 3, de "provincias" y consulte "clientes" para ver qué cambios ha realizado Oracle en los registros coincidentes Todos los registros con "codigoprovincia" 3 han sido seteados a null.
codigo sql
```sql
DELETE FROM provincias WHERE codigo = 3;

SELECT * FROM clientes;
```
## 06 Consulte el diccionario "user_constraints" para ver qué acción se ha establecido para las eliminaciones
codigo sql
```sql
SELECT constraint_name, delete_rule
FROM user_constraints
WHERE constraint_name = 'FK_CODIGOPROVINCIA';
```
## 07 Intente modificar el registro con código 2, de "provincias"
codigo sql
```sql
UPDATE provincias SET codigo = 5 WHERE codigo = 2;
```
## 08 Elimine la restricción "foreign key" establecida sobre "clientes"
codigo sql
```sql
ALTER TABLE clientes
DROP CONSTRAINT fk_codigoprovincia;
```
## 09 Establezca una restricción "foreign key" sobre "codigoprovincia" de "clientes" especificando la acción "cascade" para eliminaciones
codigo sql
```sql
ALTER TABLE clientes
ADD CONSTRAINT fk_codigoprovincia
FOREIGN KEY (codigoprovincia)
REFERENCES provincias(codigo)
ON DELETE CASCADE;
```
## 10 Consulte el diccionario "user_constraints" para ver qué acción se ha establecido para las eliminaciones sobre las restricciones "foreign key" de la tabla "clientes"
codigo sql
```sql
SELECT constraint_name, delete_rule
FROM user_constraints
WHERE table_name = 'CLIENTES' AND constraint_name = 'FK_CODIGOPROVINCIA';
```
## 11 Elimine el registro con código 2, de "provincias"
codigo sql
```sql
DELETE FROM provincias WHERE codigo = 2;
```
## 12 Verifique que el cambio se realizó en cascada, es decir, que se eliminó en la tabla "provincias" y todos los clientes de la provincia eliminada
codigo sql
```sql
SELECT * FROM provincias;
SELECT * FROM clientes;

```
## 13 Elimine la restricción "foreign key"
codigo sql
```sql
ALTER TABLE clientes
DROP CONSTRAINT fk_codigoprovincia;
```
## 14 Establezca una restricción "foreign key" sin especificar acción para eliminaciones
codigo sql
```sql
ALTER TABLE clientes
ADD CONSTRAINT fk_codigoprovincia
FOREIGN KEY (codigoprovincia)
REFERENCES provincias(codigo);
```
## 15 Intente eliminar un registro de la tabla "provincias" cuyo código exista en "clientes"
codigo sql
```sql
DELETE FROM provincias WHERE codigo = 1;
```
## 16 Consulte el diccionario "user_constraints" para ver qué acción se ha establecido para las eliminaciones sobre la restricción "FK_CLIENTES_CODIGOPROVINCIA"
codigo sql
```sql
SELECT constraint_name, delete_rule
FROM user_constraints
WHERE constraint_name = 'FK_CODIGOPROVINCIA';
```
## 17 Intente elimimar la tabla "provincias"
codigo sql
```sql
DROP TABLE provincias;
```
## 18 Elimine la restricción "foreign key"
codigo sql
```sql
ALTER TABLE clientes
DROP CONSTRAINT fk_codigoprovincia;
```
## 19 Elimine la tabla "provincias"
codigo sql
```sql
DROP TABLE provincias;
```