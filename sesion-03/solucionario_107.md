## Ejercicios propuestos
# Una empresa almacena los datos de sus empleados en una tabla denominada "empleados" y en otra llamada "control" guarda un registro por cada vez que un empleado actualiza datos sobre la tabla "empleados".
# 01 Elimine las tablas: 
```sql
drop table empleados;
drop table control;

```
# 02 Cree las tablas con las siguientes estructuras: 
```sql
create table empleados(
    documento char(8),
    apellido varchar2(20),
    nombre varchar2(20),
    seccion varchar2(30),
    sueldo number(8,2)
);

create table control(
    usuario varchar2(30),
    fecha date
);
```
# 03 Ingrese algunos registros en "empleados":
```sql
insert into empleados values('22333444','ACOSTA','Ana','Secretaria',500);
insert into empleados values('22777888','DOMINGUEZ','Daniel','Secretaria',560);
insert into empleados values('22999000','FUENTES','Federico','Sistemas',680);
insert into empleados values('22555666','CASEROS','Carlos','Contaduria',900);
insert into empleados values('23444555','GOMEZ','Gabriela','Sistemas',1200);
insert into empleados values('23666777','JUAREZ','Juan','Contaduria',1000);

```
# 04 Cree un disparador a nivel de sentencia, que se dispare cada vez que se actualicen registros en "empleados"; el trigger debe ingresar en la tabla "control", el nombre del usuario y la fecha en la cual se realizó un "update" sobre "empleados"
```sql
create or replace trigger actualizacion_empleado
after update on empleados
begin
  insert into control(usuario, fecha) values(user, sysdate);
end;
/
```
# 05 Vea qué informa el diccionario "user_triggers" respecto del trigger anteriormente creado 
```sql
select * from user_triggers where trigger_name = 'ACTUALIZACION_EMPLEADO';
```
# 06 Actualice un registro en "empleados":
```sql
update empleados set nombre='Graciela' where documento='23444555';
```
# 07 Vea si el trigger se disparó consultando la tabla "control" 
```sql
select * from control;
```
# 08 Actualice varios registros de "empleados" en una sola sentencia
```sql
update empleados set sueldo = sueldo + 100 where seccion = 'Secretaria';
```
# 09 Vea si el trigger se disparó consultando la tabla "control" Note que se modificaron 2 registros de "empleados", pero como la modificación se realizó con una sola sentencia "update" y el trigger es a nivel de sentencia, se agregó solamente una fila a la tabla "control"; si el trigger hubiese sido creado a nivel de fila, la sentencia anterior, hubiese disparado el trigger 2 veces y habría ingresado en "control" 2 filas.
```sql
select * from control;
```