# EJERCICIO 01 
## Un videoclub que alquila películas en video almacena la información de sus películas en una tabla llamada "peliculas"; para cada película necesita los siguientes datos:

## 01 Elimine la tabla "peliculas" si ya existe.
codigo sql
```sql
drop table peliculas;
```
salida sql
```sh
Error starting at line : 1 in command -
drop table peliculas
Error report -
ORA-00942: table or view does not exist
00942. 00000 -  "table or view does not exist"
*Cause:    
*Action:
```
## 02 Cree la tabla eligiendo el tipo de dato adecuado para cada campo.
codigo sql
```sql
create table peliculas (
     nombre varchar2(20), 
     actor varchar2(20),  
     duracion int,
     cantidad int
);
```
salida sql
```sh
Table EMPLEADOS created.
```
## 03 Vea la estructura de la tabla.
codigo sql
```sql
DESCRIBE peliculas;
```
salida sql
```sh
Name     Null? Type         
-------- ----- ------------ 
NOMBRE         VARCHAR2(20) 
ACTOR          VARCHAR2(20) 
DURACION       NUMBER(38)   
CANTIDAD       NUMBER(38) 
```
## 04 Ingrese los siguientes registros:
codigo sql
```sql
insert into peliculas (nombre, actor, duracion, cantidad) values ('Mision imposible','Tom Cruise',128,3);
insert into peliculas (nombre, actor, duracion, cantidad) values ('Mision imposible 2','Tom Cruise',130,2);
insert into peliculas (nombre, actor, duracion, cantidad) values ('Mujer bonita','Julia Roberts',118,3);
insert into peliculas (nombre, actor, duracion, cantidad) values ('Elsa y Fred','China Zorrilla',110,2);
```
salida sql
```sh
1 row inserted.


1 row inserted.


1 row inserted.


1 row inserted.

```
## 05 Muestre todos los registros (4 registros)
codigo sql
```sql
select * from peliculas;
```
salida sql
```sh
NOMBRE                 ACTOR           DURACION  CANTIDAD
Mision imposible       Tom Cruise      128       3
Mision imposible 2     Tom Cruise      130       2
Mujer bonita           Julia Roberts   118       3
Elsa y Fred            China Zorrilla  110       2
```
## 06 Intente ingresar una película con valor de cantidad fuera del rango permitido:
codigo sql
```sql
insert into peliculas (nombre, actor, duracion, cantidad) values ('Elsa y Fred','China Zorrilla',120,20);
```
salida sql
```sh
value too large for type integer
```
## 07 Ingrese un valor con decimales en un nuevo registro, en el campo "duracion":
codigo sql
```sql
ALTER TABLE peliculas ADD duracion DECIMAL(10,2);
```
salida sql
```sh
Error starting at line : 1 in command -
ALTER TABLE peliculas 
ADD duracion DECIMAL(10,2)
Error report -
ORA-01430: column being added already exists in table
01430. 00000 -  "column being added already exists in table"
*Cause:    
*Action:
```
## 08 Muestre todos los registros para ver cómo se almacenó el último registro ingresado.
codigo sql
```sql
select * from peliculas;
```
salida sql
```sh
NOMBRE                 ACTOR           DURACION  CANTIDAD
Mision imposible       Tom Cruise      128       3
Mision imposible 2     Tom Cruise      130       2
Mujer bonita           Julia Roberts   118       3
Elsa y Fred            China Zorrilla  110       2
SAElsa y Fred          China Zorrilla  120       20
```
## 09 Intente ingresar un nombre de película que supere los 20 caracteres.
codigo sql
```sql
insert into peliculas (nombre, actor, duracion, cantidad) values ('el zapato loco','China Zorrilla',120,2);
```
salida sql
```sh
1 row inserted.
```

# ejercicio 02
## Una empresa almacena los datos de sus empleados en una tabla "empleados" que guarda los siguientes datos: nombre, documento, sexo, domicilio, sueldobasico.
## 01 Elimine la tabla si existe.
codigo sql
```sql
drop table empleados;
```
salida sql
```sh
Table EMPLEADOS dropped.
```
## 02 Cree la tabla eligiendo el tipo de dato adecuado para cada campo:
codigo sql
```sql
create table empleados(
    nombre varchar2(20),
    documento varchar2(8),
    sexo varchar2(1),
    domicilio varchar2(30),
    sueldobasico number(6,2)
);

```
salida sql
```sh
Table EMPLEADOS created.
```
## 03 Verifique que la tabla existe consultando
codigo sql
```sql
select * from all_tables;
```
salida sql
```sh
la tabla si existe 
```
## 04 Vea la estructura de la tabla (5 campos)
codigo sql
```sql
DESCRIBE empleados;
```
salida sql
```sh
Name         Null? Type         
------------ ----- ------------ 
NOMBRE             VARCHAR2(20) 
DOCUMENTO          VARCHAR2(8)  
SEXO               VARCHAR2(1)  
DOMICILIO          VARCHAR2(30) 
SUELDOBASICO       NUMBER(6,2) 
```
## 05 Ingrese algunos registros:
codigo sql
```sql
insert into empleados (nombre, documento, sexo, domicilio, sueldobasico) values ('Juan Perez','22333444','m','Sarmiento 123',500);
insert into empleados (nombre, documento, sexo, domicilio, sueldobasico) values ('Ana Acosta','24555666','f','Colon 134',650);
insert into empleados (nombre, documento, sexo, domicilio, sueldobasico) values ('Bartolome Barrios','27888999','m','Urquiza 479',800);

```
salida sql
```sh
1 row inserted.


1 row inserted.


1 row inserted.
```
## 06 Seleccione todos los registros (3 registros)
codigo sql
```sql

```
salida sql
```sh

```


## 07 Intente ingresar un registro con el valor "masculino" en el campo "sexo".

codigo sql
```sql
insert into empleados (nombre, documento, sexo, domicilio, sueldobasico) values ('Barte Barrios','2788999','masculino','Urquisa 479',900);
```
salida sql
```sh
Error starting at line : 1 in command -
insert into empleados (nombre, documento, sexo, domicilio, sueldobasico) values ('Barte Barrios','2788999','masculino','Urquisa 479',900)
Error at Command Line : 1 Column : 108
Error report -
SQL Error: ORA-12899: value too large for column "BRAYAN"."EMPLEADOS"."SEXO" (actual: 9, maximum: 1)
12899. 00000 -  "value too large for column %s (actual: %s, maximum: %s)"
*Cause:    An attempt was made to insert or update a column with a value
           which is too wide for the width of the destination column.
           The name of the column is given, along with the actual width
           of the value, and the maximum allowed width of the column.
           Note that widths are reported in characters if character length
           semantics are in effect for the column, otherwise widths are
           reported in bytes.
*Action:   Examine the SQL statement for correctness.  Check source
           and destination column data types.
           Either make the destination column wider, or use a subset
           of the source column (i.e. use substring).

```
## 08 Intente ingresar un valor fuera de rango, en un nuevo registro, para el campo "sueldobasico" Mensaje de error.
 codigo sql
 ```sql
insert into empleados (nombre, documento, sexo, domicilio, sueldobasico) values ('Barte Barrios','2788999','masculino','Urquisa 475646549',96546500);
 ```
 salida sql
 ```sh
Error report -
SQL Error: ORA-12899: value too large for column "BRAYAN"."EMPLEADOS"."SEXO" (actual: 9, maximum: 1)
12899. 00000 -  "value too large for column %s (actual: %s, maximum: %s)"
*Cause:    An attempt was made to insert or update a column with a value
           which is too wide for the width of the destination column.
           The name of the column is given, along with the actual width
           of the value, and the maximum allowed width of the column.
           Note that widths are reported in characters if character length
           semantics are in effect for the column, otherwise widths are
           reported in bytes.
*Action:   Examine the SQL statement for correctness.  Check source
           and destination column data types.
           Either make the destination column wider, or use a subset
           of the source column (i.e. use substring).

 ```
# 09 elimine la tabla 
codigo sql 
```sql
drop table empleados;
```
salida sql
```sql
Table EMPLEADOS dropped.
```


