## Ejercicios propuestos
# Una empresa almacena los datos de sus empleados en una tabla denominada "empleados" y en una tabla "control", el nombre del usuario y la fecha, cada vez que se ingresa un nuevo registro en la tabla "empleados".
# 01 Elimine las tablas:
```sql
drop table empleados;
drop table control;

```
# 02 Cree las tablas con las siguientes estructuras:
```sql
create table empleados(
    documento char(8),
    apellido varchar2(30),
    nombre varchar2(30),
    seccion varchar2(20)
);

create table control(
    usuario varchar2(30),
    fecha date
);

```
# 03 Cree un disparador que se dispare una vez por cada registro ingresado en "empleados"; el trigger debe ingresar en la tabla "control", el nombre del usuario y la fecha en la cual se realizó un "insert" sobre "empleados"
```sql
CREATE OR REPLACE TRIGGER tr_insert_empleados
AFTER INSERT ON empleados
FOR EACH ROW
BEGIN
    INSERT INTO control (usuario, fecha)
    VALUES ('nombre_usuario', SYSDATE);
END;
/

```
# 04 Vea qué nos informa el diccionario "user_triggers" respecto del trigger anteriormente creado
```sql
SELECT * FROM user_triggers WHERE trigger_name = 'TR_INSERT_EMPLEADOS';
```
# 05 Ingrese algunos registros en "empleados":
```sql
insert into empleados values('22333444','ACOSTA','Ana','Secretaria');
insert into empleados values('22777888','DOMINGUEZ','Daniel','Secretaria');
insert into empleados values('22999000','FUENTES','Federico','Sistemas');
insert into empleados values('22555666','CASEROS','Carlos','Contaduria');
insert into empleados values('23444555','GOMEZ','Gabriela','Sistemas');
insert into empleados values('23666777','JUAREZ','Juan','Contaduria');

```
# 06 Verifique que el trigger se disparó 6 veces, una por cada fila afectada en la sentencia "insert" anteriormente ejecutada; consultamos la tabla "control":
```sql
select *from control;
```
# Si el trigger hubiese sido creado a nivel de sentencia, no de fila, el "insert" anterior se hubiese activado una sola vez aún cuando se ingresaron 6 registros.