# Ejericicio 01 
## En una página web se guardan los siguientes datos de las visitas: nombre, mail, país y fecha de la visita.
## 01 Elimine la tabla "visitas" y créela con la siguiente estructura:
codigo sql
```sql
drop table visitas;

create table visitas (
    nombre varchar2(30) default 'Anonimo',
    mail varchar2(50),
    pais varchar2(20),
    fecha date
);
```
## 02 Ingrese algunos registros:
codigo sql
```sql
insert into visitas
values ('Ana Maria Lopez','<AnaMaria@hotmail.com>','Argentina','10/10/2016');

insert into visitas
values ('Gustavo Gonzalez','<GustavoGGonzalez@gotmail.com>','Chile','10/10/2016');

insert into visitas
values ('Juancito','<JuanJosePerez@hotmail.com>','Argentina','11/10/2016');

insert into visitas
values ('Fabiola Martinez','<MartinezFabiola@hotmail.com>','Mexico','12/10/2016');

insert into visitas
values ('Fabiola Martinez','<MartinezFabiola@hotmail.com>','Mexico','12/09/2016');

insert into visitas
values ('Juancito','<JuanJosePerez@gmail.com>','Argentina','12/09/2016');

insert into visitas
values ('Juancito','<JuanJosePerez@hotmail.com>','Argentina','15/09/2016');

insert into visitas
values ('Federico1','<federicogarcia@xaxamail.com>','Argentina',null);

```
## 03 Seleccione los usuarios que visitaron la página entre el '12/09/2016' y '11/10/2016' (6 registros) Note que incluye los de fecha mayor o igual al valor mínimo y menores o iguales al valor máximo, y que los valores nulos no se incluyen.
codigo sql
```sql
SELECT * FROM visitas WHERE fecha >= TO_DATE('12/09/2016', 'DD/MM/YYYY') AND fecha <= TO_DATE('11/10/2016', 'DD/MM/YYYY') AND fecha IS NOT NULL;
```
# Ejercicio 02 
## Trabaje con la tabla llamada "medicamentos" de una farmacia.
## 01 Elimine la tabla y créela con la siguiente estructura:
codigo sql
```sql
drop table medicamentos;

create table medicamentos(
    codigo number(6) not null,
    nombre varchar2(20),
    laboratorio varchar2(20),
    precio number(6,2),
    cantidad number(4),
    fechavencimiento date not null,
    primary key(codigo)
);
```
## 02 Ingrese algunos registros:
codigo sql
```sql
insert into medicamentos
values(102,'Sertal','Roche',5.2,10,'01/02/2020');

insert into medicamentos
values(120,'Buscapina','Roche',4.10,200,'01/12/2017');

insert into medicamentos
values(230,'Amoxidal 500','Bayer',15.60,100,'28/12/2017');

insert into medicamentos
values(250,'Paracetamol 500','Bago',1.90,20,'01/02/2018');

insert into medicamentos
values(350,'Bayaspirina','Bayer',2.10,150,'01/12/2019');

insert into medicamentos
values(456,'Amoxidal jarabe','Bayer',5.10,250,'01/10/2020');

```
## 03 Recupere los nombres y precios de los medicamentos cuyo precio esté entre 5 y 15 (2 registros)
codigo sql
```sql
SELECT nombre, precio FROM medicamentos WHERE precio >= 5 AND precio <= 15;
```
## 04 Seleccione los registros cuya cantidad se encuentre entre 100 y 200 (3 registros)
codigo sql
```sql
SELECT * FROM tabla WHERE cantidad >= 100 AND cantidad <= 200;
```
## 05 Recupere los remedios cuyo vencimiento se encuentre entre la fecha actual y '01/01/2028' inclusive.
codigo sql
```sql
SELECT * FROM medicamentos WHERE fechavencimiento >= CURRENT_DATE AND fechavencimiento <= TO_DATE('01/01/2028', 'DD/MM/YYYY');
```
## 06 Elimine los remedios cuyo vencimiento se encuentre entre el año 2017 y 2018 inclusive (3 registros)
codigo sql
```sql
DELETE FROM medicamentos WHERE EXTRACT(YEAR FROM fechavencimiento) BETWEEN 2017 AND 2018;

```
