## Ejercicios propuestos
# Una escuela necesita crear 3 usuarios diferentes en su base de datos. Uno denominado "director", otro "profesor" y otro "estudiante". Luego se les concederán diferentes permisos para retringir el acceso a los diferentes objetos. Conéctese como administrador (por ejemplo "system").
# 01 Cree un usuario denominado "director", con contraseña "escuela", asignándole 100M de espacio en "system" (100M). Antes elimínelo por si existe:
```sql
DROP USER director CASCADE;
CREATE USER director IDENTIFIED BY escuela DEFAULT TABLESPACE system QUOTA 100M ON system;
```
# 02 Intente iniciar una sesión como "director". No es posible, no hemos concedido el permiso correspondiente. Aparece un mensaje indicando que el usuario "director" no tiene permiso "create session" por lo tanto no puede conectarse.
```sql
CONNECT director;
```
# 03 Vea los permisos de "director" No tiene ningún permiso.
```sql
SELECT * FROM session_privs;
```
# 04 Conceda a "director" permiso para iniciar sesion y para crear tablas
```sql
GRANT CREATE SESSION, CREATE TABLE TO director;
```
# 05 Vea los permisos de "director" Tiene permiso "create session" y para crear tablas.
```sql
SELECT * FROM session_privs;
```
# 06 Inicie una sesión como "director".
```sql
CONNECT director;
```
# 07 Como "administrador", elimine los usuarios "profesor" y "alumno", por si existen
```sql
DROP USER profesor CASCADE;
DROP USER estudiante CASCADE;
```
# 08 Cree un usuario denominado "profesor", con contraseña "maestro", asigne espacio en "system" (100M)
```sql
CREATE USER profesor IDENTIFIED BY maestro DEFAULT TABLESPACE system QUOTA 100M ON system;
```
# 09 Cree un usuario denominado "estudiante", con contraseña "alumno" y tablespace "system" (no asigne "quota")
```sql
CREATE USER estudiante IDENTIFIED BY alumno DEFAULT TABLESPACE system;
```
# 10 Consulte el diccionario de datos correspondiente para ver si existen los 3 usuarios creados
```sql
SELECT * FROM dba_users WHERE username IN ('director', 'profesor', 'estudiante');
```
# 11 Conceda a "profesor" y a "estudiante" permiso para conectarse
```sql
GRANT CREATE SESSION TO profesor, estudiante;
```
# 12 Conceda a "estudiante" permiso para crear tablas
```sql
GRANT CREATE TABLE TO estudiante;
```
# 13 Consulte el diccionario de datos "sys_privs" para ver los permisos de los 3 usuarios creados "director" y "estudiante" tienen permisos para conectarse y para crear tablas, "profesor" tiene permiso para conectarse.
```sql
SELECT * FROM dba_sys_privs WHERE grantee IN ('director', 'profesor', 'estudiante');
```
# 14 Retome su sesión como "director" y cree una tabla:
```sql
create table prueba(
  nombre varchar2(30),
  apellido varchar2(30)
 );
```
# 15 Podemos hacerlo poque "director" tiene el permiso necesario y espacio en "system".
```sql
create table prueba(
  nombre varchar2(30),
  apellido varchar2(30)
 );
```
# 16 Inicie una sesión como "profesor" e intente crear una tabla:
```sql
CONNECT profesor;
CREATE TABLE prueba (
  nombre VARCHAR2(30),
  apellido VARCHAR2(30)
);
```
# 17 Cambie a la conexión de administrador y conceda a "profesor" permiso para crear tablas
```sql
GRANT CREATE TABLE TO profesor;
```
# 18 Cambie a la sesión de "profesor" y cree una tabla Ahora si podemos hacerlo, "profesor" tiene permiso "create table".
```sql
CREATE TABLE prueba (
  nombre VARCHAR2(30),
  apellido VARCHAR2(30)
);
```
# 19 Consulte nuevamente los permisos de "profesor" Tiene permiso para crear tablas y para crear sesión.
```sql
SELECT * FROM session_privs;
```
# 20 Inicie una sesión como "estudiante" e intente crear una tabla:
```sql
create table prueba(
  nombre varchar2(30),
  apellido varchar2(30)
 );
```
# 21 Vuelva a la conexión de "administrador" y consulte todas las tablas denominadas "PRUEBA" Note que hay una tabla propiedad de "director" y otra que pertenece a "profesor".
```sql
SELECT * FROM all_tables WHERE table_name = 'PRUEBA';
```