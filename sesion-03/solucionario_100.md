## Ejercicios propuestos
# 01 Con la estructura repetitiva "for... loop" que vaya del 1 al 20, muestre los números pares.
```sql
begin
  for i in 1..20 loop
    if mod(i, 2) = 0 then
      dbms_output.put_line(i);
    end if;
  end loop;
end;
/

```
# 02 Dentro del ciclo debe haber una estructura condicional que controle que el número sea par y si lo es, lo imprima por pantalla.
```sql
begin
  for i in 1..20 loop
    if mod(i, 2) = 0 then
      dbms_output.put_line(i);
    end if;
  end loop;
end;
/

```
# 03 Con la estructura repetitiva "for... loop" muestre la sumatoria del número 5; la suma de todos los números del 1 al 5. Al finalizar el ciclo debe mostrarse por pantalla la sumatoria de 5 (15).
```sql
declare
  sumatoria number := 0;
begin
  for i in 1..5 loop
    sumatoria := sumatoria + 5;
  end loop;
  dbms_output.put_line('Sumatoria de 5: ' || sumatoria);
end;
/

```
# 04 Cree una función que reciba un valor entero y retorne el factorial de tal número; el factorial se obtiene multiplicando el valor que recibe por el anterior hasta llegar a multiplicarlo por uno.
```sql
create or replace function calcular_factorial(n in number) return number is
  factorial number := 1;
begin
  if n < 0 then
    return null;
  elsif n = 0 then
    return 1;
  else
    for i in 1..n loop
      factorial := factorial * i;
    end loop;
    return factorial;
  end if;
end;
/

```

# 05 Llame a la función creada anteriormente y obtenga el factorial de 5 y de 4 (120 y 24).
```sql
select calcular_factorial(5) as factorial_5 from dual;
select calcular_factorial(4) as factorial_4 from dual;

```
# 06 Cree un procedimiento que reciba dos parámetros numéricos; el procedimiento debe mostrar la tabla de multiplicar del número enviado como primer argumento, desde el 1 hasta el númeo enviado como segundo argumento. Emplee "for".
```sql
create or replace procedure mostrar_tabla_multiplicar(
  numero in number,
  limite in number
) is
begin
  for i in 1..limite loop
    dbms_output.put_line(numero || ' x ' || i || ' = ' || (numero * i));
  end loop;
end;
/

```

# 07 Ejecute el procedimiento creado anteriormente enviándole los valores necesarios para que muestre la tabla del 6 hasta el 20.
```sql
begin
  mostrar_tabla_multiplicar(6, 20);
end;
/

```

# 08 Ejecute el procedimiento creado anteriormente enviándole los valores necesarios para que muestre la tabla del 9 hasta el 10.
```sql
begin
  mostrar_tabla_multiplicar(9, 10);
end;
/
```