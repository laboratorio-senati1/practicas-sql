## Ejercicios propuestos
# Una librería almacena los datos de sus libros en una tabla denominada "libros" y en otra denominada "ofertas", almacena los códigos y precios de los libros cuyo precio es inferior a $50.
# 01 Elimine las tablas: 
```sql
drop table libros;
drop table ofertas;

```
# 02 Cree las tablas con las siguientes estructuras: 
```sql
create table libros(
    codigo number(6),
    titulo varchar2(40),
    autor varchar2(30),
    editorial varchar(20),
    precio number(6,2)
);

create table ofertas(
    codigo number(6),
    precio number(6,2)
);

```
# 03 Ingrese algunos registros en "libros": 
```sql
insert into libros values(100,'Uno','Richard Bach','Planeta',25);
insert into libros values(103,'El aleph','Borges','Emece',28);
insert into libros values(105,'Matematica estas ahi','Paenza','Nuevo siglo',12);
insert into libros values(120,'Aprenda PHP','Molina Mario','Nuevo siglo',55);
insert into libros values(145,'Alicia en el pais de las maravillas','Carroll','Planeta',35);

```
# 04 Cree un trigger a nivel de fila que se dispare al ingresar un registro en "libros"; si alguno de los libros ingresados tiene un precio menor o igual a $30 debe ingresarlo en "ofertas"
```sql
create or replace trigger ingreso_libro
after insert on libros
for each row
begin
  if :new.precio <= 30 then
    insert into ofertas(codigo, precio)
    values(:new.codigo, :new.precio);
  end if;
end;
/
```
# 05 Ingrese un libro en "libros" cuyo precio sea inferior a $30
```sql
insert into libros values(200, 'Libro oferta', 'Autor', 'Editorial', 25);
```
# 06 Verifique que el trigger se disparó consultando "ofertas"
```sql
select * from ofertas;
```
# 07 Ingrese un libro en "libros" cuyo precio supere los $30
```sql
insert into libros values(201, 'Libro caro', 'Autor', 'Editorial', 50);
```
# 08 Verifique que no se ingresó ningún registro en "ofertas"
```sql
select * from ofertas;
```
# 09 Cree un trigger a nivel de fila que se dispare al modificar el precio de un libro. Si tal libro existe en "ofertas" y su nuevo precio ahora es superior a $30, debe eliminarse de "ofertas"; si tal libro no existe en "ofertas" y su nuevo precio ahora es inferior a $30, debe agregarse a "ofertas"
```sql
create or replace trigger modificacion_precio
after update of precio on libros
for each row
begin
  if :new.precio > 30 then
    delete from ofertas where codigo = :old.codigo;
  elsif :new.precio <= 30 then
    insert into ofertas(codigo, precio)
    values(:new.codigo, :new.precio);
  end if;
end;
/
```
# 10 Aumente a más de $30 el precio de un libro que se encuentra en "ofertas"
```sql
update libros
set precio = 35
where codigo = 200;
```
# 11 Verifique que el trigger se disparó consultando "libros" y "ofertas"
```sql
select * from libros;
select * from ofertas;
```
# 12 Disminuya a menos de $31 el precio de un libro que no se encuentra en "ofertas"
```sql
update libros
set precio = 29
where codigo = 201;
```
# 13 Verifique que el trigger se disparó consultando "libros" y "ofertas"
```sql
select * from libros;
select * from ofertas;
```
# 14 Aumente el precio de un libro que no se encuentra en "ofertas"
```sql
update libros
set precio = 40
where codigo = 201;
```
# 15 Verifique que el trigger se disparó pero no se modificó "ofertas"
```sql
select * from libros;
select * from ofertas;
```
# 16 Cree un trigger a nivel de fila que se dispare al borrar un registro en "libros"; si alguno de los libros eliminados está en "ofertas", también debe eliminarse de dicha tabla.
```sql
create or replace trigger borrado_libro
after delete on libros
for each row
begin
  delete from ofertas where codigo = :old.codigo;
end;
/

```
# 17 Elimine un libro en "libros" que esté en "ofertas"
```sql
delete from libros where codigo = 200;
```
# 18 Verifique que el trigger se disparó consultando "libros" y "ofertas"
```sql
select * from libros;
select * from ofertas;
```
# 19 Elimine un libro en "libros" que No esté en "ofertas"
```sql
delete from libros where codigo = 201;
```
# 20 Verifique que el trigger se disparó consultando "libros" y "ofertas"
```sql
select * from libros;
select * from ofertas;
```
# 21 Cree una tabla llamada "control" que almacene el código, la fecha y el precio de un libro, antes elimínela por si existe
```sql
drop table control;
```
# 22 Cree un disparador que se dispare cada vez que se actualice el precio de un libro; el trigger debe ingresar en la tabla "control", el código del libro cuyo precio se actualizó, la fecha y el precio anterior.
```sql
create or replace trigger modificacion_precio_control
after update of precio on libros
for each row
declare
  v_anterior number(6,2);
begin
  select precio into v_anterior from libros where codigo = :old.codigo;
  insert into control(codigo, fecha, precio)
  values(:old.codigo, sysdate, v_anterior);
end;
/

```
# 23 Actualice el precio de un libro 
```sql
update libros
set precio = 50
where codigo = 200;
```
# 24 Controle que el precio se ha modificado en "libros" y que se agregó un registro en "control"
```sql
select * from libros;
select * from control;
```
# 25 Modifique nuevamente el precio del libro cambiado en el punto 11
```sql
update libros
set precio = 60
where codigo = 200;
```
# 26 Controle que el precio se ha modificado en "libros" y que se agregó un nuevo registro en "control"
```sql
select * from libros;
select * from control;
```
# 27 Modifique el precio de varios libros en una sola sentencia que incluya al modificado anteriormente
```sql
update libros
set precio = precio * 1.1;
```
# 28 Controle que el precio se ha modificado en "libros" y que se agregó un nuevo registro en "control"
```sql
select * from libros;
select * from control;

```
# 29 Vea qué informa el diccionario "user_triggers" respecto del trigger anteriormente creado
```sql
select * from user_triggers where trigger_name = 'MODIFICACION_PRECIO_CONTROL';
```