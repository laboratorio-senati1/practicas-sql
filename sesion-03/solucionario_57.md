## Ejercicios propuestos
# Una empresa tiene registrados sus clientes en una tabla llamada "clientes", también tiene una tabla "provincias" donde registra los nombres de las provincias
## 01 Elimine las tablas "clientes" y "provincias" y créelas:
codigo sql
```sql
drop table clientes;
drop table provincias;

create table clientes (
    codigo number(5),
    nombre varchar2(30),
    domicilio varchar2(30),
    ciudad varchar2(20),
    codigoprovincia number(2)
);

create table provincias(
    codigo number(2),
    nombre varchar2(20)
);

```
## 02 Intente agregar una restricción "foreign key" a la tabla "clientes" que haga referencia al campo "codigo" de "provincias" No se puede porque "provincias" no tiene restricción "primary key" ni "unique".
codigo sql
```sql

```
## 03 Establezca una restricción "unique" al campo "codigo" de "provincias"
codigo sql
```sql

```
## 04 Ingrese algunos registros para ambas tablas:
codigo sql
```sql
insert into provincias values(1,'Cordoba');
insert into provincias values(2,'Santa Fe');
insert into provincias values(3,'Misiones');
insert into provincias values(4,'Rio Negro');
insert into clientes values(100,'Perez Juan','San Martin 123','Carlos Paz',1);
insert into clientes values(101,'Moreno Marcos','Colon 234','Rosario',2);
insert into clientes values(102,'Acosta Ana','Avellaneda 333','Posadas',3);
insert into clientes values(103,'Luisa Lopez','Juarez 555','La Plata',6);

```
## 05 Intente agregar la restricción "foreign key" del punto 2 a la tabla "clientes"
No se puede porque hay un registro en "clientes" cuyo valor de "codigoprovincia" no existe en "provincias".
codigo sql
```sql
ALTER TABLE clientes ADD CONSTRAINT fk_clientes_provincias FOREIGN KEY (codigoprovincia) REFERENCES provincias (codigo);

```
## 06 Elimine el registro de "clientes" que no cumple con la restricción y establezca la restricción nuevamente.
codigo sql
```sql
DELETE FROM clientes WHERE codigoprovincia NOT IN (SELECT codigo FROM provincias);
ALTER TABLE clientes ADD CONSTRAINT fk_clientes_provincias FOREIGN KEY (codigoprovincia) REFERENCES provincias (codigo);
```
## 07 Intente agregar un cliente con un código de provincia inexistente en "provincias"
codigo sql
```sql
INSERT INTO clientes VALUES (104, 'Gonzalez Maria', 'Belgrano 456', 'Córdoba', 5);

```
## 08 Intente eliminar el registro con código 3, de "provincias". No se puede porque hay registros en "clientes" al cual hace referencia.
codigo sql
```sql
DELETE FROM provincias WHERE codigo = 3;

```
## 09 Elimine el registro con código "4" de "provincias" Se permite porque en "clientes" ningún registro hace referencia a él.
codigo sql
```sql
DELETE FROM provincias WHERE codigo = 4;

```
## 10 Intente modificar el registro con código 1, de "provincias" No se puede porque hay registros en "clientes" al cual hace referencia
codigo sql
```sql
UPDATE provincias SET nombre = 'Buenos Aires' WHERE codigo = 1;

```
## 11 Vea las restricciones de "clientes" consultando "user_constraints"
codigo sql
```sql
SELECT constraint_name, constraint_type, table_name
FROM user_constraints
WHERE table_name = 'CLIENTES';
```
## 12 Vea las restricciones de "provincias"
codigo sql
```sql
SELECT constraint_name, constraint_type, table_name
FROM user_constraints
WHERE table_name = 'PROVINCIAS';
```
## 13 Intente eliminar la tabla "provincias" (mensaje de error)
codigo sql
```sql
DROP TABLE provincias;

```
## 14 Elimine la restricción "foreign key" de "clientes" y luego elimine la tabla "provincias"
codigo sql
```sql
ALTER TABLE clientes DROP CONSTRAINT fk_clientes_provincias;
DROP TABLE provincias;
```
