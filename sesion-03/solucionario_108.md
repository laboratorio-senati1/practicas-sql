## Ejercicios propuestos
# Una empresa almacena los datos de sus empleados en una tabla denominada "empleados" y en otra llamada "control" guarda un registro por cada vez que un empleado actualiza datos sobre la tabla "empleados".

# 01 Elimine las tablas: 
```sql
drop table empleados;
drop table control;

```
# 02 Cree las tablas con las siguientes estructuras: 
```sql
create table empleados(
    documento char(8),
    apellido varchar2(20),
    nombre varchar2(20),
    seccion varchar2(30),
    sueldo number(8,2)
);

create table control(
    usuario varchar2(30),
    fecha date
);

```
# 03 Ingrese algunos registros en "empleados": 
```sql
insert into empleados values('22333444','ACOSTA','Ana','Secretaria',500);
insert into empleados values('22777888','DOMINGUEZ','Daniel','Secretaria',560);
insert into empleados values('22999000','FUENTES','Federico','Sistemas',680);
insert into empleados values('22555666','CASEROS','Carlos','Contaduria',900);
insert into empleados values('23444555','GOMEZ','Gabriela','Sistemas',1200);
insert into empleados values('23666777','JUAREZ','Juan','Contaduria',1000);

```
# 04 Cree un disparador que se dispare cada vez que se actualice un registro en "empleados"; el trigger debe ingresar en la tabla "control", el nombre del usuario y la fecha en la cual se realizó un "update" sobre "empleados" 
```sql
create or replace trigger actualizacion_empleado
after update on empleados
for each row
begin
  insert into control(usuario, fecha) values(user, sysdate);
end;
/
```
# 05 Actualice varios registros de "empleados". Aumentamos en un 10% el sueldo de todos los empleados de la seccion "Secretaria'

```sql
update empleados
set sueldo = sueldo * 1.1
where seccion = 'Secretaria';
```
# 06 Vea cuántas veces se disparó el trigger consultando la tabla "control"
```sql
select count(*) from control;
```
# 07 El trigger se disparó 2 veces, una vez por cada registro modificado en "empleados". Si el trigger hubiese sido creado a nivel de sentencia, el "update" anterior hubiese disparado el trigger 1 sola vez aún cuando se modifican 2 filas.
```sql
select * from control;
```
# 08 Vea qué informa el diccionario "user_triggers" respecto del trigger anteriormente creado obtenemos el nombre del disparador, el momento y nivel (before each row), evento que lo dispara (update), a qué objeto está asociado (table), nombre de la tabla al que está asociado (empleados) y otras columnas que no analizaremos por el momento.
```sql
select * from user_triggers where trigger_name = 'ACTUALIZACION_EMPLEADO';
```