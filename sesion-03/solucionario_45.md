## Ejercicios propuestos
# Un profesor guarda algunos datos de sus alumnos en una tabla llamada "alumnos". 
## 01 Elimine la tabla y créela con la siguiente estructura:
codigo sql
```sql
drop table alumnos;

create table alumnos(
    legajo char(5) not null,
    documento char(8) not null,
    nombre varchar2(30),
    curso char(1) not null,
    materia varchar2(20) not null,
    notafinal number(4,2)
);

```
## 02 Cree un índice no único para el campo "nombre".
codigo sql
```sql
CREATE INDEX idx_nombre ON alumnos(nombre);

```
## 03 Establezca una restricción "primary key" para el campo "legajo"
codigo sql
```sql
ALTER TABLE alumnos ADD CONSTRAINT PK_alumnos_legajo PRIMARY KEY (legajo);

```
## 04 Verifique que se creó un índice con el nombre de la restricción
codigo sql
```sql
SELECT index_name
FROM user_indexes
WHERE table_name = 'ALUMNOS' AND index_name = 'PK_ALUMNOS_LEGAJO';
```
## 05 Verifique que se creó un índice único con el nombre de la restricción consultando el diccionario de índices.
codigo sql
```sql
SELECT index_name, uniqueness
FROM user_indexes
WHERE table_name = 'ALUMNOS' AND index_name = 'PK_ALUMNOS_LEGAJO';
```
## 06 Intente eliminar el índice "PK_alumnos_legajo" con "drop index".
codigo sql
```sql
DROP INDEX PK_alumnos_legajo;
```
## 07 Cree un índice único para el campo "documento".
codigo sql
```sql
CREATE UNIQUE INDEX idx_documento ON alumnos(documento);
```
## 08 Agregue a la tabla una restricción única sobre el campo "documento" y verifique que no se creó un índice, Oracle emplea el índice creado en el punto anterior.
codigo sql
```sql
ALTER TABLE alumnos ADD CONSTRAINT UK_alumnos_documento UNIQUE (documento);

```
## 09 Intente eliminar el índice "I_alumnos_documento" (no se puede porque una restricción lo está utilizando)
codigo sql
```sql
DROP INDEX idx_documento;
```
## 10 Elimine la restricción única establecida sobre "documento".
codigo sql
```sql
ALTER TABLE alumnos DROP CONSTRAINT UK_alumnos_documento;

```
## 11 Verifique que el índice "I_alumnos_documento" aún existe.
codigo sql
```sql
SELECT index_name
FROM user_indexes
WHERE table_name = 'ALUMNOS' AND index_name = 'I_ALUMNOS_DOCUMENTO';
```
## 12 Elimine el índice "I_alumnos_documento", ahora puede hacerlo porque no hay restricción que lo utilice.
codigo sql
```sql
DROP INDEX I_alumnos_documento;

```
## 13 Elimine el índice "I_alumnos_nombre".
codigo sql
```sql
DROP INDEX idx_nombre;

```
## 14 Elimine la restricción "primary key"/
codigo sql
```sql
ALTER TABLE alumnos DROP CONSTRAINT PK_alumnos_legajo;

``` 
## 15 Verifique que el índice "PK_alumnos_legajo" fue eliminado (porque fue creado por Oracle al establecerse la restricción)
codigo sql
```sql
SELECT index_name
FROM user_indexes
WHERE table_name = 'ALUMNOS' AND index_name = 'PK_ALUMNOS_LEGAJO';
```
## 16 Cree un índice compuesto por los campos "curso" y "materia", no único.
codigo sql
```sql
CREATE INDEX idx_curso_materia ON alumnos(curso, materia);

```
## 17 Verifique su existencia.
codigo sql
```sql
SELECT index_name
FROM user_indexes
WHERE table_name = 'ALUMNOS' AND index_name = 'IDX_CURSO_MATERIA';
```
## 18 Elimine la tabla "alumnos" y verifique que todos los índices han sido eliminados junto con ella.
codigo sql
```sql
DROP TABLE alumnos;

SELECT index_name
FROM user_indexes
WHERE table_name = 'ALUMNOS';
```