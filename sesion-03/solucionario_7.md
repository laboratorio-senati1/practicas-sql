# ejercicio 01 
## Trabaje con la tabla "agenda" que registra la información referente a sus amigos. 

## 01 Elimine la tabla.
codigo sql
```sql
drop table agenda;
```
salida sql
```sh
Table AGENDA dropped.
```
## 02 Cree la tabla con los siguientes campos: apellido (cadena de 30), nombre (cadena de 20), domicilio (cadena de 30) y telefono (cadena de 11):
codigo sql
```sql
create table agenda(
    apellido varchar2(30),
    nombre varchar2(20),
    domicilio varchar2(30),
    telefono varchar2(11)
);

```
salida sql
```sh
Table AGENDA created.
```
## 03 Ingrese los siguientes registros (insert into):
codigo sql
```sql
insert into agenda(apellido,nombre,domicilio,telefono) values ('Alvarez','Alberto','Colon 123','4234567');
insert into agenda(apellido,nombre,domicilio,telefono) values ('Juarez','Juan','Avellaneda 135','4458787');
insert into agenda(apellido,nombre,domicilio,telefono) values ('Lopez','Maria','Urquiza 333','4545454');
insert into agenda(apellido,nombre,domicilio,telefono) values ('Lopez','Jose','Urquiza 333','4545454');
insert into agenda(apellido,nombre,domicilio,telefono) values ('Salas','Susana','Gral. Paz 1234','4123456');
```
salida sql
```sh
1 row inserted.


1 row inserted.


1 row inserted.


1 row inserted.


1 row inserted.
```
## 04 Elimine el registro cuyo nombre sea "Juan" (1 registro)
codigo sql
```sql
DELETE FROM agenda WHERE nombre = 'Juan';
```
salida sql
```sh
1 row deleted.
```
## 05 Elimine los registros cuyo número telefónico sea igual a "4545454" (2 registros)
codigo sql
```sql
DELETE FROM agenda WHERE telefono = '4545454';
```
salida sql
```sh
2 rows deleted.
```
## 06 Elimine todos los registros (2 registros)
codigo sql
```sql
DELETE FROM agenda;
```
salida sql
```sh
2 rows deleted.
```
# ejercicio 02
## Un comercio que vende artículos de computación registra los datos de sus artículos en una tabla con ese nombre.
## 01 Elimine "articulos"
codigo sql
```sql
drop table articulos;
```
salida sql
```sh
Table ARTICULOS dropped.
```
## 02 Cree la tabla, con la siguiente estructura:
codigo sql
```sql
create table articulos(
    codigo number(4,0),
    nombre varchar2(20),
    descripcion varchar2(30),
    precio number(7,2),
    cantidad number(3)
);

```
salida sql
```sh
Table ARTICULOS created.
```
## 03 Vea la estructura de la tabla.
codigo sql
```sql
select * from articulos;
```
salida sql
```sh
codigo nombre descripcion precio cantidad
```
## 04 Ingrese algunos registros:
codigo sql
```sql
insert into articulos (codigo, nombre, descripcion, precio,cantidad) values (1,'impresora','Epson Stylus C45',400.80,20);
insert into articulos (codigo, nombre, descripcion, precio,cantidad) values (2,'impresora','Epson Stylus C85',500,30);
insert into articulos (codigo, nombre, descripcion, precio,cantidad) values (3,'monitor','Samsung 14',800,10);
insert into articulos (codigo, nombre, descripcion, precio,cantidad) values (4,'teclado','ingles Biswal',100,50);
insert into articulos (codigo, nombre, descripcion, precio,cantidad) values (5,'teclado','español Biswal',90,50);
```
salida sql
```sh
1 row inserted.


1 row inserted.


1 row inserted.


1 row inserted.


1 row inserted.

```
## 05 Elimine los artículos cuyo precio sea mayor o igual a 500 (2 registros)
codigo sql
```sql
delete from articulos where precio >= 500;
```
salida sql
```sh
2 rows deleted.
```
## 06 Elimine todas las impresoras (1 registro)
codigo sql
```sql
delete from articulos where nombre='impresora';
```
salida sql
```sh
1 rows deleted.
```
## 07 Elimine todos los artículos cuyo código sea diferente a 4 (1 registro)
codigo sql
```sql
delete from articulos;
```
salida sql
```sh
1 rows deleted.
```
