## Ejercicios propuestos
# Una empresa almacena los datos de sus empleados en una tabla denominada "empleados" y en otra tabla llamada "control", guarda el nombre del usuario, la fecha y el tipo de operación que se realiza en la tabla "empleados".
# 01 Elimine las tablas "empleados" y "control":
```sql
drop table empleados;
drop table control;

```
# 02 Cree las tablas:
```sql
create table empleados(
    documento char(8),
    apellido varchar2(30),
    nombre varchar2(30),
    domicilio varchar2(30),
    seccion varchar2(20),
    sueldo number(8,2)
);

create table control(
    usuario varchar2(30),
    fecha date,
    operacion varchar2(30)
);

```
# 03 Ingrese algunos registros:
```sql
insert into empleados values('22222222','Acosta','Ana','Avellaneda 11','Secretaria',1800);
insert into empleados values('23333333','Bustos','Betina','Bulnes 22','Gerencia',5000);
insert into empleados values('24444444','Caseres','Carlos','Colon 333','Contaduria',3000);

```
# 04 Cree un trigger de inserción sobre "empleados" que guarde en "control" el nombre del usuario que ingresa datos, la fecha y "insercion", en el campo "operacion". Pero, si el sueldo que se intenta ingresar supera los $5000, debe mostrarse un mensaje de error y deshacer la transacción
```sql
CREATE OR REPLACE TRIGGER trg_insert_empleados
BEFORE INSERT ON empleados
FOR EACH ROW
DECLARE
    v_usuario VARCHAR2(30) := USER;
BEGIN
    IF :NEW.sueldo > 5000 THEN
        RAISE_APPLICATION_ERROR(-20001, 'El sueldo no puede superar los $5000');
    ELSE
        INSERT INTO control (usuario, fecha, operacion) VALUES (v_usuario, SYSDATE, 'insercion');
    END IF;
END;
```
# 05 Cree un trigger de borrado sobre "empleados" que guarde en "control" los datos requeridos (en "operacion" debe almacenar "borrado". Si se intenta eliminar un empleado de la sección "gerencia", debe aparecer un mensaje de error y deshacer la operación
```sql
CREATE OR REPLACE TRIGGER trg_delete_empleados
BEFORE DELETE ON empleados
FOR EACH ROW
DECLARE
    v_usuario VARCHAR2(30) := USER;
BEGIN
    IF :OLD.seccion = 'Gerencia' THEN
        RAISE_APPLICATION_ERROR(-20002, 'No se puede eliminar un empleado de la sección "Gerencia"');
    ELSE
        INSERT INTO control (usuario, fecha, operacion) VALUES (v_usuario, SYSDATE, 'borrado');
    END IF;
END;
```
# 06 Cree un trigger de actualización. Ante cualquier modificación de los registros de "empleados", se debe ingresar en la tabla "control", el nombre del usuario que realizó la actualización, la fecha y "actualizacion". Pero, controlamos que NO se permita modificar el campo "documento", en caso de suceder, la acción no debe realizarse y debe mostrarse un mensaje de error indicándolo
```sql
CREATE OR REPLACE TRIGGER trg_update_empleados
BEFORE UPDATE ON empleados
FOR EACH ROW
DECLARE
    v_usuario VARCHAR2(30) := USER;
BEGIN
    IF UPDATING('DOCUMENTO') THEN
        RAISE_APPLICATION_ERROR(-20003, 'No se permite modificar el campo "documento"');
    ELSE
        INSERT INTO control (usuario, fecha, operacion) VALUES (v_usuario, SYSDATE, 'actualizacion');
    END IF;
END;
```
# 07 Intente ingresar un empleado con sueldo superior a $5000:
```sql
insert into empleados values('25555555','Duarte','Dario','Dominicana 444','Secretaria',5800);

```
# 08 Ingrese un empleado con valores permitidos:
```sql
insert into empleados values('25555555','Duarte','Dario','Dominicana 444','Secretaria',2800);
```
# 09 Intente borrar un empleado de "gerencia" Aparece un mensaje de error.
```sql
DELETE FROM empleados WHERE seccion = 'Gerencia';
```
# 10 Elimine un empleado que no sea de "Gerencia"
```sql
DELETE FROM empleados WHERE seccion != 'Gerencia' AND ROWNUM = 1;
```
# 11 Intente modificar el documento de un empleado Mensaje de error.
```sql
UPDATE empleados SET documento = '28888888' WHERE documento = '22222222';
```
# 12 Modifique un campo diferente de "documento"
```sql
UPDATE empleados SET sueldo = 3500 WHERE documento = '23333333';
```
# 13 Vea que se ha almacenado hasta el momento en "control" Debe haber 3 registros, de inserción, de borrado y actualización.
```sql
SELECT * FROM control;
```
