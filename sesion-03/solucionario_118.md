## Ejercicios propuestos
# Una escuela necesita crear 2 usuarios diferentes en su base de datos. Uno denominado "director" y otro "profesor". Luego se les concederán diferentes permisos para retringir el acceso a los diferentes objetos. Conéctese como administrador (por ejemplo "system").
# 01 Primero eliminamos el usuario "director", porque si existe, aparecerá un mensaje de error:
```sql
drop user director cascade;
```
# 02 Cree un usuario "director", con contraseña "escuela" y 100M de espacio en "system"
```sql
CREATE USER director IDENTIFIED BY escuela QUOTA 100M ON system;

```
# 03 Elimine el usuario "profesor":
```sql
drop user profesor cascade;
```
# 04 Cree un usuario "profesor", con contraseña "maestro" y espacio en "system"
```sql
CREATE USER profesor IDENTIFIED BY maestro QUOTA UNLIMITED ON system;
```
# 05 Consulte el diccionario "dba_users" y analice la información que nos muestra
```sql
SELECT * FROM dba_users;
```
