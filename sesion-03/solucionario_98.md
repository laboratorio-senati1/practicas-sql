## Ejercicios propuestos
# Un profesor guarda los promedios de sus alumnos de un curso en una tabla llamada "alumnos".
# 01  Elimine la tabla.
```sql
 drop table alumnos;
```
# 02 Cree la tabla:
```sql
create table alumnos(
    legajo number,
    promedio number(4,2)
);

```
# 03 Ingrese los siguientes registros:
```sql
insert into alumnos values(1, 8.5);
insert into alumnos values(2, 5.2);
insert into alumnos values(3, 3.8);
insert into alumnos values(4, 9.7);
insert into alumnos values(5, 6.1);

```
# 04 Si el alumno tiene un promedio menor a 4, muestre un mensaje "reprobado", si el promedio es mayor o igual a 4 y menor a 7, muestre "regular", si el promedio es mayor o igual a 7, muestre "promocionado", usando "case" (recuerde que "case" toma valores puntuales, emplee "trunc")
```sql
select legajo, promedio,
    case
        when trunc(promedio) < 4 then 'reprobado'
        when trunc(promedio) >= 4 and trunc(promedio) < 7 then 'regular'
        else 'promocionado'
    end as condicion
from alumnos;

```
# 05 Elimine la tabla "alumnos"
```sql
drop table alumnos;

```
# 06 La nueva tabla contendrá varias notas por alumno. Cree la tabla:
```sql
create table alumnos(
    legajo number,
    nota number(3)
);

```
# 07 Ingrese los siguientes registros:
```sql
insert into alumnos values(1, 8);
insert into alumnos values(1, 7);
insert into alumnos values(1, 9);
insert into alumnos values(2, 5);
insert into alumnos values(2, 6);
insert into alumnos values(2, 4);
insert into alumnos values(3, 3);
insert into alumnos values(3, 2);
insert into alumnos values(3, 4);
insert into alumnos values(4, 10);
insert into alumnos values(4, 9);
insert into alumnos values(4, 8);
insert into alumnos values(5, 6);
insert into alumnos values(5, 7);
insert into alumnos values(5, 6);

```
# 08 Si el alumno tiene un promedio menor a 4, muestre un mensaje "reprobado", si el promedio es mayor o igual a 4 y menor a 7, muestre "regular", si el promedio es mayor o igual a 7, muestre "promocionado", usando "case" (recuerde que "case" toma valores puntuales, emplee "trunc"). Para obtener el promedio agrupe por legajo y emplee la función "avg"
```sql
select legajo, trunc(avg(nota)) as promedio,
    case
        when trunc(avg(nota)) < 4 then 'reprobado'
        when trunc(avg(nota)) >= 4 and trunc(avg(nota)) < 7 then 'regular'
        else 'promocionado'
    end as condicion
from alumnos
group by legajo;

```
# 09 Cree una tabla denominada "alumnosCondicion" con los campos "legajo", "notafinal" y "condicion":
```sql
create table alumnosCondicion(
    legajo number,
    notafinal number(4,2),
    condicion varchar2(20)
);

```
# 10 Cree o reemplace un procedimiento almacenado llamado "pa_CargarCondicion" que guarde en la tabla "alumnosCondicion" el legajo de cada alumno, el promedio de sus notas y la condición (libre, regular o promocionado)
```sql
create or replace procedure pa_CargarCondicion is
begin
    insert into alumnosCondicion
    select legajo, trunc(avg(nota)) as notafinal,
        case
            when trunc(avg(nota)) < 4 then 'reprobado'
            when trunc(avg(nota)) >= 4 and trunc(avg(nota)) < 7 then 'regular'
            else 'promocionado'
        end as condicion
    from alumnos
    group by legajo;
    commit;
end;
/

```
# 11 Ejecute el procedimiento "pa_cargarCondicion" y recupere todos los datos de la tabla "alumnoscondicion"
```sql
exec pa_CargarCondicion;

select * from alumnosCondicion;

```