## Ejercicios propuestos
# Una empresa almacena los datos de sus empleados en una tabla denominada "empleados" y en otra llamada "control" guarda un registro por cada empleado que se elimina de la tabla "empleados".
# 01 Elimine las tablas:
```sql
drop table empleados;
drop table control;

```
# 02 Cree las tablas con las siguientes estructuras:
```sql
create table empleados(
    documento char(8),
    apellido varchar2(20),
    nombre varchar2(20),
    seccion varchar2(30),
    sueldo number(8,2)
);

create table control(
  usuario varchar2(30),
  fecha date
);
```
# 03 Ingrese algunos registros en "empleados":
```sql
insert into empleados values('22333444','ACOSTA','Ana','Secretaria',500);
insert into empleados values('22777888','DOMINGUEZ','Daniel','Secretaria',560);
insert into empleados values('22999000','FUENTES','Federico','Sistemas',680);
insert into empleados values('22555666','CASEROS','Carlos','Contaduria',900);
insert into empleados values('23444555','GOMEZ','Gabriela','Sistemas',1200);
insert into empleados values('23666777','JUAREZ','Juan','Contaduria',1000);

```
# 04 Cree un disparador a nivel de fila, que se dispare cada vez que se borre un registro de "empleados"; el trigger debe ingresar en la tabla "control", el nombre del usuario y la fecha en la cual se realizó un "delete" sobre "empleados"
```sql
create or replace trigger borrado_empleado
after delete on empleados
for each row
begin
  insert into control(usuario, fecha) values(user, sysdate);
end;
/
```
# 05 Vea qué informa el diccionario "user_triggers" respecto del trigger anteriormente creado
```sql
select * from user_triggers where trigger_name = 'BORRADO_EMPLEADO';
```
# 06 Elimine todos los empleados cuyo sueldo supera los $800
```sql
delete from empleados where sueldo > 800;
```
# 07 Vea si el trigger se disparó consultando la tabla "control" Se eliminaron 3 registros, como el trigger fue definido a nivel de fila, se disparó 3 veces, una vez por cada registro eliminado. Si el trigger hubiese sido definido a nivel de sentencia, se hubiese disparado una sola vez. 
```sql
select * from control;
```
# 08 Reemplace el disparador creado anteriormente por otro con igual código pero a nivel de sentencia
```sql
create or replace trigger borrado_empleado
before delete on empleados
begin
  insert into control(usuario, fecha) values(user, sysdate);
end;
/
```
# 09 Vea qué nos informa el diccionario "user_triggers" respecto del trigger anteriormente creado en este caso es un desencadenador a nivel de sentencia; en la columna "TRIGGER_TYPE" muestra "BEFORE STATEMENT".
```sql
select * from user_triggers where trigger_name = 'BORRADO_EMPLEADO';
```
# 10 Elimine todos los empleados de la sección "Secretaria" Se han eliminado 2 registros, pero el trigger se ha disparado una sola vez.
```sql
delete from empleados where seccion = 'Secretaria';
```
# 11 Consultamos la tabla "control" Si el trigger hubiese sido definido a nivel de fila, se hubiese disparado dos veces.
```sql
select * from control;
```