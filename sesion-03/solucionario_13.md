# Ejercicio de laboratorio
## Trabajamos con la tabla "libros" que almacena los datos de los libros de una librería.
## 01 Eliminamos la tabla:
codigo sql
```sql
drop table libros;
```

## 02 Creamos la tabla:
codigo sql
```sql
create table libros(
    codigo number(4),
    titulo varchar2(30),
    autor varchar2(20),
    editorial varchar2(15),
    precio number(5,2)
);
```

## 03 Agregamos algunos registros:
codigo sql
```sql
insert into libros (codigo,titulo,autor,editorial,precio) values (1,'El aleph','Borges','Emece',25.60);
insert into libros (codigo,titulo,autor,editorial,precio) values (2,'Uno','Richard Bach','Planeta',18);
```

## 04 Seleccionamos todos los registros:
codigo sql
```sql
select *from libros;

```

## 05 Truncamos la tabla:
codigo sql
```sql
truncate table libros;
```

## 06 Si consultamos la tabla, vemos que aún existe pero ya no tiene registros:
codigo sql
```sql
select *from libros;
```

## 07 Ingresamos nuevamente algunos registros:
codigo sql
```sql
insert into libros (codigo,titulo,autor,editorial,precio) values (1,'El aleph','Borges','Emece',25.60);
insert into libros (codigo,titulo,autor,editorial,precio) values (2,'Uno','Richard Bach','Planeta',18);
```

## 08 Eliminemos todos los registros con "delete":
codigo sql
```sql
delete from libros;
```

## 09 Si consultamos la tabla, vemos que aún existe pero ya no tiene registros:
codigo sql
```sql
select *from libros;
```

## 10 Ingresamos nuevamente algunos registros:
codigo sql
```sql
insert into libros (codigo,titulo,autor,editorial,precio) values (1,'El aleph','Borges','Emece',25.60);
insert into libros (codigo,titulo,autor,editorial,precio) values (2,'Uno','Richard Bach','Planeta',18);
```

## 11 Eliminamos la tabla:
codigo sql
```sql
drop table libros;
```

## 12 Intentamos seleccionar todos los registros:
codigo sql
```sql
select *from libros;
```

## 13 Aparece un mensaje de error, la tabla no existe.
# Ingresemos el siguiente lote de comandos en el Oracle SQL Developer:

drop table libros;

create table libros(
    codigo number(4),
    titulo varchar2(30),
    autor varchar2(20),
    editorial varchar2(15),
    precio number(5,2)
);

insert into libros (codigo,titulo,autor,editorial,precio) values (1,'El aleph','Borges','Emece',25.60);
insert into libros (codigo,titulo,autor,editorial,precio) values (2,'Uno','Richard Bach','Planeta',18);

select *from libros;

truncate table libros;

select *from libros;

insert into libros (codigo,titulo,autor,editorial,precio) values (1,'El aleph','Borges','Emece',25.60);
insert into libros (codigo,titulo,autor,editorial,precio) values (2,'Uno','Richard Bach','Planeta',18);

delete from libros;

select *from libros;

insert into libros (codigo,titulo,autor,editorial,precio) values (1,'El aleph','Borges','Emece',25.60);
insert into libros (codigo,titulo,autor,editorial,precio) values (2,'Uno','Richard Bach','Planeta',18);

drop table libros;

select *from libros;
