## Ejercicios propuestos
# Un club dicta clases de distintos deportes a sus socios. El club tiene una tabla llamada "deportes" en la cual almacena el nombre del deporte, el nombre del profesor que lo dicta, el día de la semana que se dicta y el costo de la cuota mensual.
# 01 Elimine la tabla:
```sql
drop table deportes;
```
# 02 Cree la tabla:
```sql
create table deportes(
    nombre varchar2(15),
    profesor varchar2(30),
    dia varchar2(10),
    cuota number(5,2)
);
```
# 03 Ingrese algunos registros. Incluya profesores que dicten más de un curso:
```sql
insert into deportes values('tenis','Ana Lopez','lunes',20);
insert into deportes values('natacion','Ana Lopez','martes',15);
insert into deportes values('futbol','Carlos Fuentes','miercoles',10);
insert into deportes values('basquet','Gaston Garcia','jueves',15);
insert into deportes values('padle','Juan Huerta','lunes',15);
insert into deportes values('handball','Juan Huerta','martes',10);
```
# 04 Muestre los nombres de los profesores que dictan más de un deporte empleando subconsulta (2 registros)
```sql
SELECT DISTINCT d1.profesor
FROM deportes d1
WHERE (SELECT COUNT(*) FROM deportes d2 WHERE d2.profesor = d1.profesor) > 1;

```
# 05 Obtenga el mismo resultado empleando join
```sql
SELECT DISTINCT d1.profesor
FROM deportes d1
JOIN deportes d2 ON d1.profesor = d2.profesor
WHERE d1.nombre <> d2.nombre;

```
# 06 Buscamos todos los deportes que se dictan el mismo día que un determinado deporte (natacion) empleando subconsulta (1 registro)
```sql
SELECT nombre
FROM deportes
WHERE dia = (SELECT dia FROM deportes WHERE nombre = 'natacion');

```
# 07 Obtenga la misma salida empleando "join"
```sql
SELECT d1.nombre
FROM deportes d1
JOIN deportes d2 ON d1.dia = d2.dia
WHERE d2.nombre = 'natacion';

```