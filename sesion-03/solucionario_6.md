# ejercicio 01
## Un comercio que vende artículos de computación registra los datos de sus artículos en una tabla con ese nombre.

## 01 Elimine "articulos"
codigo sql
```sql
drop table articulos;
```
salida sql
```sh
Table ARTICULOS dropped.
```
## 02 Cree la tabla, con la siguiente estructura:
codigo sql
```sql
create table articulos(
    codigo number(5),
    nombre varchar2(20),
    descripcion varchar2(30),
    precio number(6,2),
    cantidad number(3)
);

```
salida sql
```sh
Table ARTICULOS created.
```
## 03 Vea la estructura de la tabla.
codigo sql
```sql
describe articulos;
```
salida sql
```sh
Name        Null? Type         
----------- ----- ------------ 
CODIGO            NUMBER(5)    
NOMBRE            VARCHAR2(20) 
DESCRIPCION       VARCHAR2(30) 
PRECIO            NUMBER(6,2)  
CANTIDAD          NUMBER(3)  
```
## 04 Ingrese algunos registros:
codigo sql
```sql
insert into articulos (codigo, nombre, descripcion, precio,cantidad) values (1,'impresora','Epson Stylus C45',400.80,20);
insert into articulos (codigo, nombre, descripcion, precio,cantidad) values (2,'impresora','Epson Stylus C85',500,30);
insert into articulos (codigo, nombre, descripcion, precio,cantidad) values (3,'monitor','Samsung 14',800,10);
insert into articulos (codigo, nombre, descripcion, precio,cantidad) values (4,'teclado','ingles Biswal',100,50);
insert into articulos (codigo, nombre, descripcion, precio,cantidad) values (5,'teclado','español Biswal',90,50);

```
salida sql
```sh
1 row inserted.


1 row inserted.


1 row inserted.


1 row inserted.


1 row inserted.
```
## 05 Seleccione los datos de las impresoras (2 registros)
codigo sql
```sql
select * from articulos where nombre='impresora';
```
salida sql
```sh
    CODIGO NOMBRE               DESCRIPCION                        PRECIO   CANTIDAD
---------- -------------------- ------------------------------ ---------- ----------
         1 impresora            Epson Stylus C45                    400.8         20
         2 impresora            Epson Stylus C85                      500         30

```
## 06 Seleccione los artículos cuyo precio sea mayor o igual a 400 (3 registros)
codigo sql

```sql
select * from articulos where precio >= 400;
```
salida sql
```sh
    CODIGO NOMBRE               DESCRIPCION                        PRECIO   CANTIDAD
---------- -------------------- ------------------------------ ---------- ----------
         1 impresora            Epson Stylus C45                    400.8         20
         2 impresora            Epson Stylus C85                      500         30
         3 monitor              Samsung 14                            800         10

```
## 07 Seleccione el código y nombre de los artículos cuya cantidad sea menor a 30 (2 registros)
codigo sql
```sql
select codigo,nombre from articulos where cantidad < 30;
```
salida sql
```sh
    CODIGO NOMBRE              
---------- --------------------
         1 impresora           
         3 monitor             
```
## 08 Selecciones el nombre y descripción de los artículos que NO cuesten $100 (4 registros)
codigo sql
```sql
select nombre,descripcion from articulos where precio <> 100;
```
salida sql
```sh                                              
NOMBRE                DESCRIPCION                   
--------------------  ------------------------------
impresora             Epson Stylus C45              
impresora             Epson Stylus C85              
monitor               Samsung 14                    
teclado               español Biswal  
```
# ejercicio 02 
## Un video club que alquila películas en video almacena la información de sus películas en alquiler en una tabla denominada "peliculas".

## 01 Elimine la tabla.
codigo sql
```sql
drop table peliculas;
```
salida sql
```sh
Table PELICULAS dropped.
```
## 02 Cree la tabla eligiendo el tipo de dato adecuado para cada campo:
codigo sql
```sql
create table peliculas(
    titulo varchar2(20),
    actor varchar2(20),
    duracion number(3),
    cantidad number(1)
);
```
salida sql
```sh
Table PELICULAS created.
```
## 03 Ingrese los siguientes registros:
codigo sql
```sql
insert into peliculas (titulo, actor, duracion, cantidad) values ('Mision imposible','Tom Cruise',120,3);
insert into peliculas (titulo, actor, duracion, cantidad) values ('Mision imposible 2','Tom Cruise',180,4);
insert into peliculas (titulo, actor, duracion, cantidad) values ('Mujer bonita','Julia R.',90,1);
insert into peliculas (titulo, actor, duracion, cantidad) values ('Elsa y Fred','China Zorrilla',80,2);
```
salida sql
```sh
1 row inserted.


1 row inserted.


1 row inserted.


1 row inserted.
```
## 04 Seleccione las películas cuya duración no supere los 90 minutos (2 registros)
codigo sql
```sql
select * from peliculas where duracion < 90;
```
salida sql
```sh
TITULO               ACTOR                  DURACION   CANTIDAD
-------------------- -------------------- ---------- ----------
Elsa y Fred          China Zorrilla               80          2
```
## 05 Seleccione el título de todas las películas en las que el actor NO sea "Tom Cruise" (2 registros)
codigo sql
```sql
SELECT titulo FROM peliculas WHERE actor <> 'Tom Cruise';
```
salida sql
```sh
TITULO              
--------------------
Mujer bonita
Elsa y Fred
```
## 06 Muestre todos los campos, excepto "duracion", de todas las películas de las que haya más de 2 copias (2 registros)
codigo sql
```sql
SELECT titulo,actor,cantidad FROM peliculas;
```
salida sql
```sh
TITULO               ACTOR                  CANTIDAD
-------------------- -------------------- ----------
Mision imposible     Tom Cruise                    3
Mision imposible 2   Tom Cruise                    4
Mujer bonita         Julia R.                      1
Elsa y Fred          China Zorrilla                2
```

