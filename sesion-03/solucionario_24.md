# Ejercicios propuestos

## Trabaje con la tabla llamada "medicamentos" de una farmacia.

## 01 Elimine la tabla y créela con la siguiente estructura:
codigo sql
```sql
drop table medicamentos;

create table medicamentos(
    codigo number(5),
    nombre varchar2(20),
    laboratorio varchar2(20),
    precio number(5,2),
    cantidad number(3),
    primary key(codigo)
);
```
## 02 Ingrese algunos registros:
codigo sql
```sql
insert into medicamentos
values(100,'Sertal','Roche',5.2,100);

insert into medicamentos
values(102,'Buscapina','Roche',4.10,200);

insert into medicamentos
values(205,'Amoxidal 500','Bayer',15.60,100);

insert into medicamentos
values(230,'Paracetamol 500','Bago',1.90,200);

insert into medicamentos
values(345,'Bayaspirina','Bayer',2.10,150);

insert into medicamentos
values(347,'Amoxidal jarabe','Bayer',5.10,250);

```
## 03 Recupere los códigos y nombres de los medicamentos cuyo laboratorio sea "Roche' y cuyo precio sea menor a 5 (1 registro cumple con ambas condiciones)
codigo sql
```sql
SELECT Codigo, Nombre FROM medicamentos WHERE Laboratorio = 'Roche' AND Precio < 5;
```
## 04 Recupere los medicamentos cuyo laboratorio sea "Roche" o cuyo precio sea menor a 5 (4 registros)
codigo sql
```sql
SELECT * FROM medicamentos WHERE laboratorio = 'Roche' OR Precio < 5;
```
## 05 Muestre todos los medicamentos cuyo laboratorio NO sea "Bayer" y cuya cantidad sea=100. Luego muestre todos los medicamentos cuyo laboratorio sea "Bayer" y cuya cantidad NO sea=100
codigo sql
```sql
SELECT * FROM Medicamentos WHERE Laboratorio <> 'Bayer' AND Cantidad = 100;
```
## 06 Recupere los nombres de los medicamentos cuyo precio esté entre 2 y 5 inclusive (2 registros)
codigo sql
```sql
SELECT Nombre FROM Medicamentos WHERE Precio BETWEEN 2 AND 5;
```
## 07 Elimine todos los registros cuyo laboratorio sea igual a "Bayer" y su precio sea mayor a 10 (1 registro eliminado)
codigo sql
```sql
DELETE FROM Medicamentos WHERE Laboratorio = 'Bayer' AND Precio > 10;
```
## 08 Cambie la cantidad por 200, de todos los medicamentos de "Roche" cuyo precio sea mayor a 5 (1 registro afectado)
codigo sql
```sql
UPDATE Medicamentos SET Cantidad = 200 WHERE Laboratorio = 'Roche' AND Precio > 5;
```
## 09 Muestre todos los registros para verificar el cambio.
codigo sql
```sql
select * from medicamentos;
```
## 10 Borre los medicamentos cuyo laboratorio sea "Bayer" o cuyo precio sea menor a 3 (3 registros borrados)
codigo sql
```sql
DELETE FROM Medicamentos WHERE Laboratorio = 'Bayer' OR Precio < 3;
```
# Ejercicio 02 
# Trabajamos con la tabla "peliculas" de un video club que alquila películas en video.

## 01 Elimine la tabla y créela con la siguiente estructura:
codigo sql
```sql
drop table peliculas;

create table peliculas(
    codigo number(4),
    titulo varchar2(40) not null,
    actor varchar2(20),
    duracion number(3),
    primary key (codigo)
);

```
## 02 Ingrese algunos registros:
codigo sql
```sql
insert into peliculas
values(1020,'Mision imposible','Tom Cruise',120);

insert into peliculas
values(1021,'Harry Potter y la piedra filosofal','Daniel R.',180);

insert into peliculas
values(1022,'Harry Potter y la camara secreta','Daniel R.',190);

insert into peliculas
values(1200,'Mision imposible 2','Tom Cruise',120);

insert into peliculas
values(1234,'Mujer bonita','Richard Gere',120);

insert into peliculas
values(900,'Tootsie','D. Hoffman',90);

insert into peliculas
values(1300,'Un oso rojo','Julio Chavez',100);

insert into peliculas
values(1301,'Elsa y Fred','China Zorrilla',110);
```
## 03 Recupere los registros cuyo actor sea "Tom Cruise" o "Richard Gere" (3 registros)
codigo sql
```sql
SELECT * FROM Peliculas WHERE Actor = 'Tom Cruise' OR Actor = 'Richard Gere';
```
## 04 Recupere los registros cuyo actor sea "Tom Cruise" y duración menor a 100 (ninguno cumple ambas condiciones)
codigo sql
```sql
SELECT * FROM Peliculas WHERE Actor = 'Tom Cruise' AND Duracion < 100;

```
## 05 Recupere los nombres de las películas cuya duración se encuentre entre 100 y 120 minutos(5 registros)
codigo sql
```sql
SELECT nombre FROM peliculas WHERE duracion BETWEEN 100 AND 120;
```
## 06 Cambie la duración a 200, de las películas cuyo actor sea "Daniel R." y cuya duración sea 180 (1 registro afectado)
codigo sql
```sql
UPDATE Peliculas SET Duracion = 200 WHERE Actor = 'Daniel R.' AND Duracion = 180;
```
## 07 Recupere todos los registros para verificar la actualización anterior
codigo sql
```sql
Select * from peliculas;
```
## 08 Borre todas las películas donde el actor NO sea "Tom Cruise" y cuya duración sea mayor o igual a 100 (2 registros eliminados)
codigo sql
```sql
DELETE FROM Peliculas WHERE Actor <> 'Tom Cruise' AND Duracion >= 100;
```
