# ejercicio 01 
## Una farmacia guarda información referente a sus medicamentos en una tabla llamada "medicamentos".

## 01 Elimine la tabla y créela con la siguiente estructura:
codigo sql
```sql
 drop table medicamentos;
 create table medicamentos(
    codigo number(5) not null,
    nombre varchar2(20) not null,
    laboratorio varchar2(20),
    precio number(5,2),
    cantidad number(3,0) not null
);
```
salida sql
```sh
Table MEDICAMENTOS dropped.
```
## 02 Visualice la estructura de la tabla "medicamentos" note que los campos "codigo", "nombre" y "cantidad", en la columna "Null" muestra "NOT NULL".
codigo sql
```sql
describe medicamentos;
```
salida sql
```sh
Name        Null?    Type         
----------- -------- ------------ 
CODIGO      NOT NULL NUMBER(5)    
NOMBRE      NOT NULL VARCHAR2(20) 
LABORATORIO          VARCHAR2(20) 
PRECIO               NUMBER(5,2)  
CANTIDAD    NOT NULL NUMBER(3)   
```
## 03 Ingrese algunos registros con valores "null" para los campos que lo admitan:
codigo sql
```sql
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(1,'Sertal gotas',null,null,100); 
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(2,'Sertal compuesto',null,8.90,150);
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(3,'Buscapina','Roche',null,200);

```
salida sql
```sh
1 row inserted.


1 row inserted.


1 row inserted.
```
## 04 Vea todos los registros.
codigo sql
```sql
select * from medicamentos;
```
salida sql
```sh
    CODIGO NOMBRE               LABORATORIO              PRECIO   CANTIDAD
---------- -------------------- -------------------- ---------- ----------
         1 Sertal gotas                                                100
         2 Sertal compuesto                                 8.9        150
         3 Buscapina            Roche                                  200  
```
## 05 Ingrese un registro con valor "0" para el precio y cadena vacía para el laboratorio.
codigo sql
```sql
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(9,'paracetamol','',0,250);

```
salida sql
```sh
1 row inserted.
```
## 06 Intente ingresar un registro con cadena vacía para el nombre (mensaje de error)
codigo sql
```sql
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(9,'','inkafarma',0,250);
```
salida sql
```sh
Error starting at line : 1 in command -
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(9,'','inkafarma',0,250)
Error at Command Line : 1 Column : 79
Error report -
SQL Error: ORA-01400: cannot insert NULL into ("BRAYAN"."MEDICAMENTOS"."NOMBRE")
01400. 00000 -  "cannot insert NULL into (%s)"
*Cause:    An attempt was made to insert NULL into previously listed objects.
*Action:   These objects cannot accept NULL values.
```
## 07 Intente ingresar un registro con valor nulo para un campo que no lo admite (aparece un mensaje de error)
codigo sql
```sql
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(9,'','inkafarma',0,null);
```
salida sql
```sh
Error starting at line : 1 in command -
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(9,'','inkafarma',0,null)
Error at Command Line : 1 Column : 79
Error report -
SQL Error: ORA-01400: cannot insert NULL into ("BRAYAN"."MEDICAMENTOS"."NOMBRE")
01400. 00000 -  "cannot insert NULL into (%s)"
*Cause:    An attempt was made to insert NULL into previously listed objects.
*Action:   These objects cannot accept NULL values.
```
## 08 Ingrese un registro con una cadena de 1 espacio para el laboratorio.
codigo sql
```sql
insert into medicamentos (codigo, nombre, laboratorio, precio, cantidad) values (4, 'Medicamento X', ' ', 12.50, 50);
```
salida sql
```sh
1 row inserted.
```
## 09 Recupere los registros cuyo laboratorio contenga 1 espacio (1 registro).
codigo sql
```sql
SELECT * FROM medicamentos WHERE laboratorio = ' ';
```
salida sql
```sh
    CODIGO NOMBRE               LABORATORIO              PRECIO   CANTIDAD
---------- -------------------- -------------------- ---------- ----------
         4 Medicamento X                                   12.5         50
```
## 10 Recupere los registros cuyo laboratorio sea distinto de ' '(cadena de 1 espacio) (1 registro).
codigo sql
```sql
SELECT * FROM medicamentos WHERE laboratorio <> ' ';
```
salida sql
```sh
    CODIGO NOMBRE               LABORATORIO              PRECIO   CANTIDAD
---------- -------------------- -------------------- ---------- ----------
         3 Buscapina            Roche                                  200
```
# Ejercicio 02 
## Trabaje con la tabla que almacena los datos sobre películas, llamada "peliculas".

## 01 Elimine la tabla:
codigo sql
```sql
drop table peliculas;
```
salida sql
```sh
Table PELICULAS dropped.
```
## 02 Créela con la siguiente estructura:
codigo sql
```sql
create table peliculas(
    codigo number(4) not null,
    titulo varchar2(40) not null,
    actor varchar2(20),
    duracion number(3)
);
```
salida sql
```sh
Table PELICULAS created.
```
## 03 Visualice la estructura de la tabla.
codigo sql
```sql
describe peliculas;
```
salida sql
```sh
Name     Null?    Type         
-------- -------- ------------ 
CODIGO   NOT NULL NUMBER(4)    
TITULO   NOT NULL VARCHAR2(40) 
ACTOR             VARCHAR2(20) 
DURACION          NUMBER(3)   
```
## 04 Ingrese los siguientes registros:
codigo sql
```sql
insert into peliculas (codigo,titulo,actor,duracion) values(1,'Mision imposible','Tom Cruise',120);
insert into peliculas (codigo,titulo,actor,duracion) values(2,'Harry Potter y la piedra filosofal',null,180);
insert into peliculas (codigo,titulo,actor,duracion) values(3,'Harry Potter y la camara secreta','Daniel R.',null);
insert into peliculas (codigo,titulo,actor,duracion) values(0,'Mision imposible 2','',150);
insert into peliculas (codigo,titulo,actor,duracion) values(4,'Titanic','L. Di Caprio',220);
insert into peliculas (codigo,titulo,actor,duracion) values(5,'Mujer bonita','R. Gere.J. Roberts',0);
```
salida sql
```sh
1 row inserted.


1 row inserted.


1 row inserted.


1 row inserted.


1 row inserted.


1 row inserted.
```
## 05 Recupere todos los registros para ver cómo Oracle los almacenó.
codigo sql
```sql
select * from peliculas;
```
salida sql
```sh
    CODIGO TITULO                                   ACTOR                  DURACION
---------- ---------------------------------------- -------------------- ----------
         1 Mision imposible                         Tom Cruise                  120
         2 Harry Potter y la piedra filosofal                                   180
         3 Harry Potter y la camara secreta         Daniel R.                      
         0 Mision imposible 2                                                   150
         4 Titanic                                  L. Di Caprio                220
         5 Mujer bonita                             R. Gere.J. Roberts            0

```
## 06 Intente ingresar un registro con valor nulo para campos que no lo admiten (aparece un mensaje de error)
codigo sql
```sql
insert into peliculas (codigo,titulo,actor,duracion) values(null,'Mujer bonita','R. Gere.J. Roberts',null);
```
salida sql
```sh
Error starting at line : 1 in command -
insert into peliculas (codigo,titulo,actor,duracion) values(null,'Mujer bonita','R. Gere.J. Roberts',null)
Error at Command Line : 1 Column : 61
Error report -
SQL Error: ORA-01400: cannot insert NULL into ("BRAYAN"."PELICULAS"."CODIGO")
01400. 00000 -  "cannot insert NULL into (%s)"
*Cause:    An attempt was made to insert NULL into previously listed objects.
*Action:   These objects cannot accept NULL values.
```
## 07 Muestre todos los registros.
codigo sql
```sql
select * from peliculas;
```
salida sql
```sh
    CODIGO TITULO                                   ACTOR                  DURACION
---------- ---------------------------------------- -------------------- ----------
         1 Mision imposible                         Tom Cruise                  120
         2 Harry Potter y la piedra filosofal                                   180
         3 Harry Potter y la camara secreta         Daniel R.                      
         0 Mision imposible 2                                                   150
         4 Titanic                                  L. Di Caprio                220
         5 Mujer bonita                             R. Gere.J. Roberts            0

```
## 08 Actualice la película en cuyo campo "duracion" hay 0 por "null" (1 registro)
codigo sql
```sql
UPDATE peliculas SET duracion = NULL WHERE duracion = 0;
```
salida sql
```sh
1 row updated.
```
## 09 Recupere todos los registros.
codigo sql
```sql
SELECT * FROM medicamentos;
```
salida sql
```sh
    CODIGO NOMBRE               LABORATORIO              PRECIO   CANTIDAD
---------- -------------------- -------------------- ---------- ----------
         1 Sertal gotas                                                100
         2 Sertal compuesto                                 8.9        150
         3 Buscapina            Roche                                  200
         9 paracetamol                                        0        250
         4 Medicamento X                                   12.5         50
```
